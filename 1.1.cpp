/**
 * Quick-Find Algorithm
 * The basis of this algorithm is an array of integers with the property that p
 * and q are connected if and only if the pth and qth array entries are equal.
 **/
#include <stdio.h>

#define N 10

int main(void)
{
	int p, q;

	int ID[N] = {0};
	for(int i = 0; i < N; i++)
	{
		ID[i] = i;
	}

	while(scanf("%d %d", &p, &q) == 2)
	{
		if(ID[p] == ID[q]) continue;

		for(int val = ID[p], i = 0; i < N; i++)
		{
			if(ID[i] == val)
			{
				ID[i] = ID[q];
			}
		}
		printf(" %d %d\n", p, q);

		for(int i = 0; i < N; i++)
		{
			printf("%d, ", ID[i]);
		}
		printf("\n");
	}

	return 0;
}
