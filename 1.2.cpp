/**
 * Quick-Union Algorithm
 * Instead of looping over the entire set and updating links we follow the links
 * through the array until we reach the end and update.
 *
 * For example linking 2 3 we start at two and follow values at each array index
 * until we reach an index pointing at itself. We do the same for three. At the
 * end we assign the value at index J to index I creating the union.
 **/
#include <stdio.h>

#define N 20

int main(void)
{
	int p, q;

	int ID[N] = {0};
	for(int i = 0; i < N; i++)
	{
		ID[i] = i;
	}

	while(scanf("%d %d", &p, &q) == 2)
	{
		int i = 0;
		int j = 0;
		for(i = p; i != ID[i]; i = ID[i]) ;
		for(j = q; j != ID[j]; j = ID[j]) ;

		if(i == j) continue;
		ID[i] = j;

		printf(" %d %d\n", p, q);

		for(int i = 0; i < N; i++)
		{
			printf("%d, ", ID[i]);
		}
		printf("\n");
	}
		for(int i = 0; i < N; i++)
		{
			printf("%2d, ", i);
		}
		printf("\n");
		for(int i = 0; i < N; i++)
		{
			printf("%2d, ", ID[i]);
		}
		printf("\n");

	return 0;
}
