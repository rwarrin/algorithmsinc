/**
 * Weighted Quick-Union Algorithm with Path Compression
 *
 * Introduces an additional array sz for the purpose of maintaining, for each
 * object with id[i] == i, the number of nodes in the associated tree, so that
 * the union operation can link the smaller of the two specified trees to the
 * larger, thus preventing the growth of long paths in the trees.
 *
 * Path Compression - Modifying the for loops we halve the length of any path
 * that we traverse. The net result of this change is that the trees become
 * almost completely flat after a long sequence of operations.
 **/
#include <stdio.h>

#define N 10

int main(void)
{
	int i, j, p, q, id[N], sz[N];
	for(i = 0; i < N; i++)
	{
		id[i] = i;
		sz[i] = 1;
	}

	while((scanf("%d %d", &p, &q)) == 2)
	{
		for(i = p; i != id[i]; i = id[i])
		{
			id[i] = id[id[i]];
		}

		for(j = q; j != id[j]; j = id[j])
		{
			id[j] = id[id[j]];
		}

		if(i == j) continue;

		if(sz[i] < sz[j])
		{
			id[i] = j;
			sz[j] += sz[i];
		}
		else
		{
			id[j] = i;
			sz[i] += sz[j];
		}
		printf(" %d %d\n", p, q);

		for(int k = 0; k < N; k++)
		{
			printf("%d ", id[k]);
		}
		printf("\n");
	}

	return 0;
}
