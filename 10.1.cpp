/**
 * Binary quicksort
 *
 * This program partitions a file on the leading bits of the keys, and then
 * sorts the subfiles recursively. The variable w keeps track of the bit being
 * examined, starting at 0 (leftmost). The partitioning stops with j equal i,
 * and all elements ot the right of a[i] having 1 bits in the wth position an
 * dall elements to the left of a[i] having 0 bits in the bth position. The
 * element a[i] itself will have a 1 bit unless all keys in the file have a 0 in
 * position w. An extra test just after the partitioning loop covers this case.
 **/

quicksortB(int a[], int l, int r, int w)
{
	int i = l, j = r;
	if(r <= l || w > bitsword)
	{
		return;
	}

	while(j != i)
	{
			while(digit(a[i], w) == 0 && (i < j))
			{
				i++;
			}

			while(digit(a[j], w) == 1 && (j > i))
			{
				j--;
			}

			exch(a[i], a[j]);
	}

	if(digit(a[r], w) == 0)
	{
		j++;
	}

	quicksortB(a, l, j-1, w+1);
	quicksortB(a, j, r, w+1);
}

void sort(Item a[], int l, int r)
{
	quicksortB(a, l, r, 0);
}
