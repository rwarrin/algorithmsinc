/**
 * MSD radix sort
 *
 * We derive this program from Program 8.17 (key-indexed-counting sort) by
 * changing key references to key-digit references, and adding a loop at the end
 * that does recursive calls for each subfile of keys starting with the same
 * diigit. For varfiable-length keys terminated by 0 digits (such as C strings),
 * omit the first if statement and the first recursive call. This implmentation
 * uses an auxiliary array (aux) that is big enough to hold a copy of the input.
 **/

#define bin(A) l+count[A]

void radixMSD(Item a[], int l, int r, int w)
{
	int i, j, count[R+1];

	if(w > bytesword)
	{
		return;
	}

	if(r - l <= M)
	{
		insertion(a, l, r);
		return;
	}

	for(j = 0; j < R; j++)
	{
		count[j] = 0;
	}

	for(i = l; i <= r; i++)
	{
		count[digit(a[i], w) + 1]++;
	}

	for(j = 1; j < R; j++)
	{
		count[j] += count[j-1];
	}

	for(i = l; i <= r; i++)
	{
		aux[count[digit(a[i], w)]++] = a[i];
	}

	for(i = l; i <= r; i++)
	{
		a[i] = aux[i-l];
	}

	radixMSD(a, l, bin(0)-1, w+1);

	for(j = 0; j < R-1; j++)
	{
		radixMSD(a, bin(j), bin(j+1)-1, w+1);
	}
}
