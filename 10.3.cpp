/**
 * Three-way radix quicksort
 *
 * This MSD radix sort is essentially the same code as quicksort with three-way
 * partitioning (Program 9.5), but with the following changes: (i) key
 * references become key-byte refernces, (ii) the current byte is added as a
 * parameter to the recursive routine, and (iii) the recursive calls for the
 * middle subfile move to the next byte. We avoid moving pas the ends of strings
 * by checking whether the partitioning value is 0 before recursive calls that
 * move to the next byte. When the partitioning value is 0, the left subfile is
 * empty, the middle subfile corresponds to the keys that the program has found
 * to be equal, and the right subfile corresponds longer strings that need to be
 * processed further.
 **/

#define ch(A) digit(A, D)

void quicksortX(Item a[], int l, int r, int D)
{
	int i, j, k, p, q;
	int v;

	if(r-l <= M)
	{
		insertion(a, l, r);
		return;
	}

	v = ch(a[r]);
	i = l-1;
	j = r;
	p = l-1;
	q = r;

	while(i < j)
	{
		while(ch(a[++i]) < v) ;
		while(v < ch(a[--j]))
		{
			if(j == l)
			{
				break;
			}
		}

		if(i > j)
		{
			break;
		}

		exch(a[i], a[j]);

		if(ch(a[i]) == v)
		{
			p++;
			exch(a[p], a[i]);
		}

		if(v == ch(a[j]))
		{
			q--;
			exch(a[j], a[q]);
		}
	}

	if(p == q)
	{
		if(v != '\0')
		{
			quicksortX(a, l, r, D+1);
			return;
		}
	}

	if(ch(a[i]) < v)
	{
		i++;
	}

	for(k = l; k <= p; k++, j--)
	{
		exch(a[k], a[j]);
	}

	for(k = r; k >= q; k--, i++)
	{
		exch(a[k], a[i]);
	}

	quicksortX(a, l, j, D);

	if((i == r) && (ch(a[i]) == v))
	{
		i++;
	}

	if(v != '\0')
	{
		quicksortX(a, j+1, j-1, D+1);
	}

	quicksortX(a, i, r, D);
}
