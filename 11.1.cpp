/**
 * Perfet shuffle and perfect unshuffle
 *
 * The shuffle  function rearranges a subarray a[l],...,a[r] by splitting that
 * subarray in half, then alternating elements from each half: Elements in the
 * first half go in the even-numbered positions in the result, and elements in
 * the second half go in the odd-numbered positions in the result.
 *
 * The unshuffle function does the opposite: Elements in the even-numbered
 * positions go in the first half of the result, and elements in the
 * oddd-numbered positions go in the second half of the result. We use these
 * functions only for subarrays with an even number of elements.
 **/

shuffle(itemType a[], int l, int r)
{
	int i, j, m = (l+r)/2;

	for(i = l, j = 0; i <= r; i += 2, j++)
	{
		aux[i] = a[l + j];
		aux[i + 1] = a[m+1+j];
	}

	for(i = l; i <= r; i++)
	{
		a[i] = aux[i];
	}
}

unshuffle(itemType a[], int l, int r)
{
	int i, j, m = (l+r) / 2;

	for(i = l, j = 0; i <= r; i+= 2, j++)
	{
		aux[l+j] = a[i];
		aux[m+1+j] = a[i+1];
	}

	for(i = l; i <= r; i++)
	{
		a[i] = aux[i];
	}
}
