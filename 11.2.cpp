/**
 * Batcher's odd-even merge (recursive version)
 *
 * This recursive program implements an abstract inplace merge, using the
 * shuffle and unshuffle operations  from program 11.1, although they are not
 * essential -- Program 11.3 is a bottom-up nonrecursive version of this program
 * with shuffling removed. Our primary interest here is that this implementation
 * provides a compact description of Batcher's algorithm, when the file size is
 * a power of 2.
 **/

mergeTD(itemType a[], int l, int r)
{
	int i, m = (l+r) / 2;

	if(r == l+1)
	{
		compexch(a[l], a[r]);
	}

	if(r < l+2)
	{
		return;
	}

	unshuffle(a, l, r);
	mergeTD(a, l, m);
	mergeTD(a, m+1, r);
	shuffle(a, l, r);

	for(i = l+1; i < r; i += 2)
	{
		compexch(a[i], a[i+1]);
	}
}
