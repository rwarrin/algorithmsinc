/**
 * Batcher's odd-even merge (nonrecursive version)
 *
 * This implmeenatation of Batcher's odd-even merge (which assumes that the file
 * size N is a power of 2) is compact but mysterious. We can understand how it
 * accomplishes ht merge by examining how it corresponds to the recursive
 * version (see Program 11.2 and Figure 11.5). It accomplishes the merge in lg N
 * passes consisting of uniform and independent compare-exchange instructions.
 **/

mergeBU(itemType a[], int l, int r)
{
	int i, j, k, N = r-l+1;

	for(k = N/2; k > 0; k /= 2)
	{
		for(j = k % (N/2); j+k < N; j+= (k+k))
		{
			for(i = 0; i < k; i++)
			{
				compexch(a[l+j+i], a[l+j+i+k]);
			}
		}
	}
}
