/**
 * Batcher's odd-even sort (nonrecursive version)
 *
 * This implementation of Batcher's odd-even sort corresponds directly to the
 * network representation in Figure 11.7. It divides into phases, indexed by the
 * variable p. The last phase, when p is N, is Batcher's odd-even merge. The
 * next-to-lastphase, when p is N/2, is the odd-even merge with the first  stage
 * and all comparators that cross N/2 eliminated; the third-to-last phase, when
 * p is N/4, is the odd-even merge with the first two stages and all comparators
 * that cross any multiple of N/4 eliminated, and so forth.
 **/

void batchersort(itemType a[], int l, int r)
{
	int i, j, k, p, N = r-l+1;

	for(p = 1; p < N; p += p)
	{
		for(k = p; k > 0; k /= 2)
		{
			for(j = k%p; j+k < N; j += (k+k))
			{
				for(i = 0; i < k; i++)
				{
					if(j+i+k < N)
					{
						if((j+i)/(p+p) == (j+i+k)/(p+p))
						{
							compexch(a[l+j+i], a[l+j+i+k]);
						}
					}
				}
			}
		}
	}
}
