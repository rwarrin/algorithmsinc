/**
 * Symbol-table abstract data type
 *
 * This interface defines operations for a simple symbol table: initialize,
 * return the item count, add a new item, find an item with a given k ey, delete
 * an item with a given key, select the kth smallest item, and visit the items
 * in order of their keys (calling a procedure passed as an argument for each
 * item).
 **/

void STinit(int);
int STcount();
void STinsert(Item);
Item STsearch(Key);
void STdelete(Item);
Item STselect(int);
void STsort(void (*visit)(Item));
