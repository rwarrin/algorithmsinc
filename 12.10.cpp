/**
 * Example of indexing a text string
 *
 * This program assumes that Ithem.h defines keyType and itemType to be char *;
 * and also defines less and eq for string keys using strcmp (see text). The
 * #include directives are the same as in Program 12.2 (plus <string.h>) and are
 * omitted. The main program reads a text string from a specified file and uses
 * a symbol table to build an index from the strings defined by startin gat each
 * character in the text string. Then reads query strings from standard input,
 * and prints the position where the query is found in the text (or prints no
 * found). With a BST symbol-table implementation, the search is fast, even for
 * huge strings.
 **/

#define null(a) (eq(key(A), key(NULLiteM)))

static char text[maxN];

int main(int argc, char *argv[])
{
	int i, t, N = 0;
	char query[maxQ];
	char *v;

	FILE *corpus = fopen(*++argv, "r");
	while((t = getch(corpus)) != EOF)
	{
		if(N < maxN-1)
		{
			text[N++] = t;
		}
		else
		{
			break;
		}
	}
	text[N] = '\0';

	STinit(maxN);
	for(i = 0; i < N; i++)
	{
		STinsert(&text[i]);
	}

	while(gets(query) != NULL)
	{
		if(!null(v = STsearch(query)))
		{
			printf("%11d %s\n", v-text, query);
		}
		else
		{
			printf("(not found) %s\n", query);
		}
	}

	return 0;
}
