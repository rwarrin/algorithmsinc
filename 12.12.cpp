/**
 * Root insertion in BSTs
 *
 * With the rotation functions in Program 12.11, a recursive function that
 * inserts a new n ode at the root of a BST is immediate: Insert the new item at
 * the root in the appropriate subtree, then perform the appropriate rotation to
 * bring it to the root of the main tree.
 **/

link insertT(link H, Item item)
{
	Key v = key(item);
	if(h == z)
	{
		return NEW(item, z, z, 1);
	}

	if(less(v, key(h->item)))
	{
		h->l = insertT(h->l, item);
		h = rotR(h);
	}
	else
	{
		h->r = insertT(h->r, item);
		h = rotL(h);
	}

	return h;
}

void STinsert(Item item)
{
	head =  insertT(head, item);
}
