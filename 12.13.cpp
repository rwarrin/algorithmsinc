/**
 * The recursive function selectR finds the item with the kth smallest key in a
 * BST. It uses zer-based indexing -- for example, we take k = 0 to look for the
 * item with the smallest key. This code assumes that each tree node has its
 * subtree size iin the N field. Compare the program with quicksort-based select
 * in an array (Program 9.6).
 **/

Item selectR(link h, int k)
{
	int t;
	if(h == z)
	{
		return NULLitem;
	}

	t = (h->l == z) ? 0 : h->l->N;

	if(t > k)
	{
		return selectR(h->l, k);
	}
	if(t < k)
	{
		return selectR(h->r, k-t-1);
	}

	return h->item;
}

Item STselect(int k)
{
	return selectR(head, k);
}
