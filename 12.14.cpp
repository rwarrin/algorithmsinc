/**
 * Partitioning a BST
 *
 * Adding rotations after the recursive calls transforms the selection function
 * of Program 12.13 into a function that puts the selected item at the root.
 **/

link partR(link h, int k)
{
	int t = h->l->N;

	if(t > k)
	{
		h->l = partR(h->l, k);
		h = rotR(h);
	}
	else
	{
		h->r = partR(h->r, k-t-1);
		h = rotL(h);
	}

	return h;
}
