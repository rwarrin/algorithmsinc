/**
 * Joining of two BSTs
 *
 * If either BST is empty, the other is the result. Otherwise, we combine the
 * two BSTs by (arbitrarily) choosing the root of the first as the root, root
 * inserting that root into the second, then (recursively) combining the pair
 * of left subtrees and the pair of right subtreeds.
 *
 **/

link STjoin(link a, link b)
{
	if(b == z)
	{
		return a;
	}

	if(a == z)
	{
		return b;
	}

	b = insertT(b, a->item);
	b->l = STjoin(a->l, b->l);
	b->r = STjoin(a->r, b->r);

	free(a);
	return b;
}
