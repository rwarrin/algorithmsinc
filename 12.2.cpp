/**
 * Example of a symbol-table client
 *
 * This program uses a symbol table to find the distinct keys in a sequence
 * generated randomly or read from standard input. For each key, it uses
 * STsearch to check whether the key has been seen before. If the key has not
 * been seen before, it inserts an item with that key into the symbol table. The
 * types of keys and items, and the abstract operations on them, are specified
 * in Item.h.
 **/

#include <stdio.h>
#include <stdlib.h>
#include "Item.h"
#include "ST.h"

void main(int argc, char *argv[])
{
	int N, maxN = atoi(argv[1]), sw = atoi(argv[2]);
	Key v;
	Item item;

	STinit(maxN);
	for(N = 0; N < maxN; N++)
	{
		if(sw)
		{
			v = ITEMrand();
		}
		else if(ITEMscan(&v) == EOF)
		{
			break;
		}

		if(STsearch(v) != NULLitem)
		{
			continue;
		}

		key(item) = v;
		STinsert(item);
	}

	STsort(ITEMshow);
	printf("\n");
	printf("%d keys ", N);
	printf("%d distinct keys\n", STcount());
}
