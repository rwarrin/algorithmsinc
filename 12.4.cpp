/**
 * Array-based symbol table (ordered)
 *
 * Like Program 12.3, this implementation uses an array of items, but without
 * any null items. We keep the array in order when inserting a new item by
 * moving larger items one position to the right in the same manner as insertion
 * sort.
 * The STsearch function is a scan through the array that looks for an item with
 * the specified key. Since the array is in order, we know that the search key
 * is not in the table as soono as we encounter an item with a larger key. The
 * STselect and STsort functions are trivial, and the implemenation of STdelete
 * is left as an exercise (see Exercise 12.14).
 **/

static Item *st;
static int N;

void STinit(int maxN)
{
	st = malloc((maxN) * sizeof(Item));
	N = 0;
}

int STcount()
{
	return N;
}

void STinsert(Item item)
{
	int j = N++;
	Key v = key(item);
	while(j > 0  && less(v, key(st[j-1])))
	{
		st[j] = st[j-1];
		j--;
	}

	st[j] = item;
}

Item STsearch(Key v)
{
	int j;
	for(j = 0; j < N; j++)
	{
		if(eq(v, key(st[j])))
		{
			return st[j];
		}
		if(less(v, key(st[j])))
		{
			break;
		}
	}

	return NULLitem;
}

Item STselect(int k)
{
	return st[k];
}

void STsort(void (*visit)(Item))
{
	int i;
	for(i = 0; i < N; i++)
	{
		visit(st[i]);
	}
}
