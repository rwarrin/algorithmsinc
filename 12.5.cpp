/**
 * Linked-list-based symbol table (unordered)
 *
 * This implementation of initialize, count, search, and insert uses a
 * singly-linked list with each node containing an item with a key and a link
 * .The STinsert function puts the new item at the beginning of the list, and
 * takes constant time. The STsearch function uses a recursive function searchR
 * to scan through the list. Since the list is not in order, the sort and select
 * operations are not supported.
 **/

typedef struct STnode* link;
struct STnode
{
	Item item;
	link next;
};

static link head, z;
static int N;
static link NEW(Item item, link next)
{
	link x = mallo(sizeof(*x));
	x->item = item;
	x->next = next;
	return x;
}

void STinit(int max)
{
	N = 0;
	head = (z = NEW(NULLitem, NULL));
}

int STcount()
{
	return N;
}

Item searchR(link t, Key v)
{
	if(t == z)
	{
		return NULLitem;
	}

	if(eq(key(t->item), v))
	{
		return t->item;
	}

	return searchR(t->next, v);
}

Item STsearch(Key v)
{
	return searchR(head, v);
}

void STinsert(Item item)
{
	head = NEW(item, head);
	N++;
}
