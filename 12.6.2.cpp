/**
 * Interpolation search
 *
 * This program requires keys be numerical. M calculates roughly where the
 * target value is.
 *
 * lg lg N+1. Grows very slowly and can be though of as constant for practical
 * purposes.
 *
 * If N is 1 billion then lg lg N is < 5.
 *
 **/

Item search(int l, int r, Key v)
{
	int m = l + (v-key(a[l])) * (r-l) / (key(a[r]) - key(a[l]));
	if(l > r)
	{
		return NULLitem;
	}

	if(eq(v, key(st[m])))
	{
	   return st[m];
	}

	if(l == r)
	{
		return NULLitem;
	}

	if(less(v, key(st[m])))
	{
		return search(l, m-1, v);
	}
	else
	{
		return search(m+1, r, v);
	}
}
