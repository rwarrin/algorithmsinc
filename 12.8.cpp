/**
 * Sorting with a BST
 *
 * An inorder traversal of a BST visits the items in order of their keys. In
 * this implementation, visit is a function supplied by the client that is
 * called with each of the items as its argument, in order of their keys.
 **/

void sortR(link h, void (*visit)(Item))
{
	if(h == z)
	{
		return;
	}

	sortR(h->l, visit);
	visit(h->item);
	sortR(h->r, visit);
}

void STsort(void (*visit)(Item))
{
	sortR(head, visit);
}
