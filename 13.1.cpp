/**
 * This recursive function puts a BST into perfect balance, using the
 * partitioning function partR from Program 12.14. We partition to put the
 * median node at the root, then (recursively) do the same for the subtrees.
 **/

link balanceR(link h)
{
	if(h->N < 2)
	{
		return h;
	}

	h = partR(h, h->N/2);
	h->l = balanceR(h->l);
	h->r = balanceR(h->r);

	return h;
}
