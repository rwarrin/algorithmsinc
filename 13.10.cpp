/**
 * Deletion in skip lists
 *
 * To delete a node with a given key from a skip list, we unlink it at each
 * level that we find a link to it, then free it when we reach the bottom level.
 **/

void deleteR(link t, Key v, link k)
{
	link x = t->next[k];
	if(!less(key(x->item), v))
	{
		if(eq(v, key(x->item)))
		{
			t->next[k] = x->next[k];
		}

		if(k == 0)
		{
			free(x);
			return;
		}

		deleteR(t, v, k-1);
		return;
	}

	deleteR(t->next[k], v, k);
}

void STdelete(Key v)
{
	deleteR(head, v, lgN);
	N--;
}
