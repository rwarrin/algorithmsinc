/**
 * Randomized BST inssertion
 *
 * This function makes a randomized decision whether to use the root insertion
 * method of Program 12.12 or the standard insertion method of Program 12.7. In
 * a random BST, each of th enodes is at the root with equal probability; so we
 * get random trees by putting a new node at the root of a tree of size N with
 * probability 1/(N+1).
 **/

link insertR(link h, Item item)
{
	Key v = key(item);
	Key t = key(h->item);

	if(h == z)
	{
		return NEW(item, z, z, 1);
	}

	if(rand() < RAND_MAX/(h->N + 1))
	{
		return insertT(h, item);
	}

	if(less(v, t))
	{
		h->l = insertR(h->l, item);
	}
	else
	{
		h->r = insertR(h->r, item);
	}

	(h->N)++;
	return h;
}

void STinsert(Item item)
{
	head = insertR(head, item);
}
