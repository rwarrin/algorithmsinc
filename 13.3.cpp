/**
 * Randomized BST combination
 *
 * This function uses the same method as Program 12.16, except that it makes a
 * randomized, rather than an arbitrary, decision about which node to use for
 * the root in a combined tree, using probabilities that ensure that each node
 * is equally likely to be the root. The function fixN updates b->N to be 1 plus
 * the sum of the corresponding fields in the subtrees (0 for null trees).
 **/

link joinR(link a, link b)
{
	if(a == z)
	{
		return b;
	}

	b = insertR(b, a->item);
	b->l = STjoin(a->l, b->l);
	b->r = STjoin(a->r, b->r);
	fixN(b);
	free(a);

	return b;
}

link STjoin(link a, link b)
{
	if(rand() / (RAND_MAX/(a->N + b->N) + 1) < a->N)
	{
		joinR(a, b);
	}
	else
	{
		joinR(b, a);
	}
}

