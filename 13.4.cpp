/**
 * Deletion in a randomized BST
 *
 * We use the same STdelete function as we did for standard BSTs (see Program
 * 12.15), but replace the joinLR function wit the one shown here, which makes a
 * randomized, rather than an arbitrary, decision about whether to replace the
 * deleted node with the predecessor or the successor, using probabilities that
 * ensure that each node in the resulting tree is equally likely to be the root.
 * To properly maintain the node counts, we also need to include a cll to fixN
 * (see Program 13.3) for h before returning from removeR.
 **/

link joinLR(link a, link b)
{
	if(a == z)
	{
		return b;
	}

	if(b == z)
	{
		return a;
	}

	if(rand() / (RAND_MAX / (a->N + b->N) + 1) < a->N)
	{
		a->r = joinLR(a->r, b);
		return a;
	}
	else
	{
		b->l = joinLR(a, b->l);
		return b;
	}
}
