/**
 * Splay insertion in BSTs
 *
 * This function differs from the root insertion algorithm of Program 12.12 in
 * just one essential detail: if the search path goes left=left or right-right,
 * the node is brought to the root with a double rotation from the top, rather
 * than from the bottom (see Figure 13.6).
 * The program checks the four possibilities for two steps of the search path
 * from the root and performs the appropriate rotations:
 *  left-left: Rotate right at the root twice.
 *  left-right: Rotate left at the left child, then right at the root.
 *  right-right: Rotate left at the root twice.
 *  right-left: Rotate right at the right child, then left at the root.
 * For economy, we use macros so that we can write hl instead of h->l and hrl
 * instead of h->r->l and so forth.
 **/

link splay(link h, Item item)
{
	Key v = key(item);
	if(h == z)
	{
		return NEW(item, z, z, 1);
	}

	if(less(v, key(h->item)))
	{
		if(hl == z)
		{
			return NEW(item, z, h, h->N+1);
		}

		if(less(v, key(hl->item)))
		{
			hll = splay(hll, item);
			h = rotR(h);
		}
		else
		{
			hlr = splay(hlr, item);
			hl = rotL(hl);
		}
		return rotR(h);
	}
	else
	{
		if(hr == z)
		{
			return NEW(item, h, z, h->N+1);
		}

		if(less(key(hr->item), v))
		{
			hrr = splay(hrr, item);
			h = rotL(h);
		}
		else
		{
			hrl = splay(hrl, item);
			hr = rotR(hr);
		}
		return rotL(h);
	}
}

void STinsert(Item item)
{
	head = splay(head, item);
}
