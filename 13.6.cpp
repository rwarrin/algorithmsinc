/**
 * Insertion in red-black BSTs
 *
 * This function implements insertion in 2-3-4 trees using the red-black
 * representation. We add a color bit red to the type STnode (and extend NEW
 * accordingly), with 1 signifying that the node is red, and 0 signifying that
 * it is black. An empty tree is a link to the sentinel node z -- a black node
 * with links to itself.
 * On the way down the tree (before the recursive call), we check for 4-nodes
 * and split them by flipping the color bits in all three nodes. When we reach
 * the bottom, we create a new red node for the item to be inserted and return a
 * pointer to it.
 * On the way up the tree (after the recursive call), we set the link down which
 * we went to the link value returned, then check whether a rotation is needed.
 * If the search path has two red links with the same orientation, we do a
 * single rotation from the top node, then flip the color bits to make a proper
 * 4-node. If the search path has two red links with different orientations, we
 * do a single rotation from the bottom node, reducing to the other case for the
 * next step up.
 **/

link RBinsert(link h, Item item, int sw)
{
	Key v = key(item);
	if(h == z)
	{
		return NEW(item, z, z, 1, 1);
	}

	if((hl->red) && (hr->red))
	{
		h->red = 1;
		hl->red = 0;
		hr->red = 0;
	}

	if(less(v, key(h->item)))
	{
		hl = RBinsert(hl, item, 0);
		if(h->red && hl->red && sw)
		{
			h = rotR(h);
		}
		if(hl->red && hll->red)
		{
			h = rotR(h);
			h->red = 0; 
			hr->red = 1;
		}
	}
	else
	{
		hr = RBinsert(hr, item, 1);
		if(h->red && hr->red && !sw)
		{
			h = rotL(h);
		}
		if(hr->red && hrr->red)
		{
			h = rotL(h);
			h->red = 0;
			hl->red = 1;
		}
	}
	fixN(h);
	return h;
}

void STinsert(Item item)
{
	head = RBinsert(head, item, 0);
	head->red = 0;
}
