/**
 * Searching in skip lists
 *
 * For k equal to 0, this code is equivalent to Program 12.5, searching in
 * singly linked lists. For general k, we move to the next node in the list on
 * level k if its key is smaller than the search key, and down to level k-1 if
 * its key is not smaller. To simply the code, we assume that all the lists end
 * with a  sentinel node z that has NULLitem with maxKey.
 **/

Item searchR(link t, Key v, int k)
{
	if(t == z)
	{
		return NULLitem;
	}

	if(eq(v, key(t->item)))
	{
		return t->item;
	}

	if(less(v, key(t->next[k]->item)))
	{
		if(k == 0)
		{
			return NULLitem;
		}

		return searchR(t, v, k-1);
	}

	return searchR(t->next[k], v, k);
}

Item STsearch(Key v)
{
	return searchR(head, v, lgN);
}
