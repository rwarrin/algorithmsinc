/**
 * Skip-list initialization
 *
 * Nodes in skip lists have an array of links, so NEW needs to allocate the
 * array and to set all the links to the sentinel z. The constant lgNmax is the
 * maximum number of levels that we will allow in the list: It might be set to
 * five for finy lists or to 30 for huge lists. The variable N keeps the number
 * of items in the liist, as usual, and lgN is the number of levels. An empy
 * list is a head node with lgNmax links, all set to z, with N and lgN set to 0.
 **/

typedef struct STnode* link;
struct STnode
{
	Item item;
	link* next;
	int sz;
};

static link head, z;
static int N, lgN;

link NEW(Item item, int k)
{
	int i;
	link x = malloc(sizeof(*x));
	x->next = malloc(k * sizeof(link));
	x->item = item;
	x->sz = k;
	for(i = 0; i < k; i++)
	{
		x->next[i] = z;
	}
	return x;
}

void STinit(int max)
{
	N = 0;
	lgN = 0;
	z = NEW(NULLitem, 0);
	head = NEW(NULLitem, lgNmax+1);
}
