/**
 * Insertion in skip lists
 *
 * To insert an item into a skip list, we generate a new j-link node with
 * probability 1/2^j, then follow the search path precisely as in Program 13.7,
 * but link in the new node when we move down to each of the bottom j levels.
 **/

int randX()
{
	int i, j, t = rand();
	for(i = 1, j = 2; i < lgNmax; i++, j += j)
	{
		if(t > RAND_MAX/j)
		{
			break;
		}
	}

	if(i > lgN)
	{
		lgN = i;
	}

	return i;
}

void insertR(link t, link x, int k)
{
	Key v = key(x->item);
	if(less(v, key(t->next[k]->item)))
	{
		if(k < x->sz)
		{
			x->next[k] = t->next[k];
			t->next[k] = x;
		}

		if(k == 0)
		{
			return;
		}

		insertR(t, x, k-1);
		return;
	}

	insertR(t->next[k], x, k);
}

void STinsert(Item item)
{
	insertR(head, NEW(item, randX()), lgN);
	N++;
}
