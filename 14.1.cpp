/**
 * Hash function for string keys
 *
 * This implementation of a hash function for string keys involves one
 * multiplication and one addition per character in the key. If we were to
 * replace the constant 127 by 128, the program would simply compute the
 * remainer when the number corresponding to the 7-bit ASCII representation of
 * the key was divided by the table size, using Horner's method. The prime base
 * 127 heps us to avoid anomalies if the table size is a power of 2 or a
 * multiple of 2.
 **/

int hash(char *v, int M)
{
	int h = 0;
	int a = 127;
	for(; *v != '\0'; v++)
	{
		h = (a*h + *v) % M;
	}

	return h;
}
