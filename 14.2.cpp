/**
 * Universal hash function (for string keys)
 *
 * This program does the same computations as program 14.1, but using
 * pseudorandom coefficient values instead of a fixed radix, to approximate the
 * ideal of having a collision between two given nonequal keys occur with
 * probability 1/M. We generate the coefficients rather than using an array of
 * precomputed random values because this alternative presents a slightly
 * simpler interface.
 **/

int hashU(char *v, int M)
{
	int h;
	int a = 31415;
	int b = 27183;

	for(h = 0; *v != '\0'; v++, a = a*b % (M-1))
	{
		h = (a*h + *v) % M;
	}

	return h;
}
