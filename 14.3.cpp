/**
 * Hashing with separate chaining
 *
 * This symbol-table implementation is based on replacing the STinit, STsearch,
 * STinsert, and STdelete functions in the linked-list-based symbl table of
 * Program 12.5 with the functions given here, and replacing the link head with
 * an array of links heads. We use the same recursive list s earch and deletion
 * procedures as in Program 12.5, but we maintain M lists, with head links in
 * heads, using a hash function to choose among the lists.  The STinit function
 * sets M such that we expect the lists to h ave about five items each;
 * therefore the other operations require just a few probes.
 **/

static link *heads, z;
static int N, M;

void STinit(int max)
{
	int i;
	N = 0; M = max / 5;
	heads = malloc(M*sizeof(link));
	z = NEW(NULLitem, NULL);
	for(i = 0; i < M; i++)
	{
		heads[i] = z;
	}
}

Item STsearch(Key v)
{
	return searchR(heads[hash(v, M)], v);
}

void STinsert(Item item)
{
	int i = hash(key(item), M);
	heads[i] = NEW(item, heads[i]);
	N++;
}

void STdelete(Item item)
{
	int i = hash(key(item), M);
	heads[i] = deleteR(heads[i], item);
}
