/**
 * Linear probing
 *
 * Thi symbol-table implementation keeps items in a table twice the size of the
 * maximum number of items expected, initialized to NULLitem. The table holds
 * the items themselves; if the items are large, we can modify the item type to
 * hold links to the items.
 * To insert a new item, we hash to a table position and scan to the right to
 * find an unoccupied position, using the macro null to check whether a table
 * position in unoccupied. To search for an item with a given key, we go to the
 * key hash position and s can to look for a match, stopping when we hit an
 * unoccupied position.
 * The STinit function sets M such that we may expect the table to be less than
 * half full, so the other operations will require just a few probes, if the
 * hash function produces values that are sufficiently close to random ones.
 **/

#include <stdlib.h>
#include "Item.h"

#define null(A) (key(st[A]) == key(NULLitem))

static in N, M;
static Item *st;

void STinit(int max)
{
	int i;
	N = 0;
	M = 2*max;
	st = malloc(M*sizeof(Item));
	for(i = 0; i < M; i++)
	{
		st[i] = NULLitem;
	}
}

int STcount()
{
	return N;
}

void STinsert(Item item)
{
	Key v = key(item);
	int i = hash(v, M);
	while(!null(i)) i = (i + 1) % M;
	st[i] = item;
	N++;
}

Item STsearch(Key v)
{
	int i = hash(v, M);
	while(!null(i))
	{
		if(eq(v, key(st[i])))
		{
			return st[i];
		}
		else
		{
			i = (i + 1) % M;
		}
	}

	return NULLitem;
}
