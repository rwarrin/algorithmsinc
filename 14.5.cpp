/**
 * Deletion in a linear-probing hash table
 *
 * To delete an item with a given key, we search for such an item and replace it
 * with NULLitem. Then, we need to correct for the possibility that some item
 * that lies to the right of th enow-unoccupied position originally hashed to
 * that position or to its left, because the vacancy would terminate a search
 * for such an item. Therefore, we reinsert all the items in the same cluster as
 * the deleted item and to that item's right. Since the table is less than half
 * full, the number of items that are reinseted will be small, on the average.
 **/

void STdelete(Item item)
{
	int j, i = hash(key(item), M);
	Item v;

	while(!null(i))
	{
		if(eq(key(item), key(st[i])))
		{
			break;
		}
		else
		{
			i = (i + 1) % M;
		}
	}

	if(null(i))
	{
		return;
	}

	st[i] = NULLitem;
	N--;
	for(j = i+1; !null(j); j = (j + 1) % M, N--)
	{
		v = st[j];
		st[j] = NULLitem;
		STinsert(v);
	}
}
