/**
 * Double hashing
 *
 * Double hashing is the same as linear probing except that we use a second hash
 * function to determine th search increment to use after each collision. The
 * search increment must be nonzero, and the table size and the search increment
 * should be relatively prime. The STdelete function for linear probing ( see
 * Program 14.5) does not work with double hashing, becuase any key might be in
 * many different probe sequences.
 **/

void STinsert(Item item)
{
	Key v = key(item);
	int i = hash(v, M);
	int k = hashtwo(v, M);
	while(!null(i))
	{
		i = (i+k) % M;
	}

	st[i] = item;
	N++;
}

Item STsearch(Key v)
{
	int i = hash(v, M);
	int k = hashtwo(v, M);
	while(!null(i))
	{
		if(eq(v, key(st[i])))
		{
			return st[i];
		}
		else
		{
			i = (i + k) % M;
		}
	}

	return NULLitem;
}
