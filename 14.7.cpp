/**
 * Dynamic hash insertion (for linear probing)
 *
 * This implmeenation of STinsert for linea rprobing (see Program 14.4) handles
 * an arbitrary number of keys by doubling the size of the table each time that
 * the table becomes half full. Doubling requires that we allocate memory for
 * the new table, rehash all the keys into the new table, then free the memory
 * for the old table. The function init is an internal version of STinit; the
 * ADT initialization STinit can be changed to start the table size M at 4 or
 * any larger value. This same approach can be used for double hashing or
 * separate chaining.
 **/

void expand();

void STinsert(Item item)
{
	Key v = key(item);
	int i = hash(v, M);
	while(!null(i))
	{
		i = (i + 1) % M;
	}

	st[i] = item;
	if(N++ >= M/2)
	{
		expand();
	}
}

void expand()
{
	int i;
	Item *t = st;
	init(M + M);

	for(i = 0; i < M/2; i++)
	{
		if(key(t[i]) != key(NULLitem))
		{
			STinsert(t[i]);
		}
	}

	free(t);
}
