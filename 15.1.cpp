/**
 * Binary digital search tree
 *
 * To develop a symbol-table implementation using DSTs, we modify the
 * implementations of search and insert in the standard BST implementation (see
 * Program 12.7) as shown in this implementation of search. Rather than doing a
 * full key comparison, we decide whether to move left or right on the basis of
 * testing a single bit (the leading bit) of the key. The recursive function
 * calls have a third argument so that we can move the bit position to be tested
 * to the right as we move down the tree. We use the digit operation to test
 * bits, as discussed in Section 10.1. These same changes apply to
 * implementation of insert; otherwise, we use all the code from Program 12.7.
 **/

Item searchR(link h, Key v, int w)
{
	Key t = key(h->item);
	if(h == z)
	{
		return NULLitem;
	}

	if(eq(v, t))
	{
		return h->item;
	}

	if(digit(v, w) == 0)
	{
		return searchR(h->l, v, w+1);
	}
	else
	{
		return searchR(h->r, v, w+1);
	}
}

item STsearch(Key v)
{
	return searchR(head, v, 0);
}
