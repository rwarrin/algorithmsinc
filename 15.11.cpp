/**
 * TST search for symbol-table ADT
 *
 * This implementation of search for TSTs (built with Program 15.10) is like
 * straight multiway-trie search, but we use only three, rather than R, links
 * per node. We use the digits of the key to travel down the tree, ending either
 * at a null link (search miss) or at a leaf that has a key that either is
 * (search hit) or is not (search miss) equal to the search key.
 **/

Item searchR(link h, Key v, int w)
{
	int i = digit(v, w);
	if(h == NULL)
	{
		return NULLitem;
	}

	if(internal(h))
	{
		if(i < h->d)
		{
			return searchR(h->l , v, w);
		}
		if(i == h->d)
		{
			return searchR(h->m, v, w+1);
		}
		if(i > h->d)
		{
			return searchR(h->r, v, w);
		}
	}

	if(eq(v, key(h->item)))
	{
		return h->item;
	}

	return NULLitem;
}

Item STsearch(Key v)
{
	return searchR(heads[digit(v, 0)], v, 1);
}
