/**
 * Trie search
 *
 * This function uses the bits of the key to control the branching on the way
 * down the trie, in the same way as in Program 15.1 for DSTs. There are three
 * possible outcomes: if the search reaches a leaf (with both links null), then
 * that is the unique node in the trie that could contain the record with key v,
 * so we test whether that node indeed contains v (search hit) or some key whose
 * leading bits match v (search miss). If the search reaches a null link, then
 * the parent's other link must not be null, so there is some other key in the
 * trie that differs from the search key in the corresponding bit, and we have a
 * search miss. This code assumes that the keys are distinct, and (if the keys
 * may be of different lengths) that no key is a prefix of another. The item
 * field is not used in non-leaf nodes.
 **/

Item searchR(link h, Key v, int w)
{
	if(h == z)
	{
		return NULLitem;
	}

	if((h->l == z) && (h->r == z))
	{
		return eq(v, key(h->item)) ? h->item : NULLitem;
	}

	if(digit(v, w) == 0)
	{
		return searchR(h->l, v, w+1);
	}
	else
	{
		return searchR(h->r, v, w+1);
	}
}

Item STsearch(Key v)
{
	return searchR(head, v, 0);
}
