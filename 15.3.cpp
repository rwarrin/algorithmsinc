/**
 * Trie insertion
 *
 * To insert a new node into a trie, we search as usual, then distinguish the
 * two cases that can occur for a search miss. If the miss was not on a leaf,
 * then we replace the null link that caused us to detect the miss with a link
 * to a new node, as usual. If the miss was on a leaf, then we use a function
 * split to make a new internal node for each bit position where the search key
 * and the key found agree, finishing with one internal node for th eletmost bit
 * position where the keys differ. The switch statement in split converts the 2
 * bits that it is testing int a number to handle the four possible cases. If
 * the bits are the same (case 00 = 0 or 11 = 3), then we continue splitting; if
 * the bits are different (case 01 = 1 or 10 = 2), then we stop splitting.
 **/

void STinit()
{
	head = (z = NEW(NULLitem, 0, 0, 0));
}

link split(link p, link q, int w)
{
	link t = NEW(NULLitem, z, z, z);
	switch(digit(p->item, w)*2 + digit(q->item, w))
	{
		case 0: {
			t->l = split(p, q, w+1);
		} break;
		case 1: {
			t->l = p;
			t->r = q;
		} break;
		case 2: {
			t->r = p;
			t->l = q;
		} break;
		case 3: {
			t->r = split(p, q, w+1);
		} break;
	}

	return t;
}

link insertR(linkh, Item item, int w)
{
	Key v = key(item);
	if(h == z)
	{
		return NEW(item, z, z, 1);
	}

	if((h->l == z) && (h->r == z))
	{
		return split(NEW(item, z, z, 1), h, w);
	}

	if(digit(v, w) == 0)
	{
		h->l = insertR(h->l, item, w+1);
	}
	else
	{
		h->r = insertR(h->r, item, w+1);
	}

	return h;
}

void STinsert(Item item)
{
	head = insertR(head, item, 0);
}
