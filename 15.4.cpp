/**
 * Patricia-trie search
 *
 * The recursive function searchR returns the unique node that could contain the
 * record with key v. It travels down the trie, using the bits of the tree to
 * control the search, but tests only 1 bit per node encountered -- the one
 * indicated in the bit field. It terminates the search when it encounters an
 * external link, one which points up the tree. The search function STsearch
 * calls searchR, then tests the key in that node to determine whether the
 * search is a hit or a miss.
 **/

Item searchR(link h, Key v, int w)
{
	if(h->bit <= w)
	{
		return h->item;
	}

	if(digit(v, h->bit) == 0)
	{
		return searchR(h->l, v, h->bit);
	}
	else
	{
		return searchR(h->r, v, h->bit);
	}
}

Item STsearch(key v)
{
	Item t = searchR(head->l, v, -1);
	return eq(v, key(t)) ? t : NULLitem;
}
