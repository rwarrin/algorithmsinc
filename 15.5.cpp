/**
 * Patricia-tree insertion
 *
 * To insert a key into a patricia trie, we begin with a search. The function
 * searchR from Program 15.4 gets us to a unique key in the tree that must be
 * distinguished from the key to be inserted. We determine the leftmost bit
 * position at which this key and the search key differ, then use the recursive
 * function insertR to travel down the tree and to insert a new node contaiing v
 * at that point.
 * In insertR, there are two cases, corresponding to the two cases illustrated
 * in Figure 15.11. The new node could replace an internal link (if the search
 * key differs from the key found in a bit position that was skipped), or an
 * external link (if the bit that distinguishes the search key from the found
 * key was not needed to distinguish the found key from all the other keys in
 * the trie).
 **/

void STinit()
{
	head = NEW(NULLitem, 0, 0, -1);
	head->l = head;
	head->r = head;
}

link insertR(link h, Item item, int w, link p)
{
	link x;
	Key v = key(item);
	if((h->bit >= w) || (h->bit <= p->bit))
	{
		x = NEW(item, 0, 0, w);
		x->l = digit(v, x->bit) ? h : x;
		x->r = digit(v, x->bit) ? x : h;
		return x;
	}

	if(digit(v, h->bit) == 0)
	{
		h->l = insertR(h->l, item, w, h);
	}
	else
	{
		h->r = insertR(h->r, item, w, h);
	}

	return h;
}

void STinsert(Item item)
{
	int i;
	Key v = key(item);
	Key t = key(searchR(head->l, v, -1));
	if(v == t)
	{
		return;
	}

	for(i = 0; digit(v, i) == digit(t, i); i++) ;
	head->l = insertR(head->l, item, i, head);
}
