/**
 * Patricia-trie sort
 *
 * This recurisve procedure visits the records in a patricia trie in order of
 * their keys. We imagine the itmes to be in (virtual) external nodes, which we
 * can identify by testing when the bit index on the current node is not larger
 * than the bit index on its parent. Otherwise, this program is a standard
 * inorder traversal.
 **/

void sortR(link h, void (*visit)(item), int w)
{
	if(h->bit <= w)
	{
		visit(h->item);
		return;
	}

	sortR(h->l, visit, h->bit);
	sortR(h->r, visit, h->bit);
}

void STsort(void (*visit)(Item))
{
	sortR(head->l, visit, -1);
}
