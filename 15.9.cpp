/**
 * Partial-match searching in TSTs
 *
 * With judicious use of multiple recursive calls, we can find close matches in
 * a TST structure, as shown in this program for printing all strings in the
 * data structure that match a search string with some character unspecified
 * (indicated by asterisks). We are not implementing a search ADT function or
 * using abstract items here, so we use explicit C string-processing primitives.
 **/

char word[maxW];

void matchR(link h, char *v, int i)
{
	if(h == z)
	{
		return;
	}

	if((*v == '\0') && (h->d == '\0'))
	{
		word[i] = h->d;
		printf("%s ", word);
	}

	if((*v == '*') || (*v == h->d))
	{
		word[i] = h->d;
		matchR(h->m, v+1, i+1);
	}

	if((*v == '*') || (*v < h->d))
	{
		matchR(h->l, v, i);
	}

	if((*v == '*') || (*v > h->d))
	{
		matchR(h->r, v, i);
	}
}

void STmatch(char *v)
{
	matchR(head, v, 0);
}

