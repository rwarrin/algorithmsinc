/**
 * B-tree definitions and initialization
 *
 * Each node in a B tree contains an array and a count of the number of active
 * entries in the array. In internal nodes, each array entry is a key and a link
 * to a node; in external nodes, each array entry is a key and an item. The C
 * union construct allows us to specify these options in a single declaration.
 * We initialize new nodes to be empty (count field set to 0), with a sentinel
 * key in array entry 0. An empty B tree is a link to an empty node. Also we
 * maintain variables to track the number of items in the tree and the height of
 * the tree, both initialized to 0.
 **/

typedef struct STnode* link;
typedef struct
{
	Key key;
	union
	{
		link next;
		Item item;
	} ref;
} entry;

struct STnode
{
	entry b[M];
	int m;
};

static link head;
static int H, N;

link NEW()
{
	link x = malloc(sizeof(*x));
	x->m = 0;
	return x;
}

void STinit(int maxN)
{
	head = NEW();
	h = 0;
	N = 0;
}
