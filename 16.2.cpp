/**
 * B-tree search
 *
 * This implemenation of search for B trees is based on a recursive function, as
 * usual. For internal nodes (positive height), we scan to find the first key
 * larger than the search key, and do a recursive call on the subtree referenced
 * by the previous link. For external nodes (height 0), we scan to see whether
 * or not there is an item with key equal to the search key.
 **/

Item searchR(link h, Key v, int H)
{
	int j;
	if(H == 0)
	{
		for(j = 0; j < h->m; j++)
		{
			if(eq(v, h->b[j].key))
			{
				return h->b[j].ref.item;
			}
		}
	}

	if(H != 0)
	{
		for(j = 0; j < h->m; j++)
		{
			if((j+1 == h->m) || less(v, h->b[j+1].key))
			{
				return searchR(h->b[j].ref.next, v, H-1);
			}
		}
	}

	return NULLitem;
}

Item STsearch(Key v)
{
	return searchR(head, v, H);
}
