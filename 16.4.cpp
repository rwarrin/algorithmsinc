/**
 * B-tree node split
 *
 * To split a node in a B tree, we create a new node, move the larger half of the
 * keys to the new node, and then adjust counts and set sentinel keys in the
 * middle of both nodes. This code assumes that M is even, and uses an extra
 * position in each node for the item that causes the split. That is, the
 * maximum number of keys in a node is M-1, and when a node gets M keys, we
 * split it into two nodes with M/2 keys each.
 **/

link split(link h)
{
	int j;
	link t = NEW();
	for(j = 0; j < M/2; j++)
	{
		t->b[j] = h->b[M/2+j];
	}

	h->m = M/2;
	t->m = M/2;
	return t;
}
