/**
 * Extendible hashing definitions and initialization
 *
 * An extendible hash table is a directory of references to pages (link external
 * nodes in B trees) that contain up to 2M items. Each page also contains a
 * count (m) of the number of items on the page, and an integer (k) that
 * speifies the number of leading bits for which we know the keys of the items
 * to be identical. As usual, N specifies th enumber of items in the table. The
 * variable d specifies the number of bits that we use to index into the
 * directory, and D is the number of directory entries, so D=2^d. The table is
 * initially set to a directory of size 1, which points to an empty page.
 **/

typedef struct STnode* link;
struct STnode
{
	Item b[M];
	int m;
	int k;
};

static link *dir;
static int d, D, N;

link New()
{
	link x = malloc(sizeof(*x));
	x->m = 0;
	x->k = 0;
	return x;
}

void STinit(int maxN)
{
	d = 0;
	N = 0;
	D = 1;
	dir = malloc(sizeof(*dir) * D);
	dir[0] = NEW();
}
