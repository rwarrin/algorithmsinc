/**
 * Extendible hashing search
 *
 * Searching in an extendible hashing table is simply a matter of using the
 * leading bits of the key to index into the directory, then doing a sequential
 * search on the specified page for an item with a key equal to the search key.
 * The only requirement is that each directory entry refer to a page that is
 * guaranteed to contain all items in the symbol talbe that begin with the
 * specified bits.
 **/

Item search(link h, Key v)
{
	int j;
	for(j = 0; j < h->m; j++)
	{
		if(eq(v, key(h->b[j])))
		{
			return h->b[j];
		}
	}

	return NULLitem;
}

Item STsearch(Key v)
{
	return search(dir[bits(v, 0, d)], v);
}
