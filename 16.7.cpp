/**
 * Extendible hashing insertion
 *
 * To insert an item into an extendible hash table, we search; then we insert
 * the item on the specified page; then we split the page if the insertion
 * caused overflow. The general scheme is the same as that for B trees, but the
 * methods that we use to find the appropriate page and to split pages are
 * different.
 * The split function creates a new ndoe, then examines the kth bit (counting
 * from the left) of each item's key: if the bit is 0, the item stays in the old
 * node; if it is 1, it goes in the new node. The value k+1 is assigned to the
 * "leading bits known to be identical" field of both nodes after the split. If
 * this process does not result in at least one key in each node we split again,
 * until the items are so separated. At the end, we insert a pointer with the
 * new node into the directory
 **/

link split(link h)
{
	int j;
	link t = NEW();

	while(h->m == 0 || h->m == M)
	{
		h->m = 0;
		t->m = 0;
		for(j = 0; j < M; j++)
		{
			if(bits(j->b[j], h->k, 1) == 0)
			{
				h->b[(h->m)++] = h->b[j];
			}
			else
			{
				t->b[(t->m)++] = h->b[j];
			}
		}
		t->k = ++(h->k);
	}

	insertDIR(t, t->k);
}

void insert(link h, Item item)
{
	int i, j;
	Key v = key(item);
	for(j = 0; j < h->m; j++)
	{
		if(less(v, key(h->b[j])))
		{
			break;
		}

		for(i = (h->m)++; i > j; i--)
		{
			h->b[i] = h->b[i-1];
		}
		h->b[j] = item;

		if(h->m == M)
		{
			split(h);
		}
	}
}

void STinsert(Item item)
{
	insert(dir[bits(key(item), 0, d)], item);
}
