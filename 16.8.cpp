/**
 * Extendible-hashing directory insertion
 *
 * This deceptively simple code is at the heart of the extendible-hashing
 * process. We are given a link t to a node that carries items that match in the
 * first k bits, which is to be incorporated into the directory. In the simplest
 * case, where d and k are equal, we just put t into d[x], where x is the value
 * of the first d bits of t->b[0] (and of all the other items on the page). If k
 * is greater than d, we have to double the size of the directory, until
 * reducing to the case where d and k are equal. if k is less than d, we need to
 * set more than one pointer -- the first for loop calculates the number of
 * pointers that we need to set (2^d-k), and the second for loop does the job.
 **/

void insertDIR(link t, int k)
{
	int i, m, x = bits(t->b[0], 0, k);
	while(d < k)
	{
		link *old = dir;
		d += 1;
		D += D;

		dir = malloc(D * (sizeof(*dir)));
		for(i = 0; i < D; i++)
		{
			dir[i] = old[i/2];
		}

		if(d < k)
		{
			dir(bits(x, 0, d) ^ 1) = NEW();
		}
	}
	for(m = 1; k < d; k++)
	{
		m *= 2;
	}

	for(i = 0; i < m; i++)
	{
		dir[x*m+i] = t;
	}
}
