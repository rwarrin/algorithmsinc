/**
 * Graph ADT interface
 *
 * This interface is a starting point for implementing and testing graph
 * algorithms. Throughout this and the next several chapters, we shall add
 * functions to this interface for solving various graph-processing problems.
 * Various assumptions that simplify the code and other issues surrounding the
 * design of a general-purpose graph-processing interface are discussed in the
 * ntext.
 * The interface defines two data types: a simple Edge data type, including a
 * constructor function EDGE tha tmakes an Edge from two vertices; and a Graph
 * data type, which is defined with the standard representation-independent
 * construction from Chapter 4. The basic operations that we use to process
 * graphs are ADT functions to create, copy, an ddestroy them; to add and delete
 * edges; and to extract and edge list.
 **/

typedef struct
{
	int v;
	int w;
} Edge;

Edge EDGE(int, int);

typedef struct graph *Graph;
Graph GRAPHinit(int);
void GRAPHinsertE(Graph, Edge);
void GRAPHRemoveE(Graph, Edge);
int GRAPHedges(Edge[], Graph G);
Graph GRAPHcopy(Graph);
void GRAPHdestroy(Graph);
