/**
 * Simple path search (adjacency matrix)
 *
 * The function GRAPHpath tests for the existence of a simple path connecting
 * two given vertices. It uses a recursive depth-first sarch function pathR,
 * which, given two vertices v an dw, checks each edge v-t adjacent to v to see
 * whether it could be the first edge on a path to w. The vertext-indexed array
 * visited keeps the function from revisiting any vertex, and therefore keeps
 * paths simple.
 * To have the function print the edges of the path (in reverse order), add the
 * statement printf("%d-%d ", t, v); just before the return 1 near the end of
 * PathR (see text).
 **/

static int visited[maxV];

int pathR(Graph G, int v, int w)
{
	int t;
	if(v == w)
	{
		return 1;
	}

	visited[v] = 1;

	for(t = 0; t < G->V; t++)
	{
		if(G->adj[v][t] == 1)
		{
			if(visited[t] == 0)
			{
				if(pathR(G, t, w))
				{
					return 1;
				}
			}
		}
	}

	return 0;
}

int GRAPHpath(Graph G, int v, int w)
{
	int t;
	for(t = 0; t < G->V; t++)
	{
		visited[t] = 0;
	}

	return pathR(G, v, w);
}
