/**
 * Hamilton path
 *
 * The function GRAPHpathH searches for a Hamilton path from v to w. It uses a
 * recursive function that differs from the one in Program 17.11 in just two
 * respects: First, it returns successfully only if it finds a path of length V;
 * second, it resets the visited marker before returning unsuccessfully. _Do not
 * expect this function to finish except for tiny graphs (see text)._
 **/

static int visited[maxV];

int pathR(Graph G, int v, int w, int d)
{
	int t;
	if(v == w)
	{
		if(d == 0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	visited[v] = 1;

	for(t = 0; t < G->V; t++)
	{
		if(G->adj[v][t] == 1)
		{
			if(visited[t] == 0)
			{
				if(pathR(g, t, w, d-1))
				{
					return 1;
				}
			}
		}
	}

	visited[v] = 0;
	return 0;
}

int GRAPHpathH(Graph G, int v, int w)
{
	int t;
	for(t = 0; t < G->V; t++)
	{
		visited[t] = 0;
	}

	return pathR(G, v, w, G->V - 1);
}
