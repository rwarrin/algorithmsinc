/**
 * Euler path existence
 *
 * This function which is based upon the corollary to Property 17.4, tests
 * whether there is an Euler path from v to w in a connected graph, using the
 * GRAPHdeg ADT function from Exercise 17.40. It takes time proportional to V,
 * not including preprocessing time to check connectivity and to build the
 * vertext-degree table used by GRAPHdeg.
 **/

int GRAPHpathE(Graph G, int v, int w)
{
	int t;
	t = GRAPHdeg(G, v) + GRAPHdeg(G, w);

	if((t % 2) != 0)
	{
		return 0;
	}

	for(t = 0; t < G->V; t++)
	{
		if((t != v) && (t != w))
		{
			if((GRAPHdeg(G, t) % 2) != 0)
			{
				return 0;
			}
		}
	}

	return 1;
}
