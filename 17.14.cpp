/**
 * Linear-time Euler path (adjacency lists)
 *
 * The function pathEshow prints an Euler path between v and w. With a
 * constant-time implementation of GRAPHremoveE (see Exercise 17.44), it runs in
 * linear time. The auxiliary function path follows and removes edges on a
 * cyclic path and pushes vertices onto a stack, to be checked for side loops
 * (see text). The main loop calls path as long as there are vertices with side
 * loops to traverse.
 **/

#include "STACK.h"

int path(Graph G, int v)
{
	int w;
	for(; G->adj[v] !=NULL; v = w)
	{
		STACKpush(v);
		w = G->adj[v]->v;
		GRAPHremoveE(G, EDGE(v, w));
	}

	return v;
}

void pathEshow(Graph G, int v, int w)
{
	STACKinit(G->E);
	printf("%d", w);
	while((path(G, v) == v) && !STACKempty())
	{
		v = STACKpop();
		printf("-%d", v);
	}
	printf("\n");
}
