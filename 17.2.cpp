/**
 * Example of a graph-processing client
 *
 * This program takes V and E from standard input, generates a random graph with
 * V vertices and E edges, prints the graph if it is small, and computes (and
 * prints) the number of connected components. It uses the ADT functions
 * GRAPHrand (see Program 17.8), GRAPHshow (see Exercise 17.16), and GRAPHcc
 * (see Program 18.4).
 **/

#include <stdio.h>
#include "GRAPH.h"

int main(int argc, char *argv[])
{
	int V = atoi(argv[1]), E = atoi(argv[2]);
	Graph G = GRAPHrand(V, E);
	if(V < 20)
	{
		GRAPHshow(G);
	}
	else
	{
		printf("%d vertices, %d edges, ", V, E);
	}
	printf("%d component(s)\n", GRAPHcc(G));

	return 0;
}
