/**
 * Graph ADT implementation (adjacence matrix)
 *
 * This implementation of the interface in Program 17.1 uses a two-dimensional
 * array. An implementation of the function MATRIXint, which allocates memory
 * for the array and initializes it, is given in Program 17.4. The rest of the
 * code is straightforward:  An edge i-j is present in the graph if and only if
 * a[i][j] and a[j][i] are both 1. Edges are inserted and removed in constant
 * time, and duplicate edges are silently ignored. Initialization and extracting
 * all edges each take time proportional to v^2.
 **/

#include <stdlib.h>
#include "GRAPH.h"


struct graph
{
	int V;
	int E;
	int **adj;
};

Graph GRAPHinit(int V)
{
	Graph G = malloc(sizeof(*G));
	G->V = V;
	G->E = 0;
	G->adj = MATRIXint(V, V, 0);

	return G;
}

void GRAPHinsertE(Graph G, Edge e)
{
	int v = e.v; w = e.w;

	if(G->adj[v][w] == 0)
	{
		G->E++;
	}

	G->adj[v][w] = 1;
	G->adj[w][v] = 1;
}

void GRAPHremoveE(Graph G, Edge e)
{
	int v = e.v, w = e.w;
	if(G->adj[v][w] == 1)
	{
		G->E--;
	}

	G->adj[v][w] = 0;
	G->adj[w][v] = 0;
}

int GRAPHedges(Edge a[], Graph G)
{
	int v, w, E = 0;

	for(v = 0; v < G->V; v++)
	{
		for(w = v+1; w < G->V; w++)
		{
			if(G->adj[v][w] == 1)
			{
				a[E++] = EDGE(v, w);
			}
		}
	}

	return E;
}
