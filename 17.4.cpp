/**
 * Adjacency-matrix allocation and initialization
 *
 * This program uses the standard C array-of-arrays representation for the
 * two-dimensional adjacency matrix (see Section 3.7). It allocates r rows with
 * c integers each, then initializes all entries to the value val. The call to
 * create a matrix that represents a V-vertex graph with no edges. For small V,
 * the cost of V calls to malloc might predominate.
 **/

int **MATRIXint(int r, int c, int val)
{
	int i, j;
	int **t = malloc(r * sizeof(int *));
	for(i = 0; i < r; i++)
	{
		t[i] = malloc(c * sizeof(int));
	}

	for(i = 0; i < r; i++)
	{
		for(j = 0; j < c; j++)
		{
			t[i][j] = val;
		}
	}

	return t;
}
