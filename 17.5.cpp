/**
 * Graph ADT output(adjacency-lists format)
 *
 * Printing out the full adjacency matrix is unwieldy for sparse graphs, so we
 * might chose to simply print out, for each vertex, the vertices that are
 * connected to that vertex by an edge.
 **/

void GRAPHshow(Graph G)
{
	int i, j;
	printf("%d vertices, %d edges\n", G->V, G->E);
	for(i = 0; i < G->V; i++)
	{
		printf("%2d:", i);
		for(j = 0; j < G->V; j++)
		{
			if(G->adj[i][j] == 1)
			{
				printf(" %2d", j);
			}
		}
		printf("\n");
	}
}
