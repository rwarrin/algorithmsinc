/**
 * Random graph generator (random edges)
 *
 * This ADT function builds a graph by generating E random pairs of integers
 * between 0 and V-1, interpreting the integers as vertex labels and the pairs
 * of vertex labels and edges. It leaves the decision about the treatment of
 * parallel edges and self-loops to the implementation of GRAPHinsertE and
 * assumes that the ADT implementation maintains counts of the number of edges
 * and vertices in G->E and G->V, respectively. This method is generally not
 * ssuitable for generating huge dense graphs because of the number of parallel
 * edges that it generates.
 **/

int randV(Graph G)
{
	return G->V * (rand() / (RAND_MAX + 1.0));
}

Graph GRAPHrand(int V, int E)
{
	Graph G = GRAPHinit(V);
	while(G->E < E)
	{
		GRAPHinsertE(G, EDGE(randV(G), randV(G)));
	}

	return G;
}
