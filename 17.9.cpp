/**
 * Building a graph from pairs of symbols
 *
 * This function uses a symbol table to build a graph by reading pairs of
 * symbols from standard input. The symbol-table ADT function STindex associates
 * an integer with each symbol: on unsuccessful search in a table of size N it
 * adds the symbol to the table with associated integer N+1; on successful
 * search, it simply returns the integer previously associated with the symbol.
 * Any of the symbol-tabl emethods in Part 4 can be adapted for this use; for
 * example, see Program 17.10. The code to check that the number of edges does
 * not exceed Emax is commited.
 **/

#include <stdio.h>
#include "GRAPH.h"
#include "ST.h"

Graph GRAPHscan(int Vmax, int Emax)
{
	char v[100], w[100];
	Graph G = GRAPHinit(Vmax);
	STinit();
	while(scanf("%99s %99s", v, w) == 2)
	{
		GRAPHinsertE(G, EDGE(STindex(v), STindex(w)));
	}

	return G;
}
