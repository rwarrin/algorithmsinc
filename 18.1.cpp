/**
 * Depth-first search (adjacency-matrix)
 *
 * This code is intended for use with a generic graph-search ADT function that
 * initializes a counter cnt to 0 and all of the entries in the vertex-index
 * array pre to -1, then calls search once for each connected component (see
 * Program 18.3), assuming that the call search(G, EDGE(v, v)) marks all
 * vertices in the same connected component as v (by setting their pre entries
 * to be nonnegative).
 * Here, we implement search with a recursive function dfsR that visits all the
 * vertices connected to e.w by scanning through its row in the adjacency matrix
 * and calling itself for each edge that leads to an unmarked vertex.
 **/

#define dfsR search

foid dfsR(Graph G, Edge e)
{
	int t, w = e.w;
	pre[w] = cnt++;
	for(t = 0; t < G->V; t++)
	{
		if(G->Adj[w][t] != 0)
		{
			if(pre[t] == -1)
			{
				dfsr(G, EDGE(w, t));
			}
		}
	}
}
