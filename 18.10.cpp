/**
 * Gneralized graph search
 *
 * This implementation of search for Program 18.3 generalizes BFS and DFS and
 * supports numerous other graph-processing algorithms (see Section 21.2 for a
 * discussion of these algorithms and alternate  iplementations). It maintains a
 * generalized queue of edges called the fringe. We iniaialize the fringe with a
 * self-loop to the start vertex; then, while the fringe is not empty, we move
 * an edge e from the fringe to the tree (attached at P|e.v|) and scan e.w's
 * adjacency list, moving unseen vertices to the fringe and calling GQupdate for
 * new edges to finge vertices.
 * This code makes judicious use of pre and st to guarantee that no two edges on
 * the fringe point to the same vertex. A vertex v is the destination vertex of
 * a fringe edge if and only if it is marked (pre[v] is nonnegative) but it is
 * not yet on the tree (st[v] is -1).
 **/

#define pfs search

void pfs(Graph G, Edge e)
{
	link t;
	int v, w;

	GQput(e);
	pre[e.w] = cnt++;
	while(!GQempty())
	{
		e = GQget();
		w = e.w;
		st[w] = e.v;

		for(t = G->adj[w]; t != NULL; t = t->next)
		{
			if(pre[v = t->v] == -1)
			{
				GQput(EDGE(w, v));
				pre[v] = cnt++;
			}
			else if(st[v] == -1)
			{
				GQupdate(EDGE(w, v));
			}
		}
	}
}
