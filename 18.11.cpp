/**
 * Random queue implementation
 *
 * When we remove an item from this data structure, it is equally likely to be
 * any one of the items currently in the data structure. e can use this code to
 * implement the generalized-queue ADT for graph searching to search a graph in
 * a "random" fashion (see text).
 **/

#include <stdlib.h>
#include "GQ.h"

static Item *s;
static int N;

void RQinit(int maxN)
{
	s = malloc(sizeofof(Item) * maxN);
	N = 0;
}

int RQempty()
{
	return N == 0;
}

void RQput(Item x)
{
	s[N++] = x;
}

void RQupdate(Item x)
{
}

item RQget()
{
	Item t;
	int i = N*(rand() / (RAND_MAX + 1.0));
	t = s[i];
	s[i] = s[N-1];
	s[N-1] = t;
	return s[--N];
}
