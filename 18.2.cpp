/**
 * Depth-first search (adjacency-lists)
 *
 * This implemenation of dfsR is DFS for graphs represented with adjacency
 * lists. The algorithm is the same as for the adjacency-matrix representation
 * (Program 18.1): to visit a vertex, we mark it and then scan through its
 * incident edges, making a recursive call whenever we encounter an edge to an
 * unmarked vertex.
 **/

void dfsR(Graph G, Edge e)
{
	link t;
	int w = e.w;
	pre[w] = cnt++;

	for(t = G->adj[w]; t != NULL; t = t->next)
	{
		if(pre[t->v] == -1)
		{
			dfsR(G, EDGE(w, t->v));
		}
	}
}
