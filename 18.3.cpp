/**
 * Graph search
 *
 * We typically use code like this to process graphs that may not be connected.
 * The function GRAPHsearch assumes that search, when called with a self-loop to
 * v as its second argument, sets the pre entries corresponding to each vertex
 * connected to v to nonnegative values. Under this assumption, this
 * implementation calls search once for each connected component in the graph --
 * we can use it for any graph representation and any search implementation.
 * Together with Program 18.1 or Program 18.2 this code computes the order in
 * which vertices are visited in pre. Other DFS-based implementations do other
 * computations, but use the same general scheme of interpreting nonnengative
 * entries in a vertext-indexed array as vertex marks.
 **/

static int cnt;
static int pre[maxV];

void GRAPHsearch(Graph G)
{
	int v;
	cnt = 0;

	for(v = 0; v < G->V; v++)
	{
		pre[v] = -1;
	}

	for(v = 0; v < G->V; v++)
	{
		if(pre[v] == -1)
		{
			search(G, EDGE(v, v));
		}
	}
}
