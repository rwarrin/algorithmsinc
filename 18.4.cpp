/**
 * Graph connectivity (adjacency lists)
 *
 * The DFS function GRAPHcc computes, in linear time, the number of connected
 * components in a graph and stores a component index associated with each
 * vertex in the vertex-indexed array G->cc in the graph represenation. (Since
 * it does not need structural information, the recursive function uses a vertex
 * as its second argument instead of an edge as in Program 18.2). After calling
 * GRAPHcc, clients can test wheter any pair of vertices are connected in
 * constant time(GRAPHconnect).
 **/

void dfsRcc(Graph G, int v, int id)
{
	link t;
	G->cc[v] = id;
	for(t = G->adj[v]; t != NULL; t = t->next)
	{
		if(G->cc[t->v] == -1)
		{
			dfsRcc(G, t-v, id);
		}
	}
}

int GRAPHcc(Graph G)
{
	int v;
	int id = 0;

	G->cc = malloc(G->V * sizeof(int));
	for(v = 0; v < G->V; v++)
	{
		G->cc[v] = -1;
	}

	for(v = 0; v < G->V; v++)
	{
		if(G->cc[v] == -1)
		{
			dfsRcc(G, v, id++);
		}
	}

	return id;
}

int GRAPHconnect(Graph G, int s, int t)
{
	return G->cc[s] == G->cc[t];
}
