/**
 * Edge connectivity (adjacency lists)
 *
 * This recursive DFS functin prints and counts the bridges in a graph. It
 * assumes that Program 18.3 is augmented with a counter bcnt and a
 * vertex-indexed array low that are initialized in the same way as cnt and pre,
 * respectively. The low array keeps track of the lowest preorder number that
 * can be reached from each vertex by a sequence of tree edges followed by one
 * back edge.
 **/

void bridgeR(Graph G, Edge e)
{
	link t;
	int v;
	int w = e.w;

	pre[w] = cnt++;
	low[w] = pre[w];

	for(t = G->adj[w]; t != NULL; t = t->next)
	{
		if(pre[v = t->v] == -1)
		{
			bridgeR(G, EDGE(w, v));
			if(low[w] > low[v])
			{
				low[w] = low[v];
			}
			if(low[v] == pre[v])
			{
				bcnt++;
				printf("%d-%d\n", w, v);
			}
		}
		else if(v != e.v)
		{
			if(low[w] > pre[v])
			{
				low[w] = pre[v];
			}
		}
	}
}
