/**
 * Breadth-first search (adjacency matrix)
 *
 * This implementation of search for the adjacency-matrix representation is
 * breadth-first search (BFS). To vist a vertex, we scan through its incident
 * edges, putting any edges to unvisited vertices onto the queue of vertices to
 * be visited.
 * We mark the nodes in the order they are visited in a vertex-indexed array pre
 * and build an explicit parent-link representation of the BFS tree (the edges
 * that first take us to each node) in another vertex indexed array st. This
 * code assumes that we add a call to QUEUEinit (and code to declare and
 * initialize st) to GRAPHsearch in Program 18.3.
 **/

#define bfs search

void bfs(Graph G, Edge e)
{
	int v, w;

	QUEUEput(e);
	while(!QUEUEempty())
	{
		if(pre[(e = QUEUEget()).w] == -1)
		{
			pre[e.w] = cnt++;
			st[e.w] = e.v;
			for(v = 0; v < G->V; v++)
			{
				if(G->adj[e.w][v] == 1)
				{
					if(pre[v] == -1)
					{
						QUEUEput(EDGE(e.w, v));
					}
				}
			}
		}
	}
}

