/**
 * Improved BFS
 *
 * To guarantee that the queue that we use during BFS has at most V entries, we
 * mark the vertices as we put them on the queue.
 **/

void bfs(Graph G, Edge e)
{
	int v, w;

	QUEUEput(e);
	pre[e.w] = cnt++;
	while(!QUEUEempty())
	{
		e = QUEUEget();
		w = e.w;
		st[w] = e.v;

		for(v = 0; v < G->V; v++)
		{
			if((G->adj[w][v] == 1) &&
			   (pre[v] == -1))
			{
				QUEUEput(EDGE(w, v));
				pre[v] = cnt++;
			}
		}
	}

}
