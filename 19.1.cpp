/**
 * Reversing a digraph (adjacency lists)
 *
 * Given a digraph represented with adjacency lists, this function createa a new
 * adjacency-lists representation of a digraph that has the same vertices and
 * edges but with the edge directions reversed.
 **/

Graph GRAPHreverse(Graph G)
{
	int v;
	link t;
	Graph R = GRAPHinit(G->V);

	for(v = 0; v < G->V; v++)
	{
		for(t = G->adj[v]; t != NULL; t = t->next)
		{
			GRAPHinsertE(R, EDGE(t->v, v));
		}
	}

	return R;
}
