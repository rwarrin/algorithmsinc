/**
 * Strong components (Kosaraju's algorithm)
 *
 * This implementation finds the strong components of a digraph represented with
 * adjacency lists. Like our solutions to the connectivity problem for
 * undirected graphs in Section 18.5, it sets values in the vertex-indexed array
 * sc such that the entries corresponding to any pair of vertices are equal if
 * and only if they are in the same stron gcomponent.
 * First, we build the reverse digraph and do a DFS to compute a postorder
 * permutation. Next, we do a DFS of the original digraph, using the reverse of
 * the postorder from the first DFS in the search loop that calls the recursive
 * function. Each recursive call in the second DFS vists all the vertices in a
 * stron gcomponent.
 **/

static int post[maxV];
static int postR[maxV];

void SCdfsR(Graph G, int w)
{
	link t;
	G->sc[w] = cnt1;
	for(t = G->adj[w]; t != NULL; t = t->next)
	{
		if(G->sc[t->v] == -1)
		{
			SCdfsR(G, t->v);
		}
	}

	post[cnt0++] = w;
}

int GRAPHsc(Graph G)
{
	int v;
	Graph R;

	cnt0 = 0;
	cnt1 = 0;
	for(v = 0; v < G->V; v++)
	{
		R->sc[v] = -1;
	}

	for(v = 0; v < G->V; v++)
	{
		if(r->sc[v] == -1)
		{
			SCdfsR(R, v);
		}
	}

	cnt0 = 0;
	cnt1 = 0;
	for(v = 0; v < G->V; v++)
	{
		G->sc[v] = -1;
	}

	for(v = 0; v < G->V; v++)
	{
		postR[v] = post[v];
	}

	for(v = G->V-1; v >= 0; v--)
	{
		if(G->sc[postR[v]] == -1)
		{
			SCdfsR(G, postR[v]);
			cnt1++;
		}
	}

	GRAPHdestroy(R);
	return cnt1;
}

int GRAPHstrongreach(Graph G, int s, int t)
{
	return G->sc[s] == G->sc[t];
}
