/**
 * Stron gcomponents (Tarjan's algorithm)
 *
 * With this implementation for the recursive DFS function, a standard
 * adjacency-lists digraph DFS will result in strong components being identified
 * in the vertex-indexed array sc, according to our conventions.
 * We use a stack s (with stack pointer N) to hold each vertex until dtermining
 * that all the vertices down to a certain point at the top of the stack belong
 * to the same strong component. The vertex-indexed array low keeps track of the
 * lowest preorder number reachable via a series of down links followed by one
 * up link from each node (see text).
 **/

void SCdfsR(Graph G, int w)
{
	link t;
	int v;
	int min;

	pre[w] = cnt0++;
	low[w] = pre[w];
	min = low[w];
	s[N++] = w;

	for(t = G->adj[w]; t != NULL; t = t->next)
	{
		if(pre[t->v] == -1)
		{
			SCdfsR(G, t->v);
		}

		if(low[t->v] < min)
		{
			min = low[t->v]
		}
	}

	if(min < low[w])
	{
		low[w] = min;
		return;
	}

	do
	{
		G->sc[(v = s[--N])] = cnt1;
		low[v] = G->V;
	} while(s[N] != w);
	cnt1++;
}
