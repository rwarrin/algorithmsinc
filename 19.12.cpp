/**
 * Strong components (Gabow's algorithm)
 *
 * This program performs the same computation as Program 19.11, but uses a
 * second stack path insead of the vertex-indexed array low to decide when to
 * pop the vertices in each strong component from the main stack (see text).
 **/

void SCdfsR(Graph G, int w)
{
	link t;
	int v;

	pre[w] = cnt0++;
	s[N++] = w;
	path[p++] = w;

	for(t = G->adj[w]; t != NULL; t = t->next)
	{
		if(pre[t->v] == -1)
		{
			SCdfsR(G, t->v);
		}
		else if(G->sc[t->v] == -1)
		{
			while(pre[path[p-1]] > pre[t->v])
			{
				p--;
			}
		}
	}

	if(path[p-1] != w)
	{
		return;
	}
	else
	{
		p--;
	}

	do
	{
		G->sc[s[--N]] = cnt1;
	} while(s[N] != w);

	cnt1++;
}
