/**
 * Strong-component-based transitive closure
 *
 * This program computes the abstract transitive closure of a digraph by
 * computing its strong components, kernel DAG< and the transitive closure of
 * the kernel DAG (see Program 19.9). The vertex-indexed array sc gives the
 * strong component index for each vertex, or its corresponding vertex index in
 * the kernel DAG. A vertex t is reachable from a vertex x if and only if sc[t]
 * is reachable from sc[s] in the kernel DAG.
 **/

DAG K;
void GRAPHtc(Graph G)
{
	int v, w;
	link t;
	int *sc = G->sc;

	K = DAGinit(GRAPHsc(G));
	for(v = 0; v < G->V; v++)
	{
		for(t = G->adj[v]; t != NULL; t = t->next)
		{
			DAGinsertE(K, dagEDGE(sc[v], sc[t->v]));
		}
	}

	DAGtc(K);
}

int GRAPHreach(Graph G, int s, int t)
{
	return DAGreach(K, G->sc[s], G->sc[t]);
}
