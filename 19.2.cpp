/**
 * DFS of a digraph
 *
 * This DFS function for digraphs represented with adjacency lists in instrumted
 * to show the role that each edge in the graph plays in the DFS. It assumes
 * that Program 18.3 is augmented to declare and initialize the array post and
 * the counter cntP in the same way as pre and cnt, respectively. See Figure
 * 19.10 for sample output and a discussion about implementing show.
 **/

void dfsR(Graph G, Edge e)
{
	link t;
	int i, v, w = e.w;
	Edge x;

	show("tree", e);

	pre[w] = cnt++;
	for(t = G->adj[w]; t != NULL; t = t->next)
	{
		if(pre[t->v] == -1)
		{
			dfsR(G, EGE(w, t->v));
		}
		else
		{
			v = t->v;
			x = EDGE(w, v);
			if(post[v] == -1)
			{
				show("back", x);
			}
			else if(pre[v] > pre[w])
			{
				show("down", x);
			}
			else
			{
				show("cross", x);
			}
		}
	}
	post[w] = cntP++;
}
