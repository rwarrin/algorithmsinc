/**
 * DFS-based transitive closure
 *
 * This program is functionally equivalent to Program 19.3. It computes the
 * transitive closure by doing a separate DFS starting at each vertex to compute
 * its set of reachable nodes. Each call on the recusirve procedure addsd an
 * edge from the start vertex and makes recursive calls to fill the
 * corresponding row in the transitive-closure matirx. The matrix also serves to
 * mark the vistied vertices during the DFS.
 **/

void TCdfsR(Graph G, Edge e)
{
	link t;
	G->tc[e.v][e.w] = 1;
	for(t = G->adj[e.w]; t != NULL; t = t->next)
	{
		if(G->tc[e.v][t->v] == 0)
		{
			TCdfsR(G, EDGE(e.v, t->v));
		}
	}
}

void GRAPHtc(GRAPH G, Edge e)
{
	int v, w;
	G->tc = MATRIXint(G->V, G->V, 0);
	for(v = 0; v < G->V; v++)
	{
		TCdfsR(G, EDGE(v, v));
	}
}

int GRAPHreach(Graph G, int s, int t)
{
	return G->tc[s][t];
}
