/**
 * Representing a binary tree with a binary DAG
 *
 * This recursive program is a postorder walk that constructs a compact
 * representation of a binary DAG corresponding to a binary tree structure (see
 * Chapter 12) by identifying common subtrees. We use an index function like
 * STindex in Program 17.10, modified to accept integer instead of string keys,
 * to assign a unique integer to each distinct tree structure for use in
 * representing the DAG as an array of 2-integer structures (see Figure 19.20).
 * The empty tree  (null link) is assigned index 0, the single-node tree (node
 * with two null links) is assigned index 1, and so forth.
 * We compute the index corresponding to each subtree, recursively. Then we
 * create a key such that any node with the same subtrees will have the same
 * index and return that index after filling in the DAG's edge (subtree) links.
 **/

int compressR(link h)
{
	int l, r, t;

	if(h == NULL)
	{
		return 0;
	}

	l = compressR(h->l);
	r = compressR(h->r);
	t = STindex(l*Vmax + r);
	adj[t].l = l;
	adj[t].r = r;

	return t;
}
