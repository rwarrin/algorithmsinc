/**
 * Reverse topological sort (adjacency lists)
 *
 * This version of DFS returns an array that contains vertex indices of a DAG
 * such that the source vertex of every edge appears to the right of the
 * destination vertex (a reverse topological sort). Comparing the last line of
 * TSdfsR with, for example, the last line of dfsR in Program 19.2 is an
 * instructive way to understand the compuation of the inverse of the
 * postorder-numbering permutation (see Figure 19.23).
 **/

static int cnt0;
static int pre[maxV];

void DAGts(DAG D, int ts[])
{
	int v;
	cnt0 = 0;
	for(v = 0; v < D->V; v++)
	{
		ts[v] = -1;
		pre[v] = -1;
	}
	for(v = 0; v < D->V; v++)
	{
		if(pre[v] == -1)
		{
			TSdfsR(D, V, ts);
		}
	}
}

void TSdfsR(Dag D, int v, int ts[])
{
	link t;
	pre[v] = 0;

	for(t = D->adj[v]; t != NULL; t = t->next)
	{
		if(pre[t->v] == -1)
		{
			TSdfsR(D, t->v, ts);
		}
	}

	ts[cnt0++] = v;
}
