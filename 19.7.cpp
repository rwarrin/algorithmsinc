/**
 * Topological sort (adjacency matrix)
 *
 * This adjacency-array DFS computes a topological sort (not the reverse)
 * because we replace the reference to a[v][w] in the DFS by a[w][v], thus
 * processing the reverse graph (see text).
 **/

void TSdfsR(DAG D, int v, int ts[])
{
	int w;
	pre[v] = 0;

	for(w = 0; w < D->V; w++)
	{
		if(D->adj[w][v] != 0)
		{
			if(pre[w] == -1)
			{
				TSdfsR(D, w, ts);
			}
		}
	}

	ts[cnt0++] = v;
}
