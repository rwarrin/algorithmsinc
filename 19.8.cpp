/**
 * Source-queue-based topological sort
 *
 * This implementation maintains a queue of sources and uses a table that keeps
 * track of the indegree of each vertex in the DAG induced by the vertices that
 * have not been removed from the queue. When we remove a source from the queue,
 * we decrement the indegree entries corresponding to each fo the vertices on
 * its adjacency list (and put on the queue any vertices corresponding to
 * entries that become 0). Vertices come off the queue in topologically sorted
 * order.
 **/

#include "QUEUE.h"

static int in[maxV];

void DAGts(DAG D, int ts[])
{
	int i, v;
	link t;
	for(v = 0; v < D->V; v++)
	{
		in[v] = 0;
		ts[v] = -1;
	}

	for(v = 0; v < D->V; v++)
	{
		for(t = D->adj[v]; t != NULL; t = t->next)
		{
			in[t->v]++;
		}
	}

	QUEUEinit(D->V);

	for(v = 0; v < D->V; v++)
	{
		if(in[v] == 0)
		{
			QUEUEput(v);
		}
	}

	for(i = 0; !QUEUEempty(); i++)
	{
		ts[i] = (v = QUEUEget());
		for(t = D->adj[v]; t != NULL; t = t->next)
		{
			if(--in[t->v] == 0)
			{
				QUEUEput(t->v);
			}
		}
	}
}
