/**
 * Weighted-graph ADT (adjacency matrix)
 *
 * For dense weighted undirected graphs, we use a matrix of weights, with the
 * entries in row v and column w and in row w and column v containing the weight
 * of the edge v-w. The sentinel value maxWT indicates the absence of an edge.
 * This code assumes that edge weights are of type double, and uses an auxiliary
 * routine MATRIXdouble to allocate a V-by-V array of weights with all entires
 * initialized to maxWT (see Program 17.4). To adpat this code for use as a
 * weighted digraph ADT implementation see Chapter 21), remove the last line of
 * GRAPHinsertE.
 **/

#include <stdlib.h>
#include "GRAPH.h"

struct graph
{
	int V;
	int E;
	double **adj;
};

Graph GRAPHinit(int V)
{
	int v;
	Graph G = malloc(sizeof(*G));
	G->adj = MATRIXdouble(V, V, maxWT);
	G->V = V;
	G->E = 0;

	return G;
}

void GRAPHinsertE(Graph G, Edge e)
{
	if(G->adj[e.v][e.w] == maxWT)
	{
		G->E++;
	}

	G->adj[e.v][e.w] = e.wt;
	G->adj[e.w][e.v] = e.wt;
}
