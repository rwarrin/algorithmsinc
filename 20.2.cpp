/**
 * Weighted-graph ADT (adjacency lists)
 *
 * This adjacency-lists representation is appropriate for sparse undirected
 * weighted graphs. As with undirected unweighted graphs, we represent each edge
 * with two list nodes, one on the adjacency list for each of the edge's
 * vertices. To represent the weights, we add a weight field to the list nodes.
 * This implementation does not check for duplicate edges. Beyond the factors
 * that we considered for unweighted graphs, a design decision has to be made
 * about whether or not to allow multiple edges of differing weights connecting
 * the same pair of nodes, which might be appropriate in certain applications.
 * To adapt this code for use as a weighted digraph ADT implementation, (see
 * Chapter 21), remove the line that adds the edge from w to v (the next-to-last
 * line fo GRAPHinsertE).
 **/

#include "GRAPH.h"

typedef struct node *link;

struct node
{
	int v;
	double wt;
	link next;
};

struct graph
{
	int V;
	int E;
	link *adj;
};

link NEW(int v, double wt, link next)
{
	link x = malloc(sizeof(*x));
	x->v = v;
	x->wt = wt;
	x->next = next;

	return x;
}

Graph GRAPHinit(int V)
{
	int i;
	Graph G = malloc(sizeof(*G));
	G->adj = malloc(V * sizeof(link));
	G->V = V;
	G->E = 0;
	for(i = 0; i < V; i++)
	{
		G->adj[i] = NULL;
	}

	return G:
}

void GRAPHinsertE(Graph G, Edge e)
{
	link t;
	int v = e.v, w = e.w;
	if(v == w)
	{
		return;
	}

	G->adj[v] = NEW(w, e.wt, G->adj[v]);
	G->adj[w] = NEW(v, e.wt, G->adj[w]);
	G->E++;
}
