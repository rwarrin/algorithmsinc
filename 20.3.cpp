/**
 * Prim's MST alogrithm
 *
 * This implementation of Prim's algorithm is the method of choice for dense
 * graphs. The outer loop grows the MST by choosing a minimal edge crossing the
 * cut between the vertices on the MST and vertices not on the MST. The w loop
 * find the minimal edge while at the same time (if w is not on the MST)
 * maintaining the invariant that the edge from w to fr[w] is the shortest edge
 * (of weight wt[w]) from w to the MST.
 **/

static int fr[maxV];

#define P G->adj[v][w]

void GRAPHmstV(Graph G, int st[], double wt[])
{
	int v, w, min;
	for(v = 0; v < G->V; v++)
	{
		st[v] = -1;
		fr[v] = v;
		wt[v] = maxWT;
	}

	st[0] = 0;
	wt[G->V] = maxWT;
	for(min = 0; min != G->V; )
	{
		v = min;
		st[min] = fr[min];
		for(w = 0, min = G->V; w < G->V; w++)
		{
			if(st[w] == -1)
			{
				if(P < wt[w])
				{
					wt[w] = P;
					fr[w] = v;
				}

				if(wt[w] < wt[min])
				{
					min = w;
				}
			}
		}
	}
}
