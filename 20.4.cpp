/**
 * Priority-first search (adjacency lists)
 *
 * This program is a generalized graph search that uses a priority-queue to
 * manage the fringe (see Section 18.8). The priority P is defined such that the
 * ADT function GRAPHpfs implements Prim's MST algorithm for sparse (connected)
 * graphs. Other priority definitions implement different graph-processing
 * algorithms.
 * The program moves the highest priority (lowest weight) edge from the fringe
 * to the tree, then checks every edge adjacent to the new tree vertex to see
 * whether it implies changes in the fringe. Edges to vertices not on the fringe
 * or the tree are added to the fringe; shorter edges to fringe vertices replace
 * corresponding fringe edges.
 * We use the priority-queue ADT interface from Section 9.6, modified to
 * substitute PQdelmin for PQdelmax and PQdec for PQchange (to emphasize that we
 * change priorities only by decreasing them). The static variable priority and
 * the function less allow the priority-queue functions to use vertex names as
 * handles and to compare priorities that are maintained by this code in the wt
 * array.
 **/

#define GRAPHpfs GRAPHmst

static int fr[maxV];
static double *priority;

int less(int i, int j)
{
	return priority[i] < priority[j];
}

#define P t->wt

void GRAPHpfs(Graph G, int st[], double wt[])
{
	link t;
	int v, w;

	PQinit();
	priority = wt;
	for(v = 0; v < G->V; v++)
	{
		st[v] = -1;
		fr[v] = -1;
	}

	fr[0] = 0;
	PQinsert(0);
	while(!PQempty())
	{
		v = PQdelmin();
		st[v] = fr[v];
		for(t = G->adj[v]; t != NULL; t = t->next)
		{
			if(fr[w = t->v] == -1)
			{
				wt[w] = P;
				PQinsert(w);
				fr[w] = v;
			}
			else if((st[w] == -1) && (P < wt[w]))
			{
				wt[w] = P;
				PQdec(w);
				fr[w] = v;
			}
		}

	}
}
