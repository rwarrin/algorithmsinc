/**
 * Kruskal's MST algorithm
 *
 * This implementation uses our sorting ADT from Chapter 6 and our union-find
 * ADT from Chapter 4 to find the MST by considering the edges in order of their
 * weights, discarding edges that create cycles until finding V-1 edges taht
 * comprise a spanning tree.
 * The implementation is packaged as a function GRAPHmstE that returns the MST
 * in a user-supplied array of edges. If desired, we can add a wrapper function
 * GRAPHmst to build a parent-link (or some other representation) of the MST, as
 * discussed in Section 20.1.
 **/

void GRAPHmstE(Graph G, Edge mst[])
{
	int i, k;
	Edge a[maxE];
	int E = GRAPHedges(a, G);

	sort(a, 0, E-1);

	UFinit(G->V);
	for(i = 0, k = 0; i < E && k < G->V-1; i++)
	{
		if(!UFfind(a[i].v, a[i].w))
		{
			UFunion(a[i].v, a[i].w);
			mst[k++] = a[i];
		}
	}
}
