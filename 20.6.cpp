/**
 * Boruvka's MST algorithm
 *
 * This implementation of Boruvka's MST algorithm uses the union-find ADT from
 * Chapter 4 to associate indices with MST subtrees as they are built. It
 * assumes that find (which returns the same index for all the vertices
 * belonging to each subtree) is in the interface.
 * Each phase corresponds to checking all the remaining edges; those that
 * connect vertices in different components are kept for the next phase. The
 * array a is used to store edges not yet discarded and not yet in the MST. The
 * index N is used to store those being saved for the next phase (the code
 * resets E from N at the end of th each phase) and the index h is used to
 * access the next edge to be checked. Each component's nearest neighbor is kept
 * in the array nn with find component numbers as indices. At the end of each
 * phase, we unite each component with its nearest neighbor and add the
 * nearest-neighbor edges to the MST.
 **/

Edge nn[maxV];
Edge a[maxE];

void GRAPHmstE(Graph G, Edge mst[])
{
	int h, i, j, k, v, w, N;
	Edge e;

	int E = GRAPHedges(a, G);
	for(UFinit(G->V); E != 0; E = N)
	{
		for(k = 0; k < G->V; k++)
		{
			nn[k] = EDGE(G->V, G->V, maxWT);
		}

		for(h = 0, N = 0; h < E; h++)
		{
			i = find(a[h].v);
			j = find(a[h].w);

			if(i == j)
			{
				continue;
			}

			if(a[h].wt < nn[i].wt)
			{
				nn[i] = a[h];
			}
			if(a[h].wt < nn[j].wt)
			{
				nn[j] = a[h];
			}

			a[N++] = a[h];
		}

		for(k = 0; k < G->V; k++)
		{
			e = nn[k];
			v = e.v;
			w = e.w;
			if((v != G->V) && !UFfind(v, w))
			{
				UFunion(v, w);
				mst[k] = e;
			}
		}
	}
}
