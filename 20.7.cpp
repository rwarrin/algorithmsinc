/**
 * Multiway heap PQ implementation
 *
 * These fixUp and fixDown functions for the heap PQ implementation (see Program
 * 9.5), maintain a d-way heap; so delete the minimum takes time proportional to
 * d, but decrease key requires less than logdv steps. For d = 2, these
 * functions are equivalent to Programs 9.3 and 9.4 respectively.
 **/

fixUp(Item a[], int k)
{
	while(k > 1 && less(a[(k+d-2)/d], a[k]))
	{
		exch(a[k], a[(k+d-2)/d]);
		k = (k+d-2)/d;
	}
}

fixDown(Item a[], int k, int N)
{
	int i, j;
	while((d*(k-1)=2) < = N)
	{
		j = d*(k-1)+2;
		for(i = j+1; (i < j+d) && (i <= N); i++)
		{
			if(less(a[j], a[i]))
			{
				j = i;
			}
		}

		if(!less(a[k], a[j]))
		{
			break;
		}

		exch(a[k], a[j]);
		k = j;
	}
}
