/**
 * Dijkstra's algorithm (adjacency lists)
 *
 * This implementation of Dijkstra's algorithm uses a priority queue of vertices
 * (in order of their distance from the source) to compute an SPT. We initialize
 * the queue with priority 0 for the source and priority maxWT for the other
 * vertices, the enter a loop where we move a lowest-priority vertex from the
 * queue to the SPT and relax along its incident edges.
 * The indirect priority-queue interface code is the same as in Program 20.4 and
 * is omitted. It defines a static variable priority and a function less, which
 * allow the priority-queue function to manipulate vertex names (indices) and to
 * use less to compare the priorities that are maintianed by this code in the wt
 * array.
 * This code is a generalized graph search, a PFS implementation. The definition
 * of P implements Dijkstra's algorithm; other definitions implement other PFS
 * algorithms (see text).
 **/

#define GRAPHpfs GRAPHspt

#define P (wt[v] + t->wt)

void GRAPHpfs(Graph G, int s, int st[], double wt[])
{
	int v, w;
	lilnk t;
	PQinit();
	priority = wt;
	for(v = 0; v < G->V; v++)
	{
		st[v] = -1;
		wt[v] = maxWT;
		PQinsert(v);
	}

	wt[s] = 0.0;
	PQdec(s);
	while(!PQempty())
	{
		if(wt[v = PQdelmin()] != maxWT)
		{
			for(t = G->adj[v]; t != NULL; t = t->next)
			{
				if(P < wt[w = t->v])
				{
					wt[w] = P;
					PQdec(w);
					st[w] = v;
				}
			}
		}
	}
}
