/**
 * All-pairs shortest-paths ADT
 *
 * Our solutions to the all-pairs shortest-paths problem provide clients with a
 * preprocessing function GRAPHspALL and two query functions: one that returns
 * the length of the shortet path from teh first argument to the second
 * (GRAPHspDIST); and another that returns the next vertex on the shortest path
 * from the first argument to the second (GRAPHspPATH). By convention, if there
 * is no such path GRAPHspPATH return G->V and GRAPHspDIST returns a sentinel
 * value greater than the length of the longest path.
 **/

void GRAPHspALL(Graph G);
double GRAPHspDIST(Graph G, int s, int t);
int GRAPHspPATH(Graph G, int s, int t);
