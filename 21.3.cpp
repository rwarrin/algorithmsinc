/**
 * Computing the diameter of a network
 *
 * This client program illustrates the use of the interface in Program 21.2. It
 * finds the longest of the shortest paths in the given network, prints its
 * weight (the diameter of the network), and prints the path.
 **/

void GRAPHdiameter(Graph G)
{
	int v, w, vMAX = 0, wMAX = 0;
	double MAX = 0.0;
	GRAPHspALL(G);

	for(v = 0; v < G->V; v++)
	{
		for(w = 0; w < G->V; w++)
		{
			if(GRAPHspPATH(G, v, w) != G->V)
			{
				if(MAX < GRAPHspDIST(G, v, w))
				{
					vMAX = v;
					wMAX = w;
					MAX = GRAPHspDIST(G, v, w);
				}
			}
		}
	}

	printf("Diameter is %f\n", MAX);
	for(v = vMAX; v != wMAX; v = w)
	{
		printf("%d-", v);
		w = GRAPHspPATH(G, v, wMAX);
	}
	printf("\n");
}
