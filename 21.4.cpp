/**
 * Dijkstra's algorithm for all shortest paths
 *
 * This implementation of the interface in Program 21.2 uses Dijkstra's
 * algorithms (see, for example, Program 21.1) on the reverse network to find
 * all shortest paths to each vertex (see text).
 * The network data type is assumed to have a pointer dist for the distances
 * array and a pointer path for the paths array. The wt and st array scomputed
 * by Dijkstra's algorithm are columns in the distances and paths matrices,
 * respectively (see Figure 21.9).
 **/

static int st[maxV];
static double wt[maxV];

void GRAPHspALL(Graph G)
{
	int v, w;
	Graph R = GRAPHreverse(G);
	G->dist = MATRIXdouble(G->V, G->V, maxWT);
	G->path = MATRIXint(G->V, G->V, G->V);

	for(v = 0; v < G->V; v++)
	{
		GRAPHpfs(R, v, st, wt);
		for(w = 0; w < G->V; w++)
		{
			G->dist[w][v] = wt[w];
		}
		for(w = 0; w < G->V; w++)
		{
			if(st[w] != -1)
			{
				G->path[w][v] = st[w];
			}
		}
	}
}

double GRAPHspDIST(Graph G, int s, int t)
{
	return G->dist[s][t];
}

int GRAPHspPATH(Graph G, int s, int t)
{
	return G->path[s][t];
}
