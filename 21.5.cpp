/**
 * Floyd's algorithm for all shortest paths
 *
 * This code for GRAPHspALL (along with the one-line implementations of
 * GRAPHspDIST and GRAPHspPATH from Program 21.4) implements the interface in
 * Program 21.2 using Floyd's algorithm, a generalization of Warshall's
 * algorithm (see Program 19.2) that finds the shortest paths instead of just
 * testing for the existence of paths.
 * After initializing the distances and paths matrices with the graph's edge, we
 * do a series of relaxation operations to compute the shortest paths. The
 * algorithm is simple to implement, but verifying that it computes the shortest
 * paths is more complicated (see text).
 **/

void GRAPHspALL(Graph G)
{
	int i, s, t;
	double **d = MATRIXdouble(G->V, G->V, maxWT);
	int **p = MATRIXint(G->V, G->V, G->V);
	for(s = 0; s < G->V; s++)
	{
		for(t = 0; t < G->V; t++)
		{
			if((d[s][t] = G->adj[s][t]) < maxWT)
			{
				p[s][t] = t;
			}
		}
	}

	for(i = 0; i < G->V; i++)
	{
		for(s = 0; s < G->V; s++)
		{
			if(d[s][i] < maxWT)
			{
				for(t = 0; t < G->V; t++)
				{
					if(d[s][t] > d[s][i]+d[i][t])
					{
						p[s][t] = p[s][i];
						d[s][t] = d[s][i]+d[i][t];
					}
				}
			}
		}
	}

	G->dist = d;
	G->path = p;
}
