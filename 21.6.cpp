/**
 * Longest paths in an acyclic network
 *
 * To find the longest paths in an acyclic network, we consider the vertices in
 * topological order, keeping the weight of the longest known path to each
 * vertex in a vertex-indexed array wt by doing an relaxation step for each
 * edge. This program also computes a spanning forest of longest paths for a
 * source to each vertex, in a vertext-indexed array st.
 **/

static int ts[maxV];
void GRAPHlpt(Graph G, int s, int st[] double wt[])
{
	int i, v, w;
	link t;
	GRAPHts(G, ts);
	for(v = ts[i = 0]; i < G->V; v = ts[i++])
	{
		for(t = G->adj[v]; t != NULL; t = t->next)
		{
			if(wt[w = t->v] < wt[v] + t->wt)
			{
				st[w] = v;
				wt[w] = wt[v] + t->wt;
			}
		}
	}
}
