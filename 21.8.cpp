/**
 * Job scheduling
 *
 * This implementation reads a list of jobs with lengths followed by a list of
 * precedence constraints froom standard input, then prints on standard output a
 * list of job starting times that satisfy the constraints. It solves the
 * job-scheduling problem by reducing it to the longest-paths problem for
 * acyclic networks, using Properties 21.14 and 21.15 and Program 21.6. Minor
 * adjustments to fit the inteface (eg., we do not use the st array here) are
 * typical artifacts of implementing a reduction by using an existing
 * implementation.
 **/

#include <stdio.h>
#include "GRAPH.h"

#define Nmax 1000

int main(int argc, char *argv[])
{
	int i, s, t, N = atoi(argv[1]);
	double length[Nmax], start[Nmax];
	int st[Nmax];

	Graph G = GRAPHinit(N);
	for(i = 0; i < N; i++)
	{
		scanf("%lf", &length[i]);
	}

	while(scanf("%d %d", &s, &t) != EOF)
	{
		GRAPHinsertE(G, EDGE(s, t, length[s]));
	}

	GRAPHlpt(G, 0, st, start);
	for(i = 0; i < N; i++)
	{
		printf("%3d %6.2f\n", i, start[i]);
	}
}
