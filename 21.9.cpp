/**
 * Bellman-Ford algorithm
 *
 * This implementation of the Bellman-Ford algorithm maintains a FIFO queue of
 * all vertices for which relazing along an outgoign edge could be effective. We
 * take a vertex off the queue and relax along all of its edges. If any of the
 * leads to a shorter path to some ertex, we put that on the queue. The sentinel
 * value G->V sparates the current batch of vertices (which changed on the last
 * iteration) from the next batch (which change on this iteration), and allows
 * us to stop after G->V passes.
 **/

void GRAPHbf(Graph G, int s, int st[], double wt[])
{
	int v, w;
	link t;
	int N = 0;
	QUEUEinit(G->E);
	for(v = 0; v < G->V; v++)
	{
		st[v] = -1;
		wt[v] = maxWT;
	}
	wt[s] = 0.0;
	st[s] = 0;
	
	QUEUEput(s);
	QUEUEput(G->V);
	while(!QUEUEempty())
	{
		if((v = QUEUEget()) == G->V)
		{
			if(N++ > G->V)
			{
				return;
			}

			QUEUEput(G->V);
		}
		else
		{
			for(t = G->adj[v]; t != NULL; t = t->next)
			{
				if(wt[w = t->v] > wt[v] + t->wt)
				{
					wt[w] = wt[v] + t->wt;
					QUEUEput(w);
					st[w] = v;
				}
			}
		}
	}
}
