/**
 * Flow-network ADT implementation
 *
 * This ADT implementation for flow networks extends the adjacency-lists
 * implementation for undirected graphs from Chapter 17 by adding to each list
 * node a field cap for edge capacities, flow for edge flow, and dup to link the
 * two representations of each edge. Appropriate extensions to other
 * graph-processing code are assumed (for example, the Edge type and EDGE
 * constructor must have cap and flow fields). The flow fields are initially all
 * 0; our network-flow implementations are cast as ADT functions that compute
 * appropriate values for them.
 **/

#include <stdlib.h>
#include "GRAPH.h"

tpedef struct node* link;
struct node
{
	int v;
	int cap;
	int flow;
	link dup;
	link next;
};

struct graph
{
	int V;
	int E;
	link *adj;
};

link NEW(int v, int cap, int flow, link next)
{
	link x = malloc(sizeof(*x));
	x->v = v;
	x->cap = cap;
	x->flow = flow;
	x->next = next;
	return x;
}

Graph GRAPHinit(int v)
{
	int i;
	Graph G = malloc(sizeof(*G));
	G->adj = malloc(V*sizeof(link));
	G->V = V;
	G->E = 0;
	for(i = 0; i < V; i++)
	{
		G->adj[i] = NULL;
	}

	return G;
}

void GRAPHinsertE(Graph G, Edge e)
{
	int v = e.v, w = e.w;

	G->adj[v] = NEW(w, e.cap, e.flow, G->adjv[v]);
	G->adj[w] = NEW(v, -e.cap, -e.flow, G->adj[w]);
	G->adj[v]->dup = G->adj[w];
	G->adj[w]->dup = G->adj[v];
	G->E++;
}
