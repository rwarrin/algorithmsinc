/**
 * Flow check and vallue computation
 *
 * This ADT function returns 0 if ingoing flow is not equal to outgoing flow at
 * some internal node or if some flow value is negative, the flow value
 * otherwise. Despite our confidence in mathematicians in Property 22.1, our
 * paranoia as programmers dictates that we also check that the flow out of the
 * source is equal to the flow into the sink.
 **/

static int flowV(Graph G, int v)
{
	link t;
	int x = 0;
	for(t = G->adj[v]; t != NULL; t = t->next)
	{
		x += t->flow;
	}

	return x;
}

int GRAPHflow(Graph G, int s, int t)
{
	int v, val = flowV(G, s);

	for(v = 0; v < G->V; v++)
	{
		if((v != s) && (v != t))
		{
			if(flowV(G, v) != 0)
			{
				return 0;
			}
		}
	}

	if(val + flowV(G, t) != 0)
	{
		return 0;
	}

	if(val <= 0)
	{
		return 0;
	}

	return val;
}
