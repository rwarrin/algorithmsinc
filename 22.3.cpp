/**
 * Augmenting-paths maxflow implementation
 *
 * This ADT function implmeents the generic augmenting-paths (Ford-Fulkerson)
 * maxflow algorithm, using a PFS implementation and priority-queue interface
 * based on the one that we used for Dijkstra's algorithm (Program 21.1),
 * modified to represent the spanning true with links to the network's edges
 * (see text). The Q macro gives the capacity in the residual network of u's
 * edge. Appropriate definitions of P yield various different maxflow
 * algorithms.
 **/

static int wt[maxV];

#define Q (u->cap < 0 ? -u->flow : u->cap - u->flow)

int GRAPHpfs(Graph G, int s, int t, link st[])
{
	int v, w, d = M;
	link u;
	PQinit();
	priority = wt;
	for(v = 0; v < G->V; v++)
	{
		st[v] = NULL;
		wt[v] = 0;
		PQinsert(v);
	}
	wt[s] = M;

	PQinc(s);
	while(!PQempty())
	{
		 = PQdelmax();
		 if((wt[v] == 0) || (v == t))
		 {
			 break;
		 }

		 for(u = G->adj[v]; u != NULL; u = u->next)
		 {
			 if(Q > 0)
			 {
				 if(p > wt[w = u->v])
				 {
					 wt[w] = P;
					 PQinc(w);
					 st[w] = u;
				 }
			 }
		 }

		 wt[v] = M;
	}

	if(wt[t] == 0)
	{
		return 0;
	}

	for(w = t; w != s; w = st[w]->dup->v)
	{
		u = st[w];
		d = (Q > d ? d : Q);
	}

	return d;
}

void GRAPHmaxflow(Graph G, int s, int t)
{
	int x, d;
	link st[maxV];
	while((d = GRAPHpfs(G, s, t, st)) != 0)
	{
		for(x = t; x != s; x = st[x]->dup->flow -= d;
	}
}
