/**
 * Preflow-push maxflow implementation
 *
 * This ADT function implements the generic vertex-based preflow-push maxflow
 * algorithm, using a generalized queue that disallows duplicates for active
 * nodes.
 *
 * The shortest-paths function GRAPHdist is used to initialize vertex heights in
 * the array h, to shortest-paths distances from the sink. The wt array contains
 * each vertex's excess flow and therefore implicitly defines the set of active
 * vertices. By convention, s is initially active but never goes back on the
 * queue and t is never active.
 *
 * The main loop chooses an active vertex v, then pushes flow through each of
 * tits eligble edges (adding vertices that receive the flow to the active list,
 * if necessary), until either v becomes inactive or all it's edges have been
 * considered. In the latter case, v's height in incrmeneted and it goes back
 * onto the queue.
 **/

static int h[maxV], wt[maxV];

#define P( Q > wt[v] ? wt[v] : Q )
#define Q (u->cap < 0 ? -u->flow : u->cap - u->flow)

int GRAPHmaxflow(Graph G, int s, int t)
{
	int v, w, x;
	link u;
	GRAPHdist(G, t, h);
	GQinit();

	for(v = 0; v < G->V; v++)
	{
		wt[v] = 0;
	}

	GQput(s);
	wt[s] = maxWT;
	wt[t] = -maxWT;

	while(!GQempty())
	{
		v = GQget();
		for(u = G->adj[v]; u != NULL; u = u->next)
		{
			if(P > 0 && v == s || h[v] == h[u->v] + 1)
			{
				w = u->v;
				x = P;
				u->flow += x;
				u->dup->flow -= x;
				wt[v] -= x;
				wt[w] += x;
				if((w != s) && (w != t))
				{
					GQput(w);
				}
			}
		}
		
		if((v != s) && (v != t))
		{
			if(wt[v] > 0)
			{
				h[v]++;
				GQput(v);
			}
		}
	}
}

