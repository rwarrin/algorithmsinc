/**
 * List reversal
 *
 * This function reverses the links in a list, returning a pointer to the final
 * node, which then points the the next-to-final node, and so forth, with the
 * link in the first node of the original list set to NULL. To accomplish this
 * task, we need to maintain links to three consecutive nodes in the list.
 **/

typedef struct node* link;
#define NULL 0

struct node
{
	int value;
	link next;
};

link reverse(link x)
{
	link t, y = x, r = NULL;
	while(y != NULL)
	{
		t = y->next;
		y->next = r;
		r = y;
		y = t;
	}

	return r;
}
