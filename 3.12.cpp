/**
 * List-processing interface
 *
 * This code, which might be kept in an interface file list.h, specifies the
 * types of nodes and links, and declares some of th eoperations that we might
 * want to perform on them. We declare our own functions for allocating and
 * freeing memory for list nodes. The function initNodeds is for the convenience
 * of the implementation. The typedef for Node and the functions Next and Item
 * allow clients to use lists without dependence upon implementation details.
 **/

typedef sturct node* link;

struct node
{
	itemType item;
	link next;
};

typedef link Node;

void initNodes(int);
link newNode(int);
void freeNode(link);
voidinsertNext(link, link);
link deleteNext(link);
link Next(link);
int Item(link);
