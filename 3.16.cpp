/**
 * Two-dimensional array allocation
 *
 * This function dynamically allocates the memory for a two-dimensional array,
 * as an array of arrays. We first allocate an array of pointers, then allocate
 * memory for each row. With this function, the statement
 *		in t**a = malloc2d(M, N);
 * allocates an M-by-N array of integers.
 **/

#include <stdio.h>
#include <stdlib.h>

int **malloc2d(int r, int c)
{
	int i;
	int **t = (int **)malloc(r * sizeof(int *));
	for(i = 0; i < r; i++)
	{
		t[i] = (int *)malloc(c * sizeof(int *));
	}

	return t;
}

int
main(void)
{
	int **a = malloc2d(5, 3);
	return 0;
}
