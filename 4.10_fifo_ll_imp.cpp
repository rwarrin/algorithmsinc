/**
 * The difference between a FIFO queue and a pushdown stack is that new items
 * are inserted at the end, rather than the beginning.
 *
 * Accordingly, this program keeps a pointer tail to the last node of the list,
 * so that the function QUEUput can add a new node by linking that node to the
 * node referenced by tail and then updating tail to point ot th enew node. The
 * functions QUEUEget, QUEUEinit, and QUEUEempty are all identical to their
 * counterparts for the linked-list pushdown-stack implementation of Program
 * 4.5.
 **/

#include <stdlib.h>
#include "Item.h"
#include "4.9_fifo_interface.h"

typedef struct QUEUEnode* link;
struct QUEUEnode
{
	Item item;
	link next;
};

static link head, tail;

link NEW(Item item, link nex)
{
	link x = malloc(sizeof *x);
	x->item = item;
	x->next = next;
	return x;
}

void QUEUEinit(int maxN)
{
	head = NULL;
}

int QUEUEempty()
{
	return head == NULL;
}

QUEUEput(Item item)
{
	if(head == NULL)
	{
		head = (tail = NEW(item, head));
		return;
	}

	tail->next = NEW(item, tail->next);
	tail = tail->next;
}

Item QUEUEget()
{
	Item item = head->item;
	link t = head->next;
	free(head);
	head = t;
	return item;
}
