/**
 * Polynomial client (binomial coefficients)
 *
 * This client program uses the polynomial ADT that is defined in the interface
 * program 4.22 to perform algebraic manipulations with polynomials. It takes an
 * integer N and a floating-point number p from the command line, computes
 * (x+1)^n, and checks the result by evaluating the resulting polynomial at x =
 * p;
 **/

#include <stdio.h>
#include <stdlib.h>
#include "4.22_poly_interface.h"
#include "4.23_poly_implementation.cpp"

int main(int argc, char *argv[])
{
	int N = atoi(argv[1]);
	float p = atof(argv[2]);
	Poly t, x;
	int i, j;
	printf("Binomial coeefficients\n");

	t = POLYadd(POLYterm(1, 1), POLYterm(1, 0));
	for(i = 0, x = t; i < N; i++)
	{
		x = POLYmult(t, x);
		showPOLY(x);
	}

	printf("%f\n", POLYeval(x, p));

	return 0;
}
