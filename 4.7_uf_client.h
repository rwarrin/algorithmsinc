/**
 * Equivalence-relations ADT client
 *
 * The ADT of program 4.6 separates the connectivity algorithm for the
 * union-find implementation, making that algorith more accessible.
 **/

#include <stdio.h>
#include "4.6_uf_interface.h"

int main(int argc, char *argv[])
{
	int p, q, N = atoi(argv[1]);

	UFinit(N);
	while(scanf("%d %d", &p, &q) == 2)
	{
		if(!UFfind(p, q))
		{
			UFunion(p, q);
			printf(" %d %d\n", p, q);
		}
	}

	return 0;
}
