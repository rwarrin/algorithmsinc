/**
 * Fibonacci numbers (recursive programming)
 *
 * This program, although compact and elegant, is not usable because it takes
 * exponential time to compute Fn. The running time to compute Fn+1 is ~ 1.6
 * times as long as the running time to compute Fn. For example, since O9 > 60,
 * if we notice that our computer takes about a second to compute Fn, we know
 * that it will take more than a minute to compute Fn+9 and more than an hour to
 * compute Fn+18.
 **/

int F(int i)
{
	if(i < 1)
	{
		return 0;
	}

	if(i == 1)
	{
		return 1;
	}

	return F(i-1) + F(i-2);
}
