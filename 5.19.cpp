/**
 * Construction of a tournament
 *
 * This recursive function divides a file a[1],...,a[r] into the two parts
 * a[1],...,a[m] and a[m+1],...,a[r], builds tournaments for the two parts
 * (recursively), and makes a tournament for the whole file by setting links in
 * a new node to the recursively built tournaments and setting its item value to
 * the larger fo the items in the roots of the two recursively built
 * tournaments.
 **/

#include <stdio.h>
#include <stdlib.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

typedef char Item;
typedef struct node* link;
struct node
{
	Item item;
	link l;
	link r;
};

link NEW(Item item, link l, link r)
{
	link x = (link)malloc(sizeof(*x));
	x->item = item;
	x->l = l;
	x->r = r;
	return x;
}

link max(Item a[], int l, int r)
{
	int m = (l + r) / 2;
	Item u, v;
	link x = NEW(a[m], NULL, NULL);
	if(l == r) return x;
	x->l = max(a, l, m);
	x->r = max(a, m+1, r);
	u = x->l->item;
	v = x->r->item;
	if(u > v)
	{
		x->item = u;
	}
	else
	{
		x->item = v;
	}

	return x;
}

void printnode(char c, int h)
{
	int i;
	for(i = 0; i < h; i++)
	{
		printf("  ");
	}
	printf("%c\n", c);
}

void show(link x, int h)
{
	if(x == NULL)
	{
		printnode('*', h);
		return;
	}

	show(x->r, h+1);
	printnode(x->item, h);
	show(x->l, h+1);
}

int main(void)
{
	char a[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
	link Tournament = max(a, 0, ArrayCount(a));
	show(Tournament, 0);
}
