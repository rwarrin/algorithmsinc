/**
 * Euclid's algorithm
 *
 * One of the oldest-known algorithms, dating back over 2000 years, is this
 * recursive method for finding the greatest common divisors of two integers.
 **/

int gcd(int m, int n)
{
	if(n == 0)
	{
		return m;
	}

	return gdc(n, m % n);
}
