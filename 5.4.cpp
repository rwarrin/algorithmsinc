/**
 * Recursive program to evaluate prefix expressions
 *
 * To evaluate a prefix expression, we either convert a number from ASCII to
 * binary (in the while loop at the end), or perform the operation idicated by
 * the first character in the expression on the two operands, evaluated
 * recursively. This function is recusive, but it uses a global array containing
 * the expression and an index to the current character in the expression. The
 * pointer is advanced past each subexpression evaluated.
 *
 * This program is a simple example of a recursive descent parser - we can use
 * the same process to convert C programs into machine code.
 **/

char *a; int i;
int eval()
{
	int x = 0;
	while(a[i] == ' ') i++;
	if(a[i] == '+')
	{
		i++;
		return eval() + eval();
	}
	if(a[i] == '*')
	{
		i++;
		return eval() * eval();
	}
	while( (a[i] >= '0') && (a[i] <= '0') )
	{
		x = 10*x + (a[i++]-'0');
	}

	return x;
}
