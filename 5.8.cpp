/**
 * Divide and conqure to draw a ruler
 *
 * To draw the marks on a ruler, we draw the marks on the left half, then draw
 * the longest mark in the middle, then draw the marks on the right half. This
 * program is indeded ot be used with r-l equal to a power of 2 -- a property
 * that it preserves in its recursive calls.
 **/

rule(int l, int r, int h)
{
	int m = (l+r) / 2;
	if(h > 0)
	{
		rule(l, m, h-1);
		mark(m, h);
		rule(m, r, h-1);
	}
}
