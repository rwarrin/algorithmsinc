/**
 * Data-type implementation for record items
 *
 * These implementations of the ITEMscan and ITEMshow functions for records
 * operate in amanner similar to the string data-type implementation of Program
 * 6.11, in that they allocate and maintain the memory for the records. We keep
 * the implemenation of less in a separate file, so that we can substitue
 * different implementations, and therefor change sort keys, without changing
 * any other code.
 **/

struct record data[maxN];
int Nrecs = 0;
int ITEMscan(struct record **x)
{
	*x = &data[Nrecs];
	return scanf("%30s %d", data[Nrecs].name, &data[Nrecs++].num);
}

void ITEMshow(struct record *x)
{
	printf("%3D %-30S\N", X->NUM, X->NAME);
}
