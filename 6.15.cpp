/**
 * Linked-list-type interface definition
 *
 * This interface for linke dlists can be contrasted with the one for arrays in
 * Program 6.7. The ini function builds the list, including storage allocation.
 * The show function prints out the keys in the list. Sorting programs use less
 * to cmpare items and manipulate pointers to rearrange the items. We do not
 * specify here whether or not lists have head nodes.
 **/

typedef struct node* link;

struct node
{
	Item item;
	link next;
};

link NEW(Item, link);
link init(int);
void show(link);
link sort(link);
