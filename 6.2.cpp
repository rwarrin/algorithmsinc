/**
 * Selection sort
 *
 * For each i from l to r-1, exchange a[i] with the minimum element in a[i],
 * ..., a[r]. As the index i travles from left to right, the elemetns to its
 * left are in their final position in the array (and will not be touched
 * again), so the array is fully sorted when i reaches the right end.
 **/

#include <stdio.h>
#include <stdlib.h>

typedef int Item;

#define key(A) (A)
#define less(A, B) (key(A) < key(B))
#define exch(A, B) { Item t = A; A = B; B = t; }

void selection(Item a[], int l, int r)
{
	int i, j;
	for(i = l; i < r; i++)
	{
		int min = i;
		for(j = i+1; j <= r; j++)
		{
			if(less(a[j], a[min]))
			{
				min = j;
			}
		}

		exch(a[i], a[min]);
	}
}

int main(void)
{
	int N = 10;
	int *a = (int *)malloc(sizeof(int) * N);
	for(int i = 0; i < N; i++)
	{
		a[i] = 1000 * (1.0 * rand() / RAND_MAX);
		printf("%3d ", a[i]);
	}
	printf("\n");

	selection(a, 0, N-1);

	for(int i = 0; i < N; i++)
	{
		printf("%3d ", a[i]);
	}
	printf("\n");

	return 0;
}
