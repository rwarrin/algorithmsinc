
/**
 *
 **/

#include <stdio.h>
#include <stdlib.h>

typedef int Item;

#define key(A) (A)
#define less(A, B) (key(A) < key(B))
#define exch(A, B) { Item t = A; A = B; B = t; }
#define compexch(A, B) if(less(B, A)) exch(A, B)

void bubble(Item a[], int l, int r)
{
	int i;
	int j;
	for(i = l; i < r; i++)
	{
		for(j = r; j > i; j--)
		{
			compexch(a[j-1], a[j]);
		}
	}
}

int main(void)
{
	int N = 10;
	int *a = (int *)malloc(sizeof(int) * N);
	for(int i = 0; i < N; i++)
	{
		a[i] = 1000 * (1.0 * rand() / RAND_MAX);
		printf("%3d ", a[i]);
	}
	printf("\n");

	bubble(a, 0, N-1);

	for(int i = 0; i < N; i++)
	{
		printf("%3d ", a[i]);
	}
	printf("\n");

	return 0;
}
