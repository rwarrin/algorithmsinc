/**
 * Shellsort
 *
 * If we do not use sentinels and then replace every occurrence of "1" by "h" in
 * insertion sort, the resulting program h-sorts the file. Adding an outer loop
 * to change the increments leads to this compact shellsort implementation,
 * which uses the increment sequence 1 4 13 40 121 364 1093 3280 9841 ....
 **/

#include <stdio.h>
#include <stdlib.h>

typedef int Item;

#define key(A) (A)
#define less(A, B) (key(A) < key(B))
#define exch(A, B) { Item t = A; A = B; B = t; }
#define compexch(A, B) if(less(B, A)) exch(A, B)

void shellsort(Item a[], int l, int r)
{
	int i, j ,h;
	for(h = 1; h <= (r-l)/9; h = 3*h+1) ;

	for(; h > 0; h /= 3)
	{
		for(i = l+h; i <= r; i++)
		{
			int j = i;
			Item v = a[i];
			while(j >= l+h && less(v, a[j-h]))
			{
				a[j] = a[j-h];
				j -= h;
			}
			a[j] = v;
		}
		for(int i = 0; i <= r; i++)
		{
			printf("%3d ", a[i]);
		}
		printf("\n");
	}
}

int main(void)
{
	int N = 30;
	int *a = (int *)malloc(sizeof(int) * N);
	for(int i = 0; i < N; i++)
	{
		a[i] = 1000 * (1.0 * rand() / RAND_MAX);
		printf("%3d ", a[i]);
	}
	printf("\n");

	shellsort(a, 0, N-1);

	for(int i = 0; i < N; i++)
	{
		printf("%3d ", a[i]);
	}
	printf("\n");

	return 0;
}
