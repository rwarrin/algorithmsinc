/**
 * Quicksort
 *
 * If the array has one or fewer elements, do nothing. Otherwise, the array is
 * processed by a partition procedure (see Program 7.2), which puts a[i] into
 * position for some i between l and r inclusive, and rearranges the other
 * elements such tha tthe recursive calls properly finish the sort.
 **/

int partition(Item a[], int l, int r);

void quicksort(Item a[], int l, int r)
{
	int i;
	if(r <= l)
	{
		return;
	}

	i = partition(a, l, r);
	quicksort(a, l, i-1);
	quicksort(a, i+1, r);
}
