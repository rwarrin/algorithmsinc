/**
 * Partitioning
 *
 * The variable v holds the value of the partitioning element a[r], and i and j
 * are the left and right scan pointers, respectively. The partitioning loop
 * increments i and decrements j, while maintaining the invariant property that
 * no elements to th eleft of i are greater than v and no elements to the right
 * of j are smaller than v. Once the pointers meet,  we complete the
 * partitioning by exchaning a[i] and a[r], which puts v into a[i], with no
 * larger elements to v's right and no smaller elements to its left.
 * The partitioning loop is implmeented as an infinite loop, with a break when
 * the pointers ross. The test j == l protects against the case that the
 * partitioning element is the smallest element in the file.
 **/

int partition(Item a[], int l, int r)
{
	int i = l-1;
	int j = r;
	Item v = a[r];
	for(;;)
	{
		while(less(a[++i], v)) ;
		while(less(v, a[--j]))
		{
			if(j == l)
			{
				break;
			}
		}

		if(i >= j)
		{
			break;
		}

		exch(a[i], a[j]);
	}
	exch(a[i], a[r]);
	return i;
}
