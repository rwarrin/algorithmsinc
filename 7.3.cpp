/**
 * Nonrecursive quicksort
 *
 * This nonrecursive implementation (see Chapter 5) of quicksort uses an
 * explicit pushdown stack, replacing recursive calls with stack pushes (of the
 * parameters) and the procedure call/exit with a loop that pops parameters from
 * the stack and processes them as long as the stack is nonempty. We put the
 * larger of the two subfiles on the stack first to ensure that the maximum
 * stack depth for sorting N elements in lg N (see property 7.3).
 **/

#define push2(A, B) push(B); push(A);

void quicksort(Item a[], int l, int r)
{
	int i;
	stackinit();
	push2(l, r);
	while(!stack empty())
	{
		l = pop();
		r = pop();
		if(r <= l)
		{
			continue;
		}

		if(i-l > r-i)
		{
			push2(l, i-1);
			push2(i+1, r);
		}
		else
		{
			push2(i+1, r);
			push2(l, i-1);
		}
	}
}
