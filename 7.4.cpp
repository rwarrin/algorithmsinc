/**
 * Improved quicksort
 *
 * Choosing the median of the first, middle, and final elements as the
 * partitioning element and cutting off the recursion for small subfiles can
 * significantly improve the performance of quicksort. This implementation
 * partitions on the median of the first, middle, and final elements  in the
 * array (otehrwise leaving these elements out of the partitioning process).
 * Files of size 11 or smaller are ignored during partitioning; then, insertion
 * forom Chapter 6 is used to finish the sort.
 **/

#define M 10
void quicksort(Item a[], int l, int r)
{
	int i;
	if(r-l <= M)
	{
		return;
	}

	exch(a[(l+r)/2], a[r-1]);
	compexch(a[l], a[r-1]);
	compexch(a[l], a[r]);
	compexch(a[r-1], a[r]);

	i = partition(a, l+1, r-1);
	quicksort(a, l, i-1);
	quicksort(a, i+1, r);
}

void sort(Item a[], int l, int r)
{
	quicksort(a, l, r);
	insertion(a, l, r);
}
