/**
 * Quicksort with three-way partitioning
 *
 * This program is based on partitioning the array into three parts: elements
 * smaller than the partitioning element (in a[l],...,a[j]); elements equal to
 * the partitioning lements (in a[j+1],...,a[i-1]); and elements larger than the
 * partitioning element (in a[i],...,a[r]). Then the sort can be completed with
 * two recursive calls, one for th esmaller keys and one for the larger keys.
 * To accomplish the objective, the program keeps keys equal to the partitioning
 * element on the left between l and ll and on the right between rr and r. In
 * the partitioning loop, after the scan pointers stop and the items and i and j
 * are exchanged, it checks each of those itms to see whether it is equal  to
 * the partitioning element. If the one now on the left is equal to the
 * partitioning element, it is exchanged into the left part of the array; if one
 * now on the right is equal to the partitioning element, it is exchanged into
 * the right part of the array.
 * After the pointers cross, the two ends of the array with elements equal to
 * the partitioning element are exchanged back to the middle. Then those keys
 * are in position and can be excluded from the subfiles for the recursive
 * calls.
 **/

#define eq(A, B) (!less(A, B) && !less(B, A))

void quicksort(Item a[], int l, int r)
{
	int i, j, k, p, q;
	Item v;

	if(r <= l)
	{
		return;
	}

	v = a[r];
	i = l-1;
	j = r;
	p = l-1;
	q = r;
	for(;;)
	{
		while(less(a[++i], v)) ;
		while(less(v, a[--j]))
		{
			if(j == l)
			{
				break;
			}
		}

		if(i >= j)
		{
			break;
		}

		exch(a[i], a[j]);
		if(eq(a[i], v))
		{
			p++;
			exch(a[p], a[i]);
		}
		if(eq(v, a[j]))
		{
			q--;
			exch(a[q], a[j]);
		}
	}

	exch(a[i], a[r]);
	j = i-1;
	i = i+1;

	for(k = l; k < p; k++, j--)
	{
		exch(a[k], a[j]);
	}
	for(k = r-1; k > q; k--, i++)
	{
		exch(a[k], a[i]);
	}

	quicksort(a, l, j);
	quicksort(a, i, r);
}
