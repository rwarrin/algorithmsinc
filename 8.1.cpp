/**
 * Merging
 *
 * To combine two ordered arrays a and b into an ordered array c, we use a for
 * loop that puts an element into c at each iteration. If a is exhausted, the
 * element comes from b; if b is exhausted, the element comes from a, and if
 * items remain in both, the smallest of the remaining elements in a and b goes
 * to c. Beyond the implict assumption that the arrays are ordered, this
 * implementation assumes that the array c is disjoin from (that is, does not
 * overlap or share storage with) a and b.
 **/

mergeAB(Item c[], Item a[], int N, Item b[], int M)
{
	int i, j, k;
	for(i = 0, j = 0, k = 0;
		k < N+M;
		k++)
	{
		if(i == N)
		{
			c[k] = b[j++];
			continue;
		}
		if(j == M)
		{
			c[k] = a[i++];
			continue;
		}
		c[k] = (less(a[i], b[j])) ? a[i++] : b[j++];
	}
}
