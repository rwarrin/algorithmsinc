/**
 * Abstract in-place merge
 *
 * This program merges without using sentinels by compying the second array into
 * aux in reverse order back to back with the first (putting aux in bitonic
 * order). The first for loop moves the first array and leaves i pointing ot l,
 * ready to begin the merge. The second for loop moves the second array, and
 * leaves j pointing t r. Then, in the merge (the third for loop), the largest
 * element serves as the sentinel in whichever array it is. The inner loop of
 * this program is short (move to aux, compare, move back to a, incrment i or j,
 * increment and test k).
 **/

Item aux[maxN];

merge(Item a[], int l, int m, int r)
{
	int i, j, k;
	for(i = m+1; i > l; i--)
	{
		aux[i-1] = a[i-1];
	}
	for(j = m; j < r; j++)
	{
		aux[r+m-j] = a[j+1];
	}

	for(k = l; k <= r; k++)
	{
		if(less(aux[j], aux[i]))
		{
			a[k] = aux[j--];
		}
		else
		{
			a[k] = aux[i++];
		}
	}
}
