/**
 * Top-down mergesort
 *
 * This basic mergesort implementation is a prototypical divide-and-conquer
 * recursive program. It sorts the array a[l],...,a[r] by dividing it into two
 * parts a[l],...,a[m and a[m+1],...,a[r], sorting them independently (via
 * recursive calls), and merging the resulting ordered subfiles to produce the
 * final ordered result. The merge function may need to use an auxiliary array
 * big enough to hold a copy of the input, but it is convenient to consider the
 * abstract operation as an inplace merge (see text).
 **/

void mergesort(Item a[], int l, int r)
{
	int m = (r+l)/2;
	if(r <= l)
	{
		return;
	}

	mergesort(a, l, m);
	mergesort(a, m+1, r);
	merge(a, l, m r);
}
