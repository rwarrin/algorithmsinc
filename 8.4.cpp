/**
 * Mergesort with no copying
 *
 * This recursive program is set up to sort b, leaving th eresult in a. Thus,
 * the recursive calls are written to leave their result in b, and we use
 * Program 8.1 to merge those files from b into a. In this way, all the data
 * movement is done during the course of the merges.
 **/

Item aux[maxN];

void mergesortABr(Item a[], Item b[], int l, int r)
{
	int m = (l+r)/2;
	if(r-l <= 10)
	{
		insertion(a, l, r);
		return;
	}

	mergesortABr(b, a, l, m);
	mergesortABr(b, a, m+1, r);
}

void mergesortAB(Item a[], int l, int r)
{
	int i;
	for(i = l; i <= r; i++)
	{
		aux[i] = a[i];
	}
	mergesortABr(a, aux, l, r);
}
