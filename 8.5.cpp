/**
 * Bottom-up mergesort
 *
 * Bottom-up merge sort consists of a sequence of passes over the whole file
 * doing m-by-m merges, doubling m on each pass. The final subfile is of size m
 * only if the file size is an even multiple of m, so the final merge is an
 * m-by-x merge, for some x less than or equal to m.
 **/

#define min(A, B) (A < B) ? A : B

void mergesortBU(Item a[], int l, int r)
{
	int i, m;
	for(m = 1; m <= r-l; m = m+m)
	{
		for(i = l; i <= r-m; i+= m+m)
		{
			merge(a, i, i+m-1, min(i+m+m-1, r));
		}
	}
}
