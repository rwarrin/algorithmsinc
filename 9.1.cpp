/**
 * Basic priority-queue ADT
 * 
 * This interface defines operations for the simplest type of priority queue:
 * initialize, test if empty, add a new item, remove the largest item.
 * Elementary implementations of these functions using arrays and linked lists
 * can require linear time in the worst case, but we shall see iplementations in
 * this chapter where all operations are guaranteed to run in time at most
 * proportional to the logarithm of th enumber of tiems in the queue. The
 * argument to PQinit specifies the maximum number of tiems expected in the
 * queue.
 **/

void PQinit(int);
int PQempty();
void PQinsert(Item);
Item PQdelmax();
