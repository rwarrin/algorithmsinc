/**
 * Joining of two equal-sized power-of-2 heaps
 *
 * We need to change only a few links to combine two equal-sized power-of-2
 * heaps into one power-of-2 heap that is twice that size. This procedure is one
 * key to the efficiency of the binomial queue algorithm.
 **/

PQlink pair(PQlink p, PQlink q)
{
	if(less(p->key, q->key))
	{
		p->r = q->l;
		q->l = p;
		return q;
	}
	else
	{
		q->r = p->l;
		p->l = q;
		return p;
	}
}
