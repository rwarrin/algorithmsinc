/**
 * Insertion into a binomial queue
 *
 * To insert a node into a binomial queue, we first make the node into a 1-heap,
 * identify it as a cary 1-heap, and then iterate the following process starting
 * at i = 0. If the binomial queue had no 2i-heap, we put the carry 2i-heap into
 * the queue. If the binomial queue has a ti-heap, we combine that with the new
 * one to make a 2i+1-heap, increment i, and iterate until finding an empty heap
 * position in the binomial queue. As usual, we adopt the convention of
 * representing null links with z, which can be defined to be NULL or can be
 * adapted to be a sentinel node.
 **/

PQlink PQinsert(PQ pq, Item v)
{
	int i;
	PQlink c, t = malloc(sizeof(*t));
	c = t;
	c->l = z;
	c->r = z;
	c->key = v;

	for(i = 0; i < maxBQsize; i++)
	{
		if(c == z)
		{
			break;
		}

		if(pq->bq[i] == z)
		{
			pq->bq[i] = c;
			break;
		}

		c = pair(c, pq->bq[i]);
		pq->bq[i] = z;
	}

	return t;
}
