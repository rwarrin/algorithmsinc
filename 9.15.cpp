/**
 * Deletion of the maximum in a binomial queue
 *
 * We first scan the root nodes to find the maximum, and remove the power-of-2
 * heap containing the maximum from the binomial queue. We then remove the root
 * node containing the maximum from its power-of-2 heap and temporarily build a
 * binomial queue that contains the remaining constituent parts of the
 * power-of-2 heap. Finally we use the join operation to merge this binomial
 * queue back into the original binomial queue.
 **/

Item PQdelmax(PQ pq)
{
	int i, max;
	PQlink x;
	Item v;

	PQlink temp[maxBQsize];

	for(i = 0, max = -1; i < maxBQsize; i++)
	{
		if(pq->bq[i] != z)
		{
			if((max == -1) || less(v, pq->bq[i]->key))
			{
				max = i;
				v = pq->bq[max]->key;
			}
		}
	}

	x = pq->bq[max]->l;

	for(i = max; i < maxBQsize; i++)
	{
		temp[i] = z;
	}

	for(i = max; i > 0; i--)
	{
		temp[i-1] = x;
		x = x->r;
		temp[i-1]->r = z;
	}

	free(pq->bq[max]);
	pq->bq[max] = z;
	BQjoin(pq->bq, temp);
	return v;
}
