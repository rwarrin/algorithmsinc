/**
 * Joining (merging) of two binomial queues
 *
 * This code mimics the operation of adding two binary numbers. Proceeding form
 * rigth to left with an initial carry bit of 0, we treat the eight possible
 * cases (all possible values of the operands and carry bits) in a
 * straightforward manner. For example, case 3 corresponds to the operand bits
 * being both 1 and the carry 0. Then, the result is 0, but the carry is 1 (the
 * result of adding the operand bits).
 **/

#define test(C, B, A) 4*(C) + 2*(B) + 1*(A)

void BQjoin(PQlink *a, PQlink *b)
{
	int i;
	PQlink c = z;

	for(i = 0; i < maxMQsize; i++)
	{
		switch(test(c != z, b[i] != z, a[i] != z))
		{
			case 2: { a[i] = b[i]; } break;
			case 3: { c = pair(a[i], b[i]); a[i] =  z; } break;
			case 4: { a[i] = c; c = z } break;
			case 5: { c = pair(c, a[i]); a[i] = z; } break;
			case 6:
			case 7: { c = pair(c, b[i]); } break;
		}
	}
}

void PQjoin(PQ a, PQ b)
{
	BQjoin(a->bq, b->bq);
}
