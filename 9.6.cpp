/**
 * Sorting with a priority queue
 *
 * To sort a subarray a[l],...,a[r] using a priority queue ADT we sipmly use
 * PQinsert to put all the elements on the priority queue, and then use PQdelmax
 * to remove them, in decreasing order. This sorting algorithm runs in time
 * proportional to N lg N, but uses extra spae proportional to the number of
 * tiems to be sorted (for the priority queue).
 **/

void PQsort(Item a[], int l, int r)
{
	int k;
	PQinit();
	for(k = l; k <= r; k++)
	{
		PQinsert(a[k]);
	}
	for(k = r; k >= l; k--)
	{
		a[k] = PQdelmax();
	}
}
