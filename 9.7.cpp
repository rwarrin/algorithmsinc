/**
 * Heapsort
 *
 * Using fixDown directly gives the classical heapsort algorith. The for loop
 * constructs the hea; then, the while loop exchanges the largest element with
 * the final element in the array and repairs the heap continuing until the heap
 * is empty. The pointer pq to a[l-1] allows the code to treat the subarray
 * passed to it as an array with the first element at index 1, for the array
 * representation of the complete tree (see Figure 9.2). Some programming
 * environments may disallow this usage.
 **/

void heapsort(Item a[], int l, int r)
{
	int k;
	int N = r-l + 1;
	Item *pq = a+l - 1;

	for(k = N/2; k >= 1; k--)
	{
		fixDown(pq, k, N);
	}

	while(n > 1)
	{
		exch(pq[1], pq[N]);
		fixDown(pq, 1, --N);
	}
}
