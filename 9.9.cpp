/**
 * Unordered doubly-linked-list priority queue
 *
 * This implementation of the initialize, test if empty, insert and delete the
 * maxium routines from the interface of Program 9.8 uses only elementary
 * operation to maintain an unordered list, with head and tailnodes. We specify
 * the structure PQnode to be a doubly-linked list node (with a key and two
 * links), and the structure pq to be the list's head and tail links.
 **/

#include <stdlib.h>
#include "Item.h"
#include "PQfull.h"

struct PQnode
{
	Item key;
	PQlink prev, next;
};

struct pq
{
	PQlink head, tail;
};

PQ PQinit()
{
	PQ pq = (PQ)malloc(sizeof(*pq));
	PQlink h = (PQlink)malloc(sizeof(*h)),
		   t = (PQlink)malloc(sizeof(*t));
	h->prev = t;
	h->next = t;
	t->prev = h;
	t->next = h;
	pq->head = h;
	pq->tail = t;

	return pq;
}

int PQempty(PQ pq)
{
	return pq->head->next->next == pq->head;
}

PQlink PQinsert(PQ pq, Item v)
{
	PQlink t = (PQlink)malloc(sizeof(*t));
	t->key = v;
	t->next = pq->head->next;
	t->next->prev = t;
	t->prev = pq->head;
	pq->head->next = t;

	return t;
}

Item PQdelmax(PQ pq)
{
	Item max;
	struct PQnode *t, *x = pq->head->next;

	for(t = x; t->next != pq->head; t = t->next)
	{
		if(t->key > x->key)
		{
			x = t;
		}
	}

	max = x->key;
	x->next->prev = x->prev;
	x->prev->next = x->next;

	free(x);
	return max;
}

/**
 * Doubly-linked-list priority queue (continued) Program 9.10
 * 
 * The overhead of maintaining doubly-linked lists is justified by the fact that
 * the change priority, delete, and join operations are also implemented in
 * constant time, again  using only elementary operations on the lists (see
 * Chapter 3 for more details on doubly linked lists).
 **/

void PQchange(PQ pq, PQlink x, Item v)
{
	x->key = v;
}

void PQdelete(PQ pq, PQlink x)
{
	x->next->prev = x->prev;
	x->prev->next = x->next;
	free(x);
}

void PQjoin(PQ a, PQ b)
{
	a->tail->prev->next = b->head->next;
	b->head->next->prev = a->tail->prev;
	a->head->prev = b->tail;
	b->tail->next = a->head;

	free(a->tail);
	free(b->head);
}
