/**
 * Radix Sort
 *
 * Underlying sorting function must be stable!
 *
 * For each "position" in the values to be sorted
 * - Use a stable sorting algorithm to sort each number by the i-th position
 *   from least significant to most significant.
 **/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdint.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

// NOTE(rick): This counting sort implementation was borrowed and modified from
// countingsort.cpp, see that file for a detailed explanation of the code.
// NOTE(rick): Added a Digit function pointer parameter and a Plce parameter to
// allow the CountingSort code to sort on the i-th digit of a number.
static inline void
CountingSort(int32_t *Array, int32_t Length, int32_t Range,
			 int32_t (*Digit)(int32_t,int32_t), int32_t Place)
{
	int32_t *Count = (int32_t *)malloc(sizeof(*Count) * Range);
	for(int32_t CountIndex = 0; CountIndex < Range; ++CountIndex)
	{
		Count[CountIndex] = 0;
	}

	for(int32_t ArrayIndex = 0; ArrayIndex < Length; ArrayIndex++)
	{
		Count[Digit(Array[ArrayIndex], Place)]++;
	}

	for(int32_t CountIndex = 1; CountIndex < Range; ++CountIndex)
	{
		Count[CountIndex] += Count[CountIndex - 1];
	}

	int32_t *SortedArray = (int32_t *)malloc(sizeof(*SortedArray) * Length);
	for(int ArrayIndex = Length - 1; ArrayIndex >= 0; --ArrayIndex)
	{
		SortedArray[--Count[Digit(Array[ArrayIndex], Place)]] = Array[ArrayIndex];
	}

	for(int32_t Index = 0; Index < Length; ++Index)
	{
		Array[Index] = SortedArray[Index];
	}

	free(Count);
	free(SortedArray);
}

static inline int32_t
GetDigitFromNumber(int32_t Number, int32_t Place)
{
	for(int i = 0; i < Place; i++)
	{
		Number = Number / 10;
	}
	int32_t Result = Number % 10;

	return(Result);
}

static inline void
RadixSortLSD(int32_t *Array, int32_t Length, int32_t Radix)
{
	for(int Position = 0; Position < Radix; ++Position)
	{
		CountingSort(Array, Length, 10, GetDigitFromNumber, Position);
	}
}

int main(void)
{
	int32_t Array[] = {123, 213, 321, 312, 311, 112, 131, 231, 123};
	RadixSortLSD(Array, ArrayCount(Array), 3);
	for(int32_t Index = 0; Index < ArrayCount(Array) - 1; ++Index)
	{
		printf("%d, ", Array[Index]);
		assert(Array[Index] <= Array[Index + 1]);
	}
	printf("%d\n", Array[ArrayCount(Array) - 1]);

	return 0;
}
