/**
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

typedef struct set* set_t;
struct set
{
	int32_t Size;
	int32_t Count;
	int32_t *Array;
};

static inline set_t
NewSet(int32_t SizeHint = 1)
{
	set_t Result = (set_t)malloc(sizeof(*Result));
	assert(Result != 0);

	Result->Size = SizeHint;
	Result->Count = 0;
	Result->Array = (int32_t *)malloc(sizeof(Result->Array) * Result->Size);
	assert(Result->Array != 0);

	return(Result);
}

static inline int32_t
SetPush(set_t Set, int32_t Value)
{
	assert(Set != 0);

	while(Set->Count + 1 > Set->Size)
	{
		int32_t NewSize = Set->Size * 2;
		int32_t *NewArray = (int32_t *)malloc(sizeof(int32_t *) * NewSize);
		assert(NewArray != 0);

		Set->Array = NewArray;
		Set->Size = NewSize;
	}

	Set->Array[Set->Count++] = Value;
	return(Value);
}

static inline int32_t
SetPop(set_t Set)
{
	assert(Set != 0);
	assert(Set->Count >= 0);

	int32_t Result = Set->Array[--Set->Count];
	return(Result);
}

static inline void
PrintSet(set_t Set)
{
	for(int Index = 0; Index < Set->Count; ++Index)
	{
		printf("%d ", Set->Array[Index]);
	}
	printf("\n");
}

static void
PermuteDiceSumHelper(int32_t Dice,
					 int32_t CurrentSum, int32_t TargetSum,
					 set_t Set)
{
	if(Dice == 0)
	{
		if(CurrentSum == TargetSum)
		{
			PrintSet(Set);
		}
		return;
	}

	for(int i = 1; i <= 6; ++i)
	{
		if(CurrentSum + i <= TargetSum)
		{
			SetPush(Set, i);
			PermuteDiceSumHelper(Dice - 1, CurrentSum + i, TargetSum, Set);
			SetPop(Set);
		}
	}
}

static void
PermuteDiceSum(int32_t Dice, int32_t TargetSum)
{
	set_t Rolls = NewSet(Dice);
	PermuteDiceSumHelper(Dice, 0, TargetSum, Rolls);
}

static void
PermuteDiceHelper(int32_t Dice, set_t Set)
{
	if(Dice == 0)
	{
		PrintSet(Set);
		return;
	}

	for(int i = 1; i <= 6; ++i)
	{
		SetPush(Set, i);
		PermuteDiceHelper(Dice - 1, Set);
		SetPop(Set);
	}
}

static void
PermuteDice(int32_t Dice)
{
	set_t Rolls = NewSet(Dice);
	PermuteDiceHelper(Dice, Rolls);
}

int main(void)
{
	PermuteDice(3);
	PermuteDiceSum(3, 7);
}
