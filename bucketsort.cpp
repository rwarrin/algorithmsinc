/**
 * Bucket Sort
 *
 * Split input into B buckets by converting the value to a number between [0,1)
 *
 * - Create an array of K empty lists (buckets)
 *   - Buckets are linked lists
 * - Get the maximum value of the array to map keys between 0 and 1
 * - Insert values Array[0,1...N] into buckets in sorted order
 * - Concatenate buckets [0,1...K] in order
 **/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

struct node
{
	int32_t Value;
	struct node* Next;
};

static inline void
BucketSort(int32_t *Array, int32_t Length, int32_t MaxValue, int32_t NumberOfBuckets)
{
	struct node **Buckets = (struct node **)malloc(sizeof(*Buckets) * NumberOfBuckets);
	for(int32_t BucketIndex = 0; BucketIndex < NumberOfBuckets; ++BucketIndex)
	{
		Buckets[BucketIndex] = 0;
	}

	for(int Index = 0; Index < Length; ++Index)
	{
		int32_t BucketIndex = ((Array[Index]/MaxValue)*NumberOfBuckets);
		struct node *NewNode = (struct node *)malloc(sizeof(*NewNode));
		NewNode->Value = Array[Index];
		NewNode->Next = 0;

		if((Buckets[BucketIndex] == 0) ||
		   (NewNode->Value <= Buckets[BucketIndex]->Value))
		{
			NewNode->Next = Buckets[BucketIndex];
			Buckets[BucketIndex] = NewNode;
		}
		else
		{
			for(struct node *Walker = Buckets[BucketIndex]; Walker != 0; Walker = Walker->Next)
			{
				if(Walker->Next == 0)
				{
					Walker->Next = NewNode;
					break;
				}
				else if(NewNode->Value <= Walker->Next->Value)
				{
					NewNode->Next = Walker->Next;
					Walker->Next = NewNode;
					break;
				}
			}
		}
	}

	int32_t ArrayIndex = 0;
	for(int32_t BucketIndex = 0; BucketIndex < NumberOfBuckets; ++BucketIndex)
	{
		for(struct node *Node = Buckets[BucketIndex]; Node != NULL; Node = Node->Next)
		{
			Array[ArrayIndex++] = Node->Value;
		}
	}
}

int main(void)
{
	printf("Hello world\n");

	int32_t Array[] = {1, 4, 3, 9, 2, 0, 6, 7, 5, 8, 3, 4, 9, 2, 7, 2};
	BucketSort(Array, ArrayCount(Array), 10, 5);
	for(int32_t Index = 0; Index < ArrayCount(Array) - 1; ++Index)
	{
		printf("%d, ", Array[Index]);
		assert(Array[Index] <= Array[Index + 1]);
	}
	printf("%d\n", Array[ArrayCount(Array) - 1]);

	return 0;
}
