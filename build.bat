@echo off

SET CompilerFlags=/nologo /Z7 /Od /fp:fast /favor:INTEL64 /Fe:app.exe
SET LinkerFlags=/incremental:no

SET Source=%1

IF NOT EXIST build mkdir build
pushd build

cl.exe %CompilerFlags% ..\%Source% /link %LinkerFlags%

popd

