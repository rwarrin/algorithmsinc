/**
 * Key-indexed sorting
 * Linear time sorting.
 *
 * Treat each key as an index into an array. Count the number of occurences of
 * each unique key. Sum each index of the count array with the previous one,
 * this will give the offset into the output sorted array where each key ends.
 * For each key in the input look it up in the count array and insert it into
 * the output array at count - 1.
 *
 * Input: 1 3 2 5 2 1
 * Count Array:
 *	0 1 2 3 4 5 6 7 8 9
 *  0 2 2 1 0 1 0 0 0 0
 *
 * Sum the count array
 *  0 1 2 3 4 5 6 7 8 9
 *  0 2 4 5 5 6 6 6 6 6
 *
 * Loop over input array and look up the count in Count array and insert at
 * Count-1. After each inserting reduce count by 1 for the key inserted.
 *
 * for 1 insert at Output[2-1] = 1 (Output[1] = 1)
 * for 3 insert at Output[5-1] = 3 (Output[4] = 3)
 * for 2 insert at Output[4-1] = 2 (Output[3] = 2)
 * for 5 insert at Output[6-1] = 5 (Output[5] = 5)
 * for 2 insert at Output[3-1] = 2 (Output[2] = 2)
 * for 1 insert at Output[1-1] = 1 (output[0] = 1)
 *
 * Final sorted array is now
 * 0 1 2 3 4 5 6 7 8 9 0
 * 1 1 2 2 3 5
 **/

#include <stdio.h>
#include <stdlib.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

static void
CountingSort(char *Array, int Length, int Range)
{
	int *Count = (int *)malloc(sizeof(int) * Range);
	for(int i = 0; i < Range; ++i)
	{
		Count[i] = 0;
	}

	for(int i = 0; i < Length; ++i)
	{
		++Count[Array[i]];
	}

	for(int i = 1; i < Range; ++i)
	{
		Count[i] = Count[i] + Count[i - 1];
	}

	char *Sorted = (char *)malloc(sizeof(char) * Length);
	for(int i = 0; i < Length; ++i)
	{
		Sorted[Count[Array[i]] - 1] = Array[i];
		--Count[Array[i]];
	}

	for(int i = 0; i < Length; ++i)
	{
		Array[i] = Sorted[i];
	}
}

int
main(void)
{
	char Array[] = {'h', 'e', 'l', 'l', 'o', 'w', 'o', 'r', 'l', 'd'};
	CountingSort(Array, ArrayCount(Array), 256);
	for(int i = 0; i < ArrayCount(Array); ++i)
	{
		printf("%c", Array[i]);
	}
	printf("\n");

	return 0;
}
