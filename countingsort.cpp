/**
 * Counting Sort
 *
 * - Count the number of occurences of each value in a Range
 * - Sum previous counts to determine the number of values <= each Value
 * - Starting from the end of the array copy values to (PreviousValues+Count)-1
 *   location.
 * - Copy sorted array back to input array
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

static inline void
CountingSort(int32_t *Array, int32_t Length, int32_t Range)
{
	int32_t *Count = (int32_t *)malloc(sizeof(*Count) * Range);
	for(int32_t CountIndex = 0; CountIndex < Range; ++CountIndex)
	{
		Count[CountIndex] = 0;
	}

	for(int32_t ArrayIndex = 0; ArrayIndex < Length; ArrayIndex++)
	{
		Count[Array[ArrayIndex]]++;
	}

	for(int32_t CountIndex = 1; CountIndex < Range; ++CountIndex)
	{
		Count[CountIndex] += Count[CountIndex - 1];
	}

	int32_t *SortedArray = (int32_t *)malloc(sizeof(*SortedArray) * Length);
	for(int ArrayIndex = Length - 1; ArrayIndex >= 0; --ArrayIndex)
	{
		SortedArray[--Count[Array[ArrayIndex]]] = Array[ArrayIndex];
	}

	for(int32_t Index = 0; Index < Length; ++Index)
	{
		Array[Index] = SortedArray[Index];
	}

	free(Count);
	free(SortedArray);
}

static inline void
PrintArray(int32_t *Array, int32_t Length)
{
	for(int32_t Index = 0; Index < Length; ++Index)
	{
		printf("%d, ", Array[Index]);
	}
	printf("\n");
}

int main(void)
{
	int32_t Array[] = {1, 3, 2, 7, 4, 5, 9, 8, 6, 2, 9, 1, 3, 3, 7, 4, 0};
	PrintArray(Array, ArrayCount(Array));

	CountingSort(Array, ArrayCount(Array), 10);
	PrintArray(Array, ArrayCount(Array));

	return 0;
}
