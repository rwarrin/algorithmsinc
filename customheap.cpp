/**
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef struct job* item_t;
typedef struct job* job_t;
struct job
{
	int Priority;
	int Id;
};

typedef int (*key_func)(item_t);
typedef struct heap* heap_t;
struct heap
{
	int Count;
	int Size;
	item_t *HeapArray;
	key_func Key;
};

heap_t
HeapNew(int SizeHint, key_func KeyFunction)
{
	heap_t NewHeap = (heap_t)malloc(sizeof(*NewHeap) + (sizeof(*(NewHeap->HeapArray)) * (SizeHint + 1)));
	NewHeap->HeapArray = (item_t *)(NewHeap + 1);
	NewHeap->Size = SizeHint;
	NewHeap->Count = 0;
	NewHeap->Key = KeyFunction;

	return NewHeap;
}

static void
HeapSwap(item_t *A, item_t *B)
{
	item_t Temp = *A;
	*A = *B;
	*B = Temp;
}

static void
HeapFixUp(int Index, heap_t Heap)
{
	assert(Heap != NULL);

	while((Index > 1) &&
		  (Heap->Key(Heap->HeapArray[Index/2]) < Heap->Key(Heap->HeapArray[Index])))
	{
		HeapSwap(&Heap->HeapArray[Index], &Heap->HeapArray[Index/2]);
		Index = Index / 2;
	}
}

static void
HeapFixDown(int Index, heap_t Heap)
{
	assert(Heap != NULL);

	int HeapEnd = Heap->Count - 1;
	while(Index*2 <= HeapEnd)
	{
		int TryAt = Index*2;
		if((TryAt < HeapEnd) &&
		   (Heap->Key(Heap->HeapArray[TryAt]) < Heap->Key(Heap->HeapArray[TryAt + 1])))
		{
			TryAt++;
		}
		if(Heap->Key(Heap->HeapArray[Index]) > Heap->Key(Heap->HeapArray[TryAt]))
		{
			break;
		}

		HeapSwap(&Heap->HeapArray[Index], &Heap->HeapArray[TryAt]);
		Index = TryAt;
	}
}

void
HeapInsert(item_t Item, heap_t Heap)
{
	assert(Heap != NULL);
	Heap->HeapArray[++Heap->Count] = Item;
	HeapFixUp(Heap->Count, Heap);
}

item_t
HeapPop(heap_t Heap)
{
	assert(Heap != NULL);
	HeapSwap(&Heap->HeapArray[1], &Heap->HeapArray[Heap->Count]);
	HeapFixDown(1, Heap);

	item_t Result = Heap->HeapArray[Heap->Count--];
	return Result;
}

int
HeapEmpty(heap_t Heap)
{
	assert(Heap != NULL);
	int Result = (Heap->Count == 0);
	return Result;
}

job_t
NewJob(int Id, int Priority)
{
	job_t Result = (job_t)malloc(sizeof(*Result));
	Result->Id = Id;
	Result->Priority = Priority;

	return Result;
}

int
JobKey(item_t Item)
{
	int Result = ((job_t)Item)->Priority;
	return Result;
}

static int
main(void)
{
	heap_t Heap = HeapNew(32, JobKey);
	HeapInsert((item_t)NewJob(0, 5), Heap);
	HeapInsert((item_t)NewJob(1, 3), Heap);
	HeapInsert((item_t)NewJob(2, 1), Heap);
	HeapInsert((item_t)NewJob(3, 2), Heap);
	HeapInsert((item_t)NewJob(4, 4), Heap);
	HeapInsert((item_t)NewJob(5, 9), Heap);
	HeapInsert((item_t)NewJob(6, 0), Heap);
	HeapInsert((item_t)NewJob(7, 4), Heap);
	HeapInsert((item_t)NewJob(8, 4), Heap);

	while(!HeapEmpty(Heap))
	{
		job_t Value = (job_t)HeapPop(Heap);
		printf("Job %d, Priority %d\n", Value->Id, Value->Priority);
		free(Value);
	}

	printf("Test\n");
	return 0;
}


