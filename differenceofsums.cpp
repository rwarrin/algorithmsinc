/**
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))
#define Abs(A) (A < 0 ? A*-1 : A)
#define MIN(A, B) (A < B ? A : B)

inline int
DifferenceOfSums(int *Array, int Length, int CurrentSum, int TotalSum)
{
	if(Length == 0)
	{
		return(Abs((TotalSum - CurrentSum) - CurrentSum));
	}

	int Result = MIN(DifferenceOfSums(Array, Length - 1, CurrentSum + Array[Length - 1], TotalSum),
					 DifferenceOfSums(Array, Length - 1, CurrentSum, TotalSum));

	return(Result);
}

int
main(void)
{
	printf("Hello world\n");

	int Array[] = {1, 6, 11, 5};
	int TotalSum = 0;
	for(int i = 0; i < ArrayCount(Array); ++i)
	{
		TotalSum += Array[i];
	}

	int Min = DifferenceOfSums(Array, ArrayCount(Array), 0, TotalSum);
	printf("DifferenceOfSums = %d\n", Min);

	return 0;
}
