/**
 *
 **/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

static inline int
Distances_TopDownDP(int Distance, int *DistanceSizes, int DistanceSizesLength, int *MemoizedArray)
{
	if(Distance < 0)
	{
		return(0);
	}

	if(Distance == 0)
	{
		return(1);
	}

	if(MemoizedArray[Distance] != -1)
	{
		return(MemoizedArray[Distance]);
	}

	int Result = 0;
	for(int i = 0; i < DistanceSizesLength; ++i)
	{
		Result += Distances_TopDownDP(Distance - DistanceSizes[i], DistanceSizes, DistanceSizesLength, MemoizedArray);
	}

	MemoizedArray[Distance] = Result;
	return(Result);
}

static inline int
Distances_BottomUpDP(int Distance, int *DistanceSizes, int DistanceSizesLength)
{
	int *MemoizedArray = (int *)malloc(sizeof(*MemoizedArray)*(Distance + 1));
	MemoizedArray[0] = 1;

	for(int i = 1; i <= Distance; ++i)
	{
		int Ways = 0;
		for(int j = 0; j < DistanceSizesLength; ++j)
		{
			if(i - DistanceSizes[j] >= 0)
			{
				Ways += MemoizedArray[i - DistanceSizes[j]];
			}
		}

		MemoizedArray[i] = Ways;
	}

	return(MemoizedArray[Distance]);
}

int
main(void)
{
	printf("Hello world\n");

	int Distance = 59;
	int DistanceSizes[] = {1, 2, 3};
	int *MemoizedArray = (int *)malloc(sizeof(*MemoizedArray)*(Distance + 1));
	for(int i = 0; i < Distance + 1; ++i)
	{
		MemoizedArray[i] = -1;
	}

	int WaysTD = Distances_TopDownDP(Distance, DistanceSizes, ArrayCount(DistanceSizes), MemoizedArray);
	printf("Distances_TopDownDP() = %d\n", WaysTD);

	int WaysBU = Distances_BottomUpDP(Distance, DistanceSizes, ArrayCount(DistanceSizes));
	printf("Distances_BottomUpDP() = %d\n", WaysBU);

	return 0;
}
