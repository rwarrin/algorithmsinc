/**
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))
#define MIN(A, B) ((A) < (B) ? A : B)

#define Sentinel (INT_MAX - 1)

static int
MinCoins(int Value, int *Coins, int CoinsLength, int *MemoizedArray)
{
	if(Value < 0)
	{
		return Sentinel;
	}

	if(Value == 0)
	{
		return 0;
	}

	if(MemoizedArray[Value] != Sentinel)
	{
		//return MemoizedArray[Value];
	}

	int Min = Sentinel;
	for(int CoinIndex = 0; CoinIndex < CoinsLength; ++CoinIndex)
	{
		Min = MIN(Min, MinCoins(Value - Coins[CoinIndex], Coins, CoinsLength, MemoizedArray) + 1);
	}

	MemoizedArray[Value] = Min;
	return Min;
}

int
main(void)
{
	int Coins[] = {1, 5, 10};
	int Value = 26;
	int *MemoizedArray = (int *)malloc(sizeof(int)*Value + 1);
	assert(MemoizedArray != NULL);
	for(int i = 0; i < Value + 1; i++)
	{
		MemoizedArray[i] = Sentinel;
	}

	int Result = MinCoins(Value, Coins, ArrayCount(Coins), MemoizedArray);
	printf("Min Coins = %d\n", Result);

	return 0;
}
