/**
 * Dynamic Programming with Memoization
 *
 * This program finds the minimum number of coins that sum up to some value. The
 * dynamic programming with memoization breaks the problem down into smaller
 * problems and then remembers those values for reuse later.
 *
 *          INF		x < 0
 * solve(x) 0		x = 0
 *		    min(ccoins)solve(x-c)+1
 *
 *	The above solve(x) function says (i) if x is less than 0 then return
 *	infinity so this branch will not be a possible solution. (ii) if x is zero
 *	then return 0 because there are 0 ways to return 0 coins. (iii) for each
 *	coin in coins get the minimum number of coins from the previous min and
 *	calling the function again with the value-coin value, adding 1 because the
 *	current coin tested needs to be counted towards the total coins required.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>
#include <windows.h>

#define MIN(A, B) (((A) < (B)) ? A : B)
#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

#define Sentinel (INT_MAX - 1)

static int
MinCoins(int Value, int *Coins, int CoinsLength, int *MemoizedValues)
{
	if(Value < 0)
	{
		return Sentinel;
	}

	if(Value == 0)
	{
		return 0;
	}

	if(MemoizedValues[Value] != Sentinel)
	{
		return MemoizedValues[Value];
	}

	int Min = Sentinel;
	for(int CoinIndex = 0; CoinIndex < CoinsLength; ++CoinIndex)
	{
		Min = MIN(Min, MinCoins(Value - Coins[CoinIndex], Coins, CoinsLength, MemoizedValues) + 1);
	}

	MemoizedValues[Value] = Min;
	return Min;
}

static int
main(void)
{
	int Value = 36;
	int Coins[] = {1, 5, 10, 25};
	int *MemoizedValues = (int *)malloc(sizeof(int) * Value + 1);
	assert(MemoizedValues != 0);
	for(int i = 0; i < Value + 1; ++i)
	{
		MemoizedValues[i] = Sentinel;
	}

	LARGE_INTEGER TimeStart = {0};
	LARGE_INTEGER TimeEnd = {0};
	LARGE_INTEGER Frequency = {0};

	QueryPerformanceFrequency(&Frequency);
	QueryPerformanceCounter(&TimeStart);
	int Result = MinCoins(Value, Coins, ArrayCount(Coins), MemoizedValues);
	QueryPerformanceCounter(&TimeEnd);

	int CyclesElapsed = TimeEnd.QuadPart - TimeStart.QuadPart;
	float MicrosecondsElapsed = (float)CyclesElapsed / (float)Frequency.QuadPart;
	float MillisecondsElapsed = MicrosecondsElapsed * 1000.0f;
	printf("MinCoins(%d) = %d\n", Value, Result);
	printf("%12dcy %.5fms\n", CyclesElapsed, MillisecondsElapsed);

	return 0;
}
