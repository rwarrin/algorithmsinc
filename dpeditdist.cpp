/**
 * FIGURE OUT HOW TO MEMOIZE THIS SHIT
 *
 * General recursive function:
 *
 *			a < 0		0
 * med(a,b) b < 0		0
 *			min( med(a,b-1)+1, med(a-1,b)+1, med(a-1,b-1) )
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define MIN(A, B) ((A) < (B) ? A : B)
#define MIN3(A, B, C) (MIN(MIN(A, B), C))

#define Sentinel (INT_MAX - 10000)

static inline void
ArraySet(int Value, int *Array, int Y, int X, int Pitch)
{
	assert(Array != NULL);
	*(Array + ((Y + 1) * Pitch) + (X + 1)) = Value;
}

static inline int
ArrayGet(int *Array, int Y, int X, int Pitch)
{
	assert(Array != NULL);
	int Result = *(Array + ((Y + 1) * Pitch) + (X + 1));
	return Result;
}

static int
_EditDistance(char *StringOne, int StringOneLength,
			  char *StringTwo, int StringTwoLength,
			  int *MemoizedDistances, int Pitch)
{
	if(StringOneLength == 0)
	{
		return StringTwoLength;
	}

	if(StringTwoLength == 0)
	{
		return StringOneLength;
	}

	int MemoizedDistance = ArrayGet(MemoizedDistances, StringOneLength - 1, StringTwoLength - 1, Pitch);
	if(MemoizedDistance != Sentinel)
	{
		return MemoizedDistance;
	}

	int Insert = _EditDistance(StringOne, StringOneLength, StringTwo, StringTwoLength - 1, MemoizedDistances, Pitch) + 1;
	int Delete = _EditDistance(StringOne, StringOneLength - 1, StringTwo, StringTwoLength, MemoizedDistances, Pitch) + 1;
	int Change = _EditDistance(StringOne, StringOneLength - 1, StringTwo, StringTwoLength - 1, MemoizedDistances, Pitch) + (StringOne[StringOneLength - 1] == StringTwo[StringTwoLength - 1] ? 0 : 1);
	int Result = MIN3(Insert, Delete, Change);

	ArraySet(Result, MemoizedDistances, StringOneLength - 1, StringTwoLength - 1, Pitch);
	return Result;
}

static int
EditDistance(char *StringOne, char *StringTwo)
{
	assert(StringOne != 0);
	assert(StringTwo != 0);

	int StringOneLength = strlen(StringOne);
	int StringTwoLength = strlen(StringTwo);

	int ArrayLength = (StringOneLength + 1)*(StringTwoLength + 1);
	int Pitch = StringTwoLength + 1;
	int *MemoizedDistances = (int *)malloc(sizeof(int) * ArrayLength);
	for(int i = 0; i < ArrayLength; i++)
	{
		*(MemoizedDistances + i) = Sentinel;
	}

	int Result = _EditDistance(StringOne, StringOneLength,
							   StringTwo, StringTwoLength,
							   MemoizedDistances, Pitch);

	free(MemoizedDistances);
	return Result;
}

static int
main(void)
{
	char *StringOne = "this is a really long string that has nothing to do with the first";
	char *StringTwo = "to be or not to be that is the question";
	int Distance = EditDistance(StringOne, StringTwo);
	printf("MinimumEditDistance(%s, %s) = %d\n", StringOne, StringTwo, Distance);

	return 0;
}
