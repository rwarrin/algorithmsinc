/**
 * Dynamic Programming
 *
 * There are multiple examples here, see each function for a detailed
 * explanation of each.
 *
 * Top-Down DP is the easiest to think about because it is a simple
 * transformation of the basic recursive solution.
 *
 * Bottom-Up DP is a little more complicated because to solve larger problems we
 * first solve smaller problems, think of fibonnaci in that F(N) = F(N-1)+F(N-2)
 * meaning that to Sove N we first solve N-1 and N-2, but starting at 0 and
 * counting up to N and looking back and previous solutions.
 **/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <limits.h>

#define MAX(A, B) (A > B ? A : B)
#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

/**
 * This is the basic recursive solution to the rod cutting problem. It does not
 * employ any sort of dynamic programming memoization strategy. For every
 * sub-problem it must recalculate each solution every time.
 **/
inline int32_t
CutRod_TopDownNoDP(int32_t RodLength, int32_t *Prices, int32_t Length)
{
	if(RodLength == 0)
	{
		return 0;
	}

	int32_t Max = INT_MIN;
	for(int32_t TrySize = 1; TrySize <= RodLength; ++TrySize)
	{
		Max = MAX(Max, Prices[TrySize] + CutRod_TopDownNoDP(RodLength - TrySize, Prices, Length));
	}

	return(Max);
}

/**
 * Top-Down Dynamic Programming
 *
 * Comparing this to the pure recursive solution above it is nearly identical.
 * The only modifications is that now the function has a parameter for an extra
 * array _MemoizedArray_ which stores previously calculated solutions. The
 * recursive function first checks for the presence of a previous solution and
 * returns it if found, otherwise the function does the work to compute the
 * solution for this sub-problem. Before returning the solution to a problem,
 * the function stores the result in to the Memoized array so it can be reused
 * later if neccessary.
 **/
inline int32_t
CutRod_TopDownDP(int32_t RodLength,
				 int32_t *Prices, int32_t PricesLength,
				 int32_t *MemoizedArray, int32_t MemoizedArrayLength)
{
	if(MemoizedArray[RodLength] != INT_MIN)
	{
		return MemoizedArray[RodLength];
	}

	if(RodLength == 0)
	{
		return 0;
	}

	int32_t Max = INT_MIN;
	for(int32_t TrySize = 1; TrySize <= RodLength; ++TrySize)
	{
		Max = MAX(Max, Prices[TrySize] + CutRod_TopDownDP(RodLength - TrySize, Prices, PricesLength, MemoizedArray, MemoizedArrayLength));
	}

	MemoizedArray[RodLength] = Max;
	return(Max);
}

/**
 * Bottom-Up Dynamic Programming
 *
 * In this function we first solve the smaller sub-problems inorder to solve the
 * larger problems. _MemoizedArray_ is initialized to INT_MIN except for 0,
 * which is the base case (a rod length 0 is 0 profit). From there we are able
 * to calculate the increasingly larger rode cut sizes by looking at previously
 * calculated solutions.
 **/
inline int32_t
CutRod_BottomUpDP(int32_t RodLength, int32_t *Prices, int32_t PricesLength)
{
	int32_t *MemoizedArray = (int32_t *)malloc(sizeof(*MemoizedArray)*PricesLength);
	for(int32_t i = 0; i < PricesLength; ++i)
	{
		MemoizedArray[i] = INT_MIN;
	}
	MemoizedArray[0] = 0;

	int32_t Max = INT_MIN;
	for(int32_t Size = 0; Size <= RodLength; ++Size)
	{
		for(int32_t TrySize = 0; TrySize <= Size; ++TrySize)
		{
			Max = MAX(Max, Prices[TrySize] + MemoizedArray[Size - TrySize]);
		}
		MemoizedArray[Size] = Max;
	}

	return(MemoizedArray[RodLength]);
}

/**
 * This function is identical to the CutRod_TopDownDP function above, with the
 * exception that we have added yet another array to save values to use to
 * reconstruct the solution to a problem. Whenever we find a new solution to a
 * problem we save information about how we got to that solution so it can later
 * be used to reconstruct the intermediate problems that were used to solve the
 * larger problem.
 * In the case of this function, whenever we find a new Max profit value we
 * store the _TrySize_ which is the cut length which provides the largest profit
 * for the current rod length. See _PrintSolutionArray_ for how this
 * reconstruction solution array is used.
 **/
inline int32_t
CutRod_TopDownDP_Solution(int32_t RodLength,
						  int32_t *Prices, int32_t PricesLength,
						  int32_t *MemoizedArray, int32_t *SolutionArray)
{
	if(MemoizedArray[RodLength] != INT_MIN)
	{
		return MemoizedArray[RodLength];
	}

	if(RodLength == 0)
	{
		return 0;
	}

	int32_t Max = INT_MIN;
	for(int32_t TrySize = 1; TrySize <= RodLength; ++TrySize)
	{
		int32_t TempMax = MAX(Max, Prices[TrySize] + CutRod_TopDownDP_Solution(RodLength - TrySize, Prices, PricesLength, MemoizedArray, SolutionArray));
		if(TempMax > Max)
		{
			SolutionArray[RodLength] = TrySize;
		}

		Max = TempMax;
	}

	MemoizedArray[RodLength] = Max;
	return(Max);
}

/**
 * This function is not dynamic programming, its purpose is to print the
 * sub-problems that produce the solution for a given problem. We start at the
 * problem size and find the the value which we should use to modify the problem
 * to get to one of the sub-problems. We iterate through the sub-problems while
 * there are still problems to solve, each time printing the calculated solution
 * for that step.
 **/
inline void
PrintSolutionArray(int32_t RodSize, int32_t *SolutionsArray)
{
	while(RodSize > 0)
	{
		printf("%d ", SolutionsArray[RodSize]);
		RodSize = RodSize - SolutionsArray[RodSize];
	}

	printf("\n");
}

int
main(void)
{
	// NOTE(rick): A whole lot of testing code, it's not pretty but it
	// demonstrates each of the function above.
	int32_t Prices[] = {0, 1, 5, 8, 9, 10, 17, 17, 20, 24, 30};
	int32_t RodLength = 4;


	int32_t MaxProfit = CutRod_TopDownNoDP(RodLength, Prices, ArrayCount(Prices));
	printf("CutRod_TopDownNoDP(%d) = %d\n", RodLength, MaxProfit);


	int PricesCount = ArrayCount(Prices);
	int32_t *TopDownMemoizedArray = (int32_t *)malloc(sizeof(*TopDownMemoizedArray)*PricesCount);
	for(int32_t i = 0; i < PricesCount; ++i)
	{
		TopDownMemoizedArray[i] = INT_MIN;
	}
	MaxProfit = CutRod_TopDownDP(RodLength,
								 Prices, PricesCount,
								 TopDownMemoizedArray, PricesCount);
	printf("CutRod_TopDownDP(%d) = %d\n", RodLength, MaxProfit);


	MaxProfit = CutRod_BottomUpDP(RodLength, Prices, PricesCount);
	printf("CutRod_BottomUpDP(%d) = %d\n", RodLength, MaxProfit);


	int32_t *TopDownMemoizedWSolutionArray = (int32_t *)malloc(sizeof(*TopDownMemoizedWSolutionArray)*PricesCount);
	int32_t *TopDownSolutionArray = (int32_t *)malloc(sizeof(*TopDownSolutionArray)*PricesCount);
	for(int32_t i = 0; i < PricesCount; ++i)
	{
		TopDownMemoizedWSolutionArray[i] = INT_MIN;
		TopDownSolutionArray[i] = INT_MIN;
	}
	MaxProfit = CutRod_TopDownDP_Solution(RodLength, Prices, PricesCount,
										  TopDownMemoizedWSolutionArray,
										  TopDownSolutionArray);
	printf("CutRod_TopDownDP_Solution(%d) = %d\n", RodLength, MaxProfit);
	PrintSolutionArray(RodLength, TopDownSolutionArray);

	return 0;
}
