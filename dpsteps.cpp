/**
 *
 *
 * Recursive Formula is:
 *					x < 0		0
 * CountSteps(x)    x = 0		1
 *					s(stepsizes)countsteps(x-s)
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))
#define Sentinel (INT_MAX - 100)

static int
CountSteps(int StepCount, int *StepSizes, int StepSizesCount, int *MemoizedStepCount)
{
	printf("StepCount = %d\n", StepCount);
	if(StepCount < 0)
	{
		return 0;
	}

	if(StepCount == 0)
	{
		return 1;
	}

	if(MemoizedStepCount[StepCount] != Sentinel)
	{
		return MemoizedStepCount[StepCount];
	}

	int Result = 0;
	for(int StepIndex = 0; StepIndex < StepSizesCount; ++StepIndex)
	{
		Result += CountSteps(StepCount - StepSizes[StepIndex], StepSizes, StepSizesCount, MemoizedStepCount);
	}

	MemoizedStepCount[StepCount] = Result;
	return Result;
}

static int
main(void)
{
	int StepSizes[] = {2, 3, 5};
	int StepCount = 12;
	int *MemoizedStepCount = (int *)malloc(sizeof(int) * StepCount + 1);
	for(int i = 0; i < StepCount+1; i++)
	{
		MemoizedStepCount[i] = Sentinel;
	}

	int Steps = CountSteps(StepCount, StepSizes, ArrayCount(StepSizes), MemoizedStepCount);
	printf("CountSteps(%d) = %d\n", StepCount, Steps);

	return 0;
}
