/**
 * Write a recursive program that finds the maximum element in an array, based
 * on comparing the first element in the array against the maximum element in
 * the rest of the array (computed recursively).
 **/

#include <stdio.h>

#define MAX(A, B) ((A) > (B) ? (A) : (B))
#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

int
Max(int *Array, int Length)
{
	if(Length == 1)
	{
		return *Array;
	}

	return MAX(*Array, Max(Array + 1, Length - 1));
}

int
main(void)
{
	int Array[] = {15, 1, 3, 5, 9, 21, 4, 6, 0, 13};
	int Maximum = Max(Array, ArrayCount(Array));
	printf("Max = %d\n", Maximum);

	return 0;
}
