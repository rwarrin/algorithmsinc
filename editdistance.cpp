/**
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#define MIN(A, B) (A < B ? A : B)

enum edit_type
{
	EditType_Unknown,
	EditType_None,
	EditType_Delete,
	EditType_Insert,
	EditType_Replace,
};

static inline int **
Make2DArray(int Rows, int Cols)
{
	int **Result = (int **)malloc(sizeof(*Result)*Rows);
	for(int Row = 0; Row < Rows; ++Row)
	{
		Result[Row] = (int *)malloc(sizeof(Result[Row])*Cols);
	}
	
	return(Result);
}

static inline void
PrintArray(int **Array, int Rows, int Cols)
{
	for(int Row = 0; Row < Rows; ++Row)
	{
		for(int Col = 0; Col < Cols; ++Col)
		{
			printf("%2d ", Array[Row][Col]);
		}
		printf("\n");
	}
}

static inline void
PrintReconstruction(int **ReconstructionArray, char *First, int FirstLength, char *Second, int SecondLength)
{
	if((FirstLength < 0) &&
	   (SecondLength < 0))
	{
		return;
	}

	if(FirstLength < 0)
	{
		PrintReconstruction(ReconstructionArray, First, FirstLength, Second, SecondLength - 1);
		printf(" +'%c' ", Second[SecondLength]);
		return;
	}
	else if(SecondLength < 0)
	{
		PrintReconstruction(ReconstructionArray, First, FirstLength - 1, Second, SecondLength);
		printf(" -'%c' ", First[FirstLength]);
		return;
	}

	int EditType = ReconstructionArray[SecondLength][FirstLength];
	if(EditType == EditType_None)
	{
		PrintReconstruction(ReconstructionArray, First, FirstLength - 1, Second, SecondLength - 1);
		printf("  '%c' ", First[FirstLength]);
	}
	else if(EditType == EditType_Delete)
	{
		PrintReconstruction(ReconstructionArray, First, FirstLength - 1, Second, SecondLength);
		printf(" -'%c' ", First[FirstLength]);
	}
	else if(EditType == EditType_Insert)
	{
		PrintReconstruction(ReconstructionArray, First, FirstLength, Second, SecondLength - 1);
		printf(" +'%c' ", Second[SecondLength]);
	}
	else  // EditType_Replace
	{
		PrintReconstruction(ReconstructionArray, First, FirstLength - 1, Second, SecondLength - 1);
		printf(" r'%c%c' ", First[FirstLength], Second[SecondLength]);
	}
}

static inline int
EditDistance_TopDownDP(char *First, int FirstLength, char *Second, int SecondLength, int **MemoizedArray, int **ReconstructionArray)
{
	if(SecondLength == 0)
	{
		return FirstLength;
	}

	if(FirstLength == 0)
	{
		return SecondLength;
	}

	if(MemoizedArray[SecondLength][FirstLength] != INT_MAX)
	{
		return MemoizedArray[SecondLength][FirstLength];
	}

	int Result = INT_MAX;
	if(First[FirstLength] == Second[SecondLength])
	{
		Result = EditDistance_TopDownDP(First, FirstLength - 1, Second, SecondLength - 1, MemoizedArray, ReconstructionArray);
		ReconstructionArray[SecondLength][FirstLength] = EditType_None;
	}
	else
	{
		// TODO(rick): Reconstruct the edits
		// Split each value out and write whichever is the smallest value to the
		// reconstruction array then print the edits
		int DeleteLength = EditDistance_TopDownDP(First, FirstLength - 1, Second, SecondLength, MemoizedArray, ReconstructionArray);
		int InsertLength = EditDistance_TopDownDP(First, FirstLength, Second, SecondLength - 1, MemoizedArray, ReconstructionArray);
		int ReplaceLength = EditDistance_TopDownDP(First, FirstLength - 1, Second, SecondLength - 1, MemoizedArray, ReconstructionArray);
		int MinimumDistance = MIN(DeleteLength, MIN(InsertLength, ReplaceLength));

		if(MinimumDistance == ReplaceLength)
		{
			ReconstructionArray[SecondLength][FirstLength] = EditType_Replace;
		}
		else if(MinimumDistance == InsertLength)
		{
			ReconstructionArray[SecondLength][FirstLength] = EditType_Insert;
		}
		else
		{
			ReconstructionArray[SecondLength][FirstLength] = EditType_Delete;
		}

		Result = 1 + MinimumDistance;
	}

	MemoizedArray[SecondLength][FirstLength] = Result;
	return(Result);
}

static inline int
EditDistance_BottomUpDP(char *First, char *Second)
{
	int FirstLength = strlen(First);
	int SecondLength = strlen(Second);

	int **DistanceArray = Make2DArray(SecondLength + 1, FirstLength + 1);
	for(int Row = 0; Row < SecondLength; ++Row)
	{
		for(int Col = 0; Col < FirstLength; ++Col)
		{
			DistanceArray[Row][Col] = INT_MAX;
		}
	}

	for(int Row = 0; Row <= SecondLength; ++Row)
	{
		for(int Col = 0; Col <= FirstLength; ++Col)
		{
			if(Row == 0)
			{
				DistanceArray[Row][Col] = Col;
			}
			else if(Col == 0)
			{
				DistanceArray[Row][Col] = Row;
			}
			else if(First[Col] == Second[Row])
			{
				DistanceArray[Row][Col] = DistanceArray[Row - 1][Col - 1];
			}
			else
			{
				int Distance = 1 + MIN(DistanceArray[Row - 1][Col - 1],
									   MIN(DistanceArray[Row - 1][Col],
										   DistanceArray[Row][Col - 1]));
				DistanceArray[Row][Col] = Distance;
			}
		}
	}

	PrintArray(DistanceArray, SecondLength + 1, FirstLength + 1);
	return(DistanceArray[SecondLength][FirstLength]);
}

int
main(void)
{
	printf("Hello world\n");

	char *First = "everything is too late";
	char *Second = "its never too late";
	int FirstLength = strlen(First);
	int SecondLength = strlen(Second);
	int **MemoizedArray = Make2DArray(SecondLength + 1, FirstLength + 1);
	int **ReconstructionArray = Make2DArray(SecondLength + 1, FirstLength + 1);
	for(int Row = 0; Row <= SecondLength; ++Row)
	{
		for(int Col = 0; Col <= FirstLength; ++Col)
		{
			MemoizedArray[Row][Col] = INT_MAX;
			ReconstructionArray[Row][Col] = EditType_Unknown;
		}
	}

	int EditDistanceTD = EditDistance_TopDownDP(First, FirstLength, Second, SecondLength, MemoizedArray, ReconstructionArray);
	printf("EditDistance_TopDownDP('%s', '%s') = %d\n", First, Second, EditDistanceTD);
	PrintArray(ReconstructionArray, SecondLength + 1, FirstLength + 1);
	PrintReconstruction(ReconstructionArray, First, FirstLength, Second, SecondLength);
	printf("\n");

	int EditDistanceBU = EditDistance_BottomUpDP(First, Second);
	printf("\nEditDistance_BottomUpDP('%s', '%s') = %d\n", First, Second, EditDistanceBU);

	return 0;
}
