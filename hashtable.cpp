/**
 * 
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>

enum
{
	HASHTABLE_DEFAULT_TABLE_SIZE = 101
};

struct hash_node
{
	void *Key;
	void *Data;
	struct hash_node *Next;
};

struct hash_table
{
	int32_t Size;
	struct hash_node **Table;

	uint32_t (*Hash)(void *, uint32_t);
	int32_t (*Compare)(void *, void *);
};

struct hash_table *
HashTableNew(int32_t Hint, int32_t (*Compare)(void *, void *), uint32_t (*Hash)(void *, uint32_t))
{
	int32_t Size = Hint;
	if(!Size)
	{
		Size = HASHTABLE_DEFAULT_TABLE_SIZE;
	}

	struct hash_table *Result = (struct hash_table *)malloc(sizeof(*Result) + (sizeof(*Result->Table)*Size));
	Result->Table = (struct hash_node **)(Result + 1);
	Result->Compare = Compare;
	Result->Hash = Hash;
	Result->Size = Size;

	for(int32_t Index = 0; Index < Result->Size; ++Index)
	{
		Result->Table[Index] = 0;
	}

	return(Result);
}

void
HashTableDelete(struct hash_table **HashTable)
{
	for(int32_t Index = 0; Index < (*HashTable)->Size; ++Index)
	{
		struct hash_node *List = ((*HashTable)->Table[Index]);
		for( ; List != 0; )
		{
			struct hash_node *Temp = List;
			List = List->Next;
			free(Temp);
		}
	}

	free(*HashTable);
	*HashTable = 0;
}

void *
HashTableInsert(struct hash_table *HashTable, void *Key, void *Data)
{
	uint32_t Hash = HashTable->Hash(Key, HashTable->Size);
	printf("%s => %u\n", (char *)Key, Hash);
	for(struct hash_node *Node = HashTable->Table[Hash]; Node != 0; Node = Node->Next)
	{
		if(HashTable->Compare(Key, Node->Key) == 0)
		{
			void *Result = Node->Data;
			Node->Data = Data;
			return(Result);
		}
	}

	struct hash_node *NewNode = (struct hash_node *)malloc(sizeof(*NewNode));
	NewNode->Key = Key;
	NewNode->Data = Data;
	NewNode->Next = HashTable->Table[Hash];
	HashTable->Table[Hash] = NewNode;

	return(Data);
}

void *
HashTableRemove(struct hash_table *HashTable, void *Key)
{
	void *Result = 0;

	uint32_t Hash = HashTable->Hash(Key, HashTable->Size);
	struct hash_node **NodePtr = &HashTable->Table[Hash];
	while((*NodePtr != 0) &&
		  (HashTable->Compare((*NodePtr)->Key, Key) != 0))
	{
		NodePtr = &((*NodePtr)->Next);
	}

	if(*NodePtr != 0)
	{
		Result = (*NodePtr)->Data;
		struct hash_node *Temp = *NodePtr;
		*NodePtr = Temp->Next;
		free(Temp);
	}

	return(Result);
}

void *
HashTableGet(struct hash_table *HashTable, void *Key)
{
	void *Result = 0;

	uint32_t Hash = HashTable->Hash(Key, HashTable->Size);
	for(struct hash_node *Node = HashTable->Table[Hash]; Node != 0; Node = Node->Next)
	{
		if(HashTable->Compare(Key, Node->Key) == 0)
		{
			Result = Node->Data;
			break;
		}
	}

	return(Result);
}

uint32_t
DefaultHash(void *KeyIn, uint32_t TableSize)
{
	uint32_t Hash = 0;
	int32_t A = 31415;
	int32_t B = 27183;
	char *Key = (char *)KeyIn;

	for(; *Key != 0; ++Key)
	{
		Hash = (A*Hash + *Key) % TableSize;
		A = A*B % (TableSize - 1);
	}

	return(Hash);
}

int32_t
DefaultCompare(void *KeyLeft, void *KeyRight)
{
	char *Left = (char *)KeyLeft;
	char *Right = (char *)KeyRight;
	while(*Left == *Right)
	{
		if(*Left == 0)
		{
			return 0;
		}

		++Left, ++Right;
	}

	int32_t Result = *Left - *Right;
	return(Result);
}

int
main(void)
{
	struct hash_table *HashTable = HashTableNew(64, DefaultCompare, DefaultHash);
	int32_t Data = 5;
	HashTableInsert(HashTable, "AAPL", &Data);
	HashTableInsert(HashTable, "MSFT", &Data);
	HashTableInsert(HashTable, "ETFC", &Data);
	HashTableInsert(HashTable, "AAPLS", &Data);
	int32_t *Value = (int32_t *)HashTableGet(HashTable, "AAPL");
	printf("%u\n", *Value);
	Value = (int32_t *)HashTableRemove(HashTable, "AAPL");
	printf("%u\n", *Value);

	HashTableDelete(&HashTable);

	return 0;
}
