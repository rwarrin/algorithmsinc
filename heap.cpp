/**
 * My Heap implementation!
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

typedef struct heap* heap_t;
struct heap
{
	int Size;
	int Count;
	int *Array;
};

static heap_t
HeapNew(int SizeHint)
{
	heap_t Heap = (heap_t)malloc(sizeof(*Heap));
	Heap->Size = SizeHint;
	Heap->Count = 0;
	Heap->Array = (int *)malloc(sizeof(int) * (Heap->Size + 1));

	return Heap;
}

static void
_Swap(int *A, int *B)
{
	int Temp = *A;
	*A = *B;
	*B = Temp;
}

static void
_FixUp(heap_t Heap, int Index)
{
	while((Index > 1) &&
		  (Heap->Array[Index/2] < Heap->Array[Index]))
	{
		_Swap(&Heap->Array[Index/2], &Heap->Array[Index]);
		Index = Index / 2;
	}
}

static void
_FixDown(heap_t Heap, int Index, int Size)
{
	while(Index*2 <= Size)
	{
		int At = Index * 2;
		if((At < Size) &&
		   (Heap->Array[At] < Heap->Array[At + 1]))
		{
			At++;
		}
		if(Heap->Array[Index] > Heap->Array[At])
		{
			break;
		}

		_Swap(&Heap->Array[Index], &Heap->Array[At]);
		Index = At;
	}
}

static void
HeapInsert(int Key, heap_t Heap)
{
	if(Heap->Count + 1 >= Heap->Size)
	{
		int NewSize = Heap->Size * 2;
		int *NewArray = (int *)realloc(Heap->Array, sizeof(int) * NewSize);
		assert(NewArray != NULL);

		Heap->Size = NewSize;
		Heap->Array = NewArray;
	}

	Heap->Array[++Heap->Count] = Key;
	_FixUp(Heap, Heap->Count);
}

static int
HeapPop(heap_t Heap)
{
	assert(Heap != NULL);
	_Swap(&Heap->Array[1], &Heap->Array[Heap->Count]);
	_FixDown(Heap, 1, Heap->Count - 1);
	int Result = Heap->Array[Heap->Count--];
	return Result;
}

static void
PrintHeap(heap_t Heap)
{
	assert(Heap != NULL);

	printf("Size: %d\nCount: %d\n", Heap->Size, Heap->Count);
	for(int i = 1; i <= Heap->Count; i++)
	{
		printf("%4d ", Heap->Array[i]);
	}
	printf("\n");
}

int
main(void)
{
	heap_t Heap = HeapNew(32);
	PrintHeap(Heap);

#if 0
	int KeysToInsert[] = {5, 7, 2, 3, 6, 4, 9, 8, 1, 0, 10, 3};
	for(int i = 0; i < ArrayCount(KeysToInsert); i++)
	{
		HeapInsert(KeysToInsert[i], Heap);
		PrintHeap(Heap);
	}

	for(int i = 0; i < ArrayCount(KeysToInsert); i++)
	{
		int Value = HeapPop(Heap);
		printf("Popped: %d\n", Value);
		PrintHeap(Heap);
	}
#else
	int RandomKeys = 64;
	for(int i = 0; i < RandomKeys; i++)
	{
		HeapInsert(rand() % 1000, Heap);
		PrintHeap(Heap);
	}

	for(int i = 0; i < RandomKeys; i++)
	{
		int Value = HeapPop(Heap);
		printf("Popped: %4d\n", Value);
		PrintHeap(Heap);
	}
#endif
	
	return 0;
}
