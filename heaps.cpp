/**
 * Heaps.cpp
 *
 * All things heaps!
 *
 * Implementing a heap data structure, heapsort, and a priority queue based on
 * heaps.
 *
 * Chapter 6 of Introduction To Algorithms CLRS
 **/

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// TODO(rick): Add/Cleanup asserts
// TODO(rick): Bundle this stuff up! Library? Single Header File (yes)!

enum
{
	DEFAULT_HEAP_SIZE = 32
};

static inline void
_HeapSwap(int32_t *A, int32_t *B)
{
	int32_t Temp = *A;
	*A = *B;
	*B = Temp;
}

static inline int32_t
HeapDefaultMaxFunction(int32_t A, int32_t B)
{
	int32_t Result = (A > B ? 1 : 0);
	return(Result);
}

static inline int32_t
HeapDefaultMinFunction(int32_t A, int32_t B)
{
	int32_t Result = (A < B ? 1 : 0);
	return(Result);
}


typedef struct heap* heap_t;
struct heap
{
	int32_t Size;
	int32_t Length;
	// TODO(rick): Change this to some sort of item_t type that a user of the
	// library can specify as their own type.
	int32_t *Array;

	int32_t (*HeapFunction)(int32_t, int32_t);
	// TODO(rick): Add a key function so that a user of the library can use the
	// Heap to sort an external array by allowing them to tell the Heap what the
	// actual values to sort.
};

heap_t
HeapNew(int32_t SizeHint)
{
	int32_t Size = SizeHint;
	if(Size <= 0)
	{
		Size = DEFAULT_HEAP_SIZE;
	}

	heap_t Result = (heap_t)malloc(sizeof(*Result));
	if(Result != NULL)
	{
		Result->Size = Size;
		Result->Length = 0;
		Result->Array = (int32_t *)malloc(sizeof(*Result->Array));
		assert(Result->Array != NULL);
	}

	return(Result);
}

void
HeapDelete(heap_t *Heap)
{
	assert(Heap != NULL);
	assert(*Heap != NULL);

	free((*Heap)->Array);
	(*Heap)->Size = -1;
	(*Heap)->Length = -1;
	free(*Heap);

	*Heap = 0;
}

void
HeapSetArray(heap_t Heap, int32_t *Array, int32_t Length)
{
	free(Heap->Array);

	Heap->Length = 0;
	Heap->Size = Length + 1;
	Heap->Array = (int32_t *)malloc(sizeof(*Heap->Array)*Heap->Size);
	assert(Heap->Array != NULL);

	for(int32_t Index = 0; Index < Length; ++Index)
	{
		Heap->Array[++Heap->Length] = Array[Index];
	}
}

inline int32_t
HeapEmpty(heap_t Heap)
{
	int32_t Result = (Heap->Length < 1);
	return(Result);
}


static void
_HeapifyFixDown(heap_t Heap, int32_t Key)
{
	int32_t Left = 2*Key;
	int32_t Right = 2*Key + 1;

	int32_t Largest = Key;
	if((Left <= Heap->Length) &&
	   (Heap->HeapFunction(Heap->Array[Left], Heap->Array[Largest])))
	{
		Largest = Left;
	}

	if((Right <= Heap->Length) &&
	   (Heap->HeapFunction(Heap->Array[Right], Heap->Array[Largest])))
	{
		Largest = Right;
	}

	if(Largest != Key)
	{
		_HeapSwap(&Heap->Array[Key], &Heap->Array[Largest]);
		_HeapifyFixDown(Heap, Largest);
	}
}

static void
_HeapifyFixUp(heap_t Heap, int32_t Key)
{
	while((Key > 1) &&
		  !(Heap->HeapFunction(Heap->Array[Key/2], Heap->Array[Key])))
	{
		_HeapSwap(&Heap->Array[Key/2], &Heap->Array[Key]);
		Key = Key/2;
	}
}

void
HeapBuild(heap_t Heap)
{
	for(int32_t Index = Heap->Length / 2; Index >= 1; --Index)
	{
		_HeapifyFixDown(Heap, Index);
	}
}

void
HeapSort(heap_t Heap)
{
	HeapBuild(Heap);
	int32_t Length = Heap->Length;
	for(int32_t Index = Heap->Length; Index > 1; --Index)
	{
		_HeapSwap(&Heap->Array[1], &Heap->Array[Index]);
		--Heap->Length;
		_HeapifyFixDown(Heap, 1);
	}

	Heap->Length = Length;
}

inline int32_t
HeapTop(heap_t Heap)
{
	int32_t Result = Heap->Array[1];
	return(Result);
}

int32_t
HeapPop(heap_t Heap)
{
	assert(Heap->Length >= 1);

	int32_t Result = Heap->Array[1];
	Heap->Array[1] = Heap->Array[Heap->Length];
	--Heap->Length;
	_HeapifyFixDown(Heap, 1);

	return(Result);
}

void
HeapSetKey(heap_t Heap, int32_t Index, int32_t NewKey)
{
	assert((Index >= 1) && (Index <= Heap->Length));

	int32_t OldKey = Heap->Array[Index];
	Heap->Array[Index] = NewKey;

	if(Heap->HeapFunction(NewKey, OldKey))
	{
		_HeapifyFixUp(Heap, Index);
	}
	else
	{
		_HeapifyFixDown(Heap, Index);
	}
}

void
HeapInsert(heap_t Heap, int32_t Key)
{
	while(Heap->Length + 1 > Heap->Size)
	{
		int32_t NewSize = Heap->Size*2;
		int32_t *NewArray = (int32_t *)realloc(Heap->Array, sizeof(*NewArray)*NewSize);

		Heap->Size = NewSize;
		Heap->Array = NewArray;
	}

	Heap->Length = Heap->Length + 1;
	Heap->Array[Heap->Length] = Key;
	_HeapifyFixUp(Heap, Heap->Length);
}

inline void
PrintHeap(heap_t Heap)
{
	printf("Heap Size: %-5d Heap Length: %-5d\n", Heap->Size, Heap->Length);
	// NOTE(rick): Starting at index 1 because index 0 is not used by Heaps and
	// so it will contain data we don't care to see.
	for(int32_t Index = 1; Index <= Heap->Length; ++Index)
	{
		printf("%3d, ", Heap->Array[Index]);
	}
	printf("\n");
}

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))
int main(void)
{
	heap_t Heap = HeapNew(32);
#if 0
	Heap->HeapFunction = HeapDefaultMinFunction;
#else
	Heap->HeapFunction = HeapDefaultMaxFunction;
#endif

#if 0
	HeapInsert(Heap, 4);
	HeapInsert(Heap, 1);
	HeapInsert(Heap, 3);
	HeapInsert(Heap, 2);
	HeapInsert(Heap, 16);
	HeapInsert(Heap, 9);
	HeapInsert(Heap, 10);
	HeapInsert(Heap, 14);
	HeapInsert(Heap, 8);
	HeapInsert(Heap, 7);
	PrintHeap(Heap);
#endif


#if 1
	int32_t Values[] = {4, 1, 3, 2, 16, 9, 10, 14, 8, 7};
	HeapSetArray(Heap, Values, ArrayCount(Values));
	PrintHeap(Heap);
#endif

#if 1
	HeapBuild(Heap);
	PrintHeap(Heap);
#endif

#if 1
	HeapSetKey(Heap, 3, 21);
	PrintHeap(Heap);
#endif

#if 0
	printf("Popped: %d\n", HeapPop(Heap));
	PrintHeap(Heap);
#endif

#if 0
	HeapSort(Heap);
	PrintHeap(Heap);
#endif

#if 1
	while(!HeapEmpty(Heap))
	{
		printf("Popped: %d\n", HeapPop(Heap));
		PrintHeap(Heap);
	}
#endif

	HeapDelete(&Heap);
	return 0;
}
