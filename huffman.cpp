#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define Assert(Condition) if(!(Condition)) { *(int *)0 = 0; }
#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

enum frequency_node_type
{
    NodeType_External,
    NodeType_Internal = 1,

    NodeType_Count
};

struct frequency_table_item
{
    enum frequency_node_type NodeType;

    struct frequency_table_item *Left;
    struct frequency_table_item *Right;

    int32_t Count;
    char Character;
};

enum
{
    HEAP_DEFAULT_SIZE = 32
};

struct huffman_node
{
    char Character;
    int32_t Count;
};

struct binding
{
    int32_t Priority;
    void *Handle;
};

typedef struct heap* heap_t;
struct heap
{
    int Length;
    int Size;
    struct binding *Bindings;
};

static inline void
_HeapSwap(struct binding *A, struct binding *B)
{
    struct binding Temp = *A;
    *A = *B;
    *B = Temp;
}

static heap_t
HeapNew(int32_t SizeHint = HEAP_DEFAULT_SIZE)
{
    heap_t Result = (heap_t)malloc(sizeof(*Result) + (sizeof(*Result->Bindings)*(SizeHint + 1)));
    Result->Bindings = (struct binding *)(Result + 1);
    Result->Size = SizeHint;
    Result->Length = 0;

    return(Result);
}

static void
HeapFixUp(heap_t Heap, int32_t Index)
{
    while((Index > 1) &&
          ((Heap->Bindings[Index].Priority) > (Heap->Bindings[Index/2].Priority)))
    {
        _HeapSwap(&Heap->Bindings[Index], &Heap->Bindings[Index/2]);
        Index = Index/2;
    }
}

static void
HeapInsert(heap_t Heap, int32_t Priority, void *Handle)
{
    Assert(Heap != 0);
    
    struct binding *Binder = &Heap->Bindings[++Heap->Length];
    Binder->Priority = Priority;
    Binder->Handle = Handle;
    HeapFixUp(Heap, Heap->Length);
}

static inline int32_t
HeapEmpty(heap_t Heap)
{
    int32_t Result = (Heap->Length < 1);
    return(Result);
}

inline static int32_t
HeapPeek(heap_t Heap)
{
    Assert(Heap != 0);

    int32_t Result = -1;
    if(Heap->Length > 0)
    {
        Result = Heap->Bindings[1].Priority;
    }

    return(Result);
}

static inline void
HeapFixDown(heap_t Heap, int32_t Index)
{
    while(Index*2 <= Heap->Length)
    {
        int32_t SwapIndex = Index;
        if((Index*2 + 1 <= Heap->Length) &&
           (Heap->Bindings[Index].Priority < Heap->Bindings[Index*2 + 1].Priority))
        {
            SwapIndex = Index*2+1;
        }

        if((Index*2 <= Heap->Length) &&
           (Heap->Bindings[Index*2].Priority > Heap->Bindings[SwapIndex].Priority))
        {
            SwapIndex = Index*2;
        }

        if(SwapIndex == Index)
        {
            break;
        }

        _HeapSwap(&Heap->Bindings[Index], &Heap->Bindings[SwapIndex]);
        Index = SwapIndex;
    }
}

static void *
HeapPop(heap_t Heap)
{
    Assert(Heap != 0);
    Assert(Heap->Length > 0);

    void *Result = Heap->Bindings[1].Handle;
    _HeapSwap(&Heap->Bindings[1], &Heap->Bindings[Heap->Length]);
    --Heap->Length;
    HeapFixDown(Heap, 1);

    return(Result);
}

static inline void
PrintHuffmanTree(struct frequency_table_item *Node)
{
    if(Node == 0)
    {
        return;
    }

    PrintHuffmanTree(Node->Left);
    if(Node->NodeType == NodeType_External)
    {
        printf("%c (%d)\n", Node->Character, Node->Count);
    }
    PrintHuffmanTree(Node->Right);
}

int
main(void)
{
    struct frequency_table_item FrequencyTable[26] = {};
    for(int32_t Index = 0; Index < ArrayCount(FrequencyTable); ++Index)
    {
        FrequencyTable[Index].Character = 'A' + Index;
    }

    uint32_t Character = 0;
    while((Character = fgetc(stdin)) != EOF)
    {
        if(((Character >= 'a') && (Character <= 'z')) ||
           ((Character >= 'A') && (Character <= 'Z')))
        {
            Character = Character & ~0x20;
            int32_t Index = Character - 'A';
            ++FrequencyTable[Index].Count;
        }
    }


    heap_t Heap = HeapNew();
    for(int32_t Index = 0; Index < ArrayCount(FrequencyTable); ++Index)
    {
        if(FrequencyTable[Index].Count != 0)
        {
            HeapInsert(Heap, FrequencyTable[Index].Count, &FrequencyTable[Index]);
        }
    }

    struct frequency_table_item *HuffmanTree = 0;
    while(!HeapEmpty(Heap))
    {
        if(Heap->Length == 1)
        {
            HuffmanTree = (struct frequency_table_item *)HeapPop(Heap);
        }
        else
        {
            struct frequency_table_item *First = (struct frequency_table_item *)HeapPop(Heap);
            struct frequency_table_item *Second = (struct frequency_table_item *)HeapPop(Heap);

            struct frequency_table_item *NewNode = (struct frequency_table_item *)malloc(sizeof(*NewNode));
            NewNode->NodeType = NodeType_Internal;
            NewNode->Left = First;
            NewNode->Right = Second;
            NewNode->Count = First->Count + Second->Count;
            NewNode->Character = 0;

            HeapInsert(Heap, NewNode->Count, NewNode);
        }
    }

    PrintHuffmanTree(HuffmanTree);
    return 0;
}
