/**
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

enum
{
	DEFAULT_HEAP_SIZE = 32
};

struct binding
{
	int Key;    // NOTE(rick): Priority
	int Value;  // NOTE(rick): Satellite data
};
typedef struct heap* heap_t;
struct heap
{
	int Length;
	int Size;
	struct binding *Bindings;
};

static inline void
_HeapSwap(struct binding *A, struct binding *B)
{
	struct binding Temp = *A;
	*A = *B;
	*B = Temp;
}

static heap_t
HeapCreate(int SizeHint = DEFAULT_HEAP_SIZE)
{
	heap_t Result = (heap_t)malloc(sizeof(*Result) + (sizeof(*Result->Bindings)*(SizeHint + 1)));
	Result->Bindings = (struct binding *)(Result + 1);
	Result->Size = SizeHint;
	Result->Length = 0;

	return(Result);
}

static inline void
_HeapFixUp(heap_t Heap, int Index)
{
	while((Index > 1) &&
		  (Heap->Bindings[Index].Key < Heap->Bindings[Index/2].Key))
	{
		_HeapSwap(&Heap->Bindings[Index], &Heap->Bindings[Index/2]);
		Index = Index / 2;
	}
}

static inline void
_HeapFixDown(heap_t Heap, int Index)
{
	while(Index*2 <= Heap->Length)
	{
		int SwapIndex = Index;
		if((Index*2 + 1 <= Heap->Length) &&
		   (Heap->Bindings[Index*2 + 1].Key < Heap->Bindings[SwapIndex].Key))
		{
			SwapIndex = Index*2 + 1;
		}

		if((Index*2 <= Heap->Length) &&
		   (Heap->Bindings[Index*2].Key < Heap->Bindings[SwapIndex].Key))
		{
			SwapIndex = Index*2;
		}

		if(SwapIndex == Index)
		{
			break;
		}

		_HeapSwap(&Heap->Bindings[Index], &Heap->Bindings[SwapIndex]);
		Index = SwapIndex;
	}
}

static void
HeapInsert(heap_t Heap, int Key, int Value)
{
	++Heap->Length;
	struct binding *Binding = Heap->Bindings + Heap->Length;
	Binding->Key = Key;
	Binding->Value = Value;
	_HeapFixUp(Heap, Heap->Length);
}

static inline int
HeapEmpty(heap_t Heap)
{
	int Result = (Heap->Length < 1);
	return(Result);
}

static inline int
HeapLength(heap_t Heap)
{
	int Result = (Heap->Length - 1);
	return(Result);
}

static inline int
HeapPeekTop(heap_t Heap)
{
	int Result = Heap->Bindings[1].Value;
	return (Result);
}

static inline int
HeapPopTop(heap_t Heap)
{
	struct binding ResultBinding = Heap->Bindings[1];

	_HeapSwap(&Heap->Bindings[1], &Heap->Bindings[Heap->Length]);
	--Heap->Length;
	_HeapFixDown(Heap, 1);

	int Result = ResultBinding.Value;
	return (Result);
}

enum node_type
{
	NodeType_Internal,
	NodeType_Leaf
};

struct node
{
	int NodeType;
	int Count;
	char Value;
	struct node *Left;
	struct node *Right;
};

static inline void
PrintTabs(int Count)
{
	for(int i = 0; i < Count; ++i)
	{
		printf("\t");
	}
}

static inline void
PrintTree(struct node *Tree, int Level = 0)
{
	if(Tree == 0)
	{
		return;
	}

	if(Tree->NodeType == NodeType_Internal)
	{
		PrintTabs(++Level);
		printf("(%3d)\n", Tree->Count);
	}
	if(Tree->NodeType == NodeType_Leaf)
	{
		PrintTabs(++Level);
		printf("(%c %3d)\n", Tree->Value, Tree->Count);
	}
	PrintTree(Tree->Left, Level);
	PrintTree(Tree->Right, Level);
}

int
main(void)
{
	char *String = "yizgykrslpztoswbhycugkojgjwldmslhijxokdasgozvzrqqbkhyknbojp"
				   "twtzhqyugflwadgonomcauvosmwiukkxehvrfndkjvmaumpkmdeprdddyqr"
				   "gelvhvrwwlipkwjuijhhzvomiszajcldrkhsdsholhsptfkawyxzwzzlkqc"
				   "dseiegbqyjvjqgekzjocjqx";

	struct node TreeNodes[64] = {0};
	for(int Index = 0; Index < ArrayCount(TreeNodes); ++Index) 
	{
		TreeNodes[Index].NodeType = NodeType_Leaf;
		TreeNodes[Index].Count = 0;
		TreeNodes[Index].Value = 'a' + Index;
		TreeNodes[Index].Left = 0;
		TreeNodes[Index].Right = 0;
	}

	for(char *At = String; *At != 0; ++At)
	{
		++TreeNodes[*At - 'a'].Count;
	}

	heap_t Heap = HeapCreate();
	for(int i = 0; i < 26; ++i)
	{
		if(TreeNodes[i].Count > 0)
		{
			HeapInsert(Heap, TreeNodes[i].Count, i);
		}
	}

	int InsertAt = 26;
	while(!HeapEmpty(Heap))
	{
		struct node *Node = &TreeNodes[InsertAt];
		Node->NodeType = NodeType_Internal;
		Node->Count = 0;

		if(!HeapEmpty(Heap))
		{
			int Index = HeapPopTop(Heap);
			Node->Left = &TreeNodes[Index];
			Node->Count += Node->Left->Count;
		}
		if(!HeapEmpty(Heap))
		{
			int Index = HeapPopTop(Heap);
			Node->Right = &TreeNodes[Index];
			Node->Count += Node->Right->Count;
		}

		if(!HeapEmpty(Heap))
		{
			HeapInsert(Heap, Node->Count, InsertAt++);
		}
	}

	struct node *TreeRoot = &TreeNodes[InsertAt];
	PrintTree(TreeRoot);

	return 0;
}
