/**
 * Index Sorting
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

static void
InsertionSort(int *Data, int *SortedArray, int Length, int (*Comparer)(void*, void*))
{
	assert(Data != NULL);
	assert(SortedArray != NULL);
	assert(Length > 0);

	for(int i = 0; i < Length; i++)
	{
		int Index = SortedArray[i];
		int Val = Data[Index];
		int j = 0;
		for(j = i; (j > 0) && Comparer((void *)&Data[SortedArray[j-1]], (void *)&Val); j--)
		{
			SortedArray[j] = SortedArray[j-1];
		}
		SortedArray[j] = Index;
	}
}

int Ascending(void *A, void *B)
{
	return ((*(int *)A) > (*(int *)B));
}

int Descending(void *A, void *B)
{
	return ((*(int *)A) < (*(int *)B));
}

int
main(void)
{
	int Data[] = {1, 3, 5, 2, 4, 6, 7, 9, 8, 0};
	int *SortedAscArray = (int *)malloc(sizeof(int) * ArrayCount(Data));
	int *SortedDescArray = (int *)malloc(sizeof(int) * ArrayCount(Data));
	for(int i = 0; i < ArrayCount(Data); i++)
	{
		SortedAscArray[i] = i;
		SortedDescArray[i] = i;
	}

	InsertionSort(Data, SortedAscArray, ArrayCount(Data), Ascending);
	InsertionSort(Data, SortedDescArray, ArrayCount(Data), Descending);

	printf("Index Desc I V Asc I V Val\n");
	for(int i = 0; i < ArrayCount(Data); i++)
	{
		printf("%-5d      %d %d     %d %d %3d\n", i, SortedDescArray[i], Data[SortedDescArray[i]],
			   SortedAscArray[i], Data[SortedAscArray[i]], Data[i]);
	}

	return 0;
}
