/**
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof(Array) / sizeof(Array[0]))
#define MAX(A, B) (A > B ? A : B)

struct item
{
	int Weight;
	int Value;
};

static inline int**
Make2DArray(int Rows, int Columns, int Init = 0)
{
	int **Result = (int **)malloc(sizeof(*Result)*Rows);
	for(int Row = 0; Row < Rows; ++Row)
	{
		Result[Row] = (int *)malloc(sizeof(Result[Row])*Columns);
	}

	for(int Row = 0; Row < Rows; ++Row)
	{
		for(int Col = 0; Col < Columns; ++Col)
		{
			Result[Row][Col] = Init;
		}
	}

	return(Result);
}

static inline int
Knapsack_TopDownDP(struct item *Items, int ItemCount, int Capacity, int *MemoizedArray)
{
	if(Capacity <= 0)
	{
		return 0;
	}

	if(ItemCount < 0)
	{
		return 0;
	}

	if(MemoizedArray[Capacity] != -1)
	{
		return(MemoizedArray[Capacity]);
	}

	if(Items[ItemCount].Weight > Capacity)
	{
		return Knapsack_TopDownDP(Items, ItemCount - 1, Capacity, MemoizedArray);
	}

	int BestValue = MAX(Knapsack_TopDownDP(Items, ItemCount - 1, Capacity - Items[ItemCount].Weight, MemoizedArray) + Items[ItemCount].Value,
						Knapsack_TopDownDP(Items, ItemCount - 1, Capacity, MemoizedArray));

	MemoizedArray[Capacity] = BestValue;
	return(BestValue);
}

static inline int
Knapsack_BottomUpDP(struct item *Items, int ItemCount, int Capacity)
{
	int **Table = Make2DArray(ItemCount + 1, Capacity + 1, 0);
	for(int Row = 1; Row <= ItemCount; ++Row)
	{
		for(int Col = 1; Col <= Capacity; ++Col)
		{
			int Value = Table[Row - 1][Col];
			if(Col - Items[Row - 1].Weight >= 0)
			{
				Value = MAX(Table[Row - 1][Col],
							Table[Row - 1][Col - Items[Row - 1].Weight] + Items[Row - 1].Value);
			}

			Table[Row][Col] = Value;
		}
	}

	return Table[ItemCount][Capacity];
}

int
main(void)
{
#if 0
	enum { Capacity = 8 };
	struct item Items[] = {{2, 1},
						   {3, 2},
						   {4, 5},
						   {5, 6}};
#elif 0
	enum { Capacity = 50 };
	struct item Items[] = {{10, 60},
						   {20, 100},
						   {30, 120}};
#elif 1
	enum { Capacity = 10 };
	struct item Items[] = {{5, 10},
						   {4, 40},
						   {6, 30},
						   {3, 50}};
#endif

	int MemoizedArray[Capacity + 1] = {0};
	for(int i = 0; i < ArrayCount(MemoizedArray); ++i)
	{
		MemoizedArray[i] = -1;
	}

	int MaxValueTD = Knapsack_TopDownDP(Items, ArrayCount(Items) - 1, Capacity, MemoizedArray);
	printf("Knapsack_TopDownDP() = %d\n", MaxValueTD);

	int MaxValueBU = Knapsack_BottomUpDP(Items, ArrayCount(Items), Capacity);
	printf("Knapsack_BottomUpDP() = %d\n", MaxValueBU);

	return 0;
}
