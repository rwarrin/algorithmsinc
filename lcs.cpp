/**
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

#define MAX(A, B) (A >= B ? A : B)

enum direction
{
	Direction_Unknown,
	Direction_Up,
	Direction_Left,
	Direction_Diagonal
};

inline void
PrintArray(int **Array, int Rows, int Cols)
{
	for(int Row = 0; Row < Rows; ++Row)
	{
		for(int Col = 0; Col < Cols; ++Col)
		{
			printf("%2d ", Array[Row][Col]);
		}
		printf("\n");
	}
}

inline int **
Make2DArray(int Rows, int Cols)
{
	int **Result = (int **)malloc(sizeof(*Result)*Rows);
	for(int i = 0; i < Rows; ++i)
	{
		Result[i] = (int *)malloc(sizeof(Result[i])*Cols);
	}

	return(Result);
}

static inline void
PrintReconstruction(int **ReconstructionArray, char *String, int Row, int Col)
{
	if((Row == 0) ||
	   (Col == 0))
	{
		return;
	}

	int Direction = ReconstructionArray[Row][Col];
	if(Direction == Direction_Diagonal)
	{
		PrintReconstruction(ReconstructionArray, String, Row - 1, Col - 1);
		printf("%2c ", String[Col]);
	}
	else if(Direction == Direction_Left)
	{
		PrintReconstruction(ReconstructionArray, String, Row, Col - 1);
	}
	else
	{
		PrintReconstruction(ReconstructionArray, String, Row - 1, Col);
	}
}

static inline int
LCS_BottomUpDP(char *Left, char *Right)
{
	int LeftLength = strlen(Left);
	int RightLength = strlen(Right);

	int **LengthArray = Make2DArray(RightLength + 1, LeftLength + 1);
	for(int Row = 0; Row <= RightLength; ++Row)
	{
		LengthArray[Row][0] = 0;
	}
	for(int Col = 0; Col <= LeftLength; ++Col)
	{
		LengthArray[0][Col] = 0;
	}

	int **ReconstructionArray = Make2DArray(RightLength + 1, LeftLength + 1);
	for(int Row = 0; Row <= RightLength; ++Row)
	{
		for(int Col = 0; Col <= RightLength; ++Col)
		{
			ReconstructionArray[Row][Col] = Direction_Unknown;
		}
	}

	int Result = 0;
	for(int Row = 1; Row <= RightLength; ++Row)
	{
		for(int Col = 1; Col <= LeftLength; ++Col)
		{
			if(Left[Col] == Right[Row])
			{
				LengthArray[Row][Col] = LengthArray[Row - 1][Col - 1] + 1;
				ReconstructionArray[Row][Col] = Direction_Diagonal;
			}
			else
			{
				int LeftTest = LengthArray[Row][Col - 1];
				int RightTest = LengthArray[Row - 1][Col];
				int Length = MAX(LeftTest, RightTest);
				LengthArray[Row][Col] = Length;
				ReconstructionArray[Row][Col] = (Length == LeftTest ? Direction_Left : Direction_Up);
			}

			Result = MAX(Result, LengthArray[Row][Col]);
		}
	}

	PrintReconstruction(ReconstructionArray, Left, RightLength, LeftLength);
	return(Result);
}

static inline int
LCS_TopDownDP(char *Left, int LeftLength, char *Right, int RightLength,
			  int **MemoizeArray, int **ReconstructionArray)
{
	// TODO(rick): This is broken
	if((LeftLength == 0) ||
	   (RightLength == 0))
	{
		return 0;
	}

	if(MemoizeArray[RightLength][LeftLength] != -1)
	{
		return MemoizeArray[RightLength][LeftLength];
	}

	int Result = 0;
	int Length = 0;
	if(Left[LeftLength] == Right[RightLength])
	{
		Length = LCS_TopDownDP(Left, LeftLength - 1, Right, RightLength - 1, MemoizeArray, ReconstructionArray) + 1;
		ReconstructionArray[RightLength][LeftLength] = Direction_Diagonal;
		printf("%2c ", Left[LeftLength]);
	}
	else
	{
		int TryUp = LCS_TopDownDP(Left, LeftLength, Right, RightLength - 1, MemoizeArray, ReconstructionArray);
		int TryLeft = LCS_TopDownDP(Left, LeftLength - 1, Right, RightLength, MemoizeArray, ReconstructionArray);
		int Length = MAX(TryUp, TryLeft);
		ReconstructionArray[RightLength][LeftLength] = (TryUp == Length ? Direction_Up : Direction_Left);
	}

	Result = MAX(Result, Length);
	MemoizeArray[RightLength][LeftLength] = Result;
	return(Result);
}

int
main(void)
{
	printf("Hello world\n");

	char *Left = "xmjauz";
	char *Right = "mzjawxu";
	int LCSLength = LCS_BottomUpDP(Left, Right);
	printf("LCS_BottomUpDP('%s', '%s') = %d\n", Left, Right, LCSLength);

	int LeftLength = strlen(Left);
	int RightLength = strlen(Right);
	int **MemoizeArray = Make2DArray(RightLength + 1, LeftLength + 1);
	int **ReconstructionArray = Make2DArray(RightLength + 1, LeftLength + 1);
	for(int Row = 0; Row <= RightLength; ++Row)
	{
		for(int Col = 0; Col <= LeftLength; ++Col)
		{
			MemoizeArray[Row][Col] = -1;
			ReconstructionArray[Row][Col] = Direction_Unknown;
		}
	}

	int LCSTopDownLength = LCS_TopDownDP(Left, LeftLength, Right, RightLength, MemoizeArray, ReconstructionArray);
	printf("LCS_TopDownDP('%s', '%s')  %d\n", Left, Right, LCSTopDownLength);
	PrintReconstruction(ReconstructionArray, Left, RightLength, LeftLength);

	return 0;
}
