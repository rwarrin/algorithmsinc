/**
 *
 **/

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct node* list_t;
struct node
{
	int32_t Key;
	struct node *Next;
	struct node *Prev;
};

static list_t
ListNew()
{
	list_t Result = (list_t)malloc(sizeof(*Result));
	Result->Key = 0;
	Result->Next = Result;
	Result->Prev = Result;

	return(Result);
}

static void
ListInsert(list_t List, int32_t Value)
{
	assert(List != 0);

	list_t NewNode = ListNew();
	NewNode->Key = Value;

	NewNode->Next = List->Next;
	List->Next->Prev = NewNode;
	List->Next = NewNode;
	NewNode->Prev = List;
}

static list_t
ListSearch(list_t List, int32_t Key)
{
	list_t Result = List->Next;
	while((Result != List) &&
		  (Result->Key != Key))
	{
		Result = Result->Next;
	}

	return(Result);
}

static void
ListRemove(list_t List, list_t Element)
{
	if(Element->Prev != List)
	{
		Element->Prev->Next = Element->Next;
	}
	else
	{
		List->Next = Element->Next;
	}

	if(Element->Next != List)
	{
		Element->Next->Prev = Element->Prev;
	}
}

int main(void)
{
	list_t List = ListNew();
	ListInsert(List, 1);
	ListInsert(List, 2);
	ListInsert(List, 3);

	list_t Found = ListSearch(List, 2);
	printf("%d\n", Found->Key);
	ListRemove(List, Found);

	return 0;
}
