/**
 *
 **/

#include <stdio.h>
#include <stdlib.h>

struct node
{
	int Value;
	struct node *Next;
};

struct node *
NewNode(int Value, struct node *Next)
{
	struct node *Node = (struct node *)malloc(sizeof(*Node));
	Node->Value = Value;
	Node->Next = Next;

	return(Node);
}

void
NodeAdd(struct node **List, int Value)
{
	*List = NewNode(Value, *List);
};

void
NodeDelete(struct node **List, int Value)
{
	struct node **Walker = List;

	while((*Walker != NULL) &&
		  ((*Walker)->Value != Value))
	{
		Walker = &((*Walker)->Next);
	}

	// NOTE(rick): Leaking memory :)
	printf("\t(%p -> %p) %d\n", Walker, *Walker, (*Walker)->Value);
	*Walker = (*Walker)->Next;
}

int main(void)
{
	struct node *List = NULL;
	NodeAdd(&List, 0);
	NodeAdd(&List, 1);
	NodeAdd(&List, 2);
	NodeAdd(&List, 3);
	NodeAdd(&List, 4);

	for(struct node *T = List; T != NULL; T = T->Next)
	{
		printf("(%p) %d\n", T, T->Value);
	}

	NodeDelete(&List, 2);
	for(struct node *T = List; T != NULL; T = T->Next)
	{
		printf("(%p) %d\n", T, T->Value);
	}

	return 0;
}
