/**
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <assert.h>
#include <string.h>

#define MAX(A, B) (A > B ? A : B)

enum direction
{
	Direction_Unknown,
	Direction_Diagonal,
	Direction_Up,
	Direction_Left
};

static void
SetArray(int Value, int *Array, int Col, int Row, int Stride)
{
	*(Array + (Row*Stride) + Row) = Value;
}

static int
GetArray(int *Array, int Col, int Row, int Stride)
{
	int Value = *(Array + (Row*Stride) + Row);
	return(Value);
}

static int
LCS_BottomUpDP(char *Left, char *Right, int **LengthsOut, int **ReconstructOut, int *StrideOut)
{
	int LeftLength = strlen(Left);
	int RightLength = strlen(Right);

	int ArraySize = (LeftLength + 1)*(RightLength + 1);
	int *Lengths = (int *)malloc(sizeof(*Lengths)*ArraySize);
	int Stride = LeftLength;
	*StrideOut = Stride;
	for(int i = 0; i < LeftLength*RightLength; ++i)
	{
		Lengths[i] = -9;
	}
	for(int i = 0; i < LeftLength; i++)
	{
		SetArray(0, Lengths, i, 0, Stride);
	}
	for(int i = 0; i < RightLength; i++)
	{
		SetArray(0, Lengths, 0, i, Stride);
	}
	*LengthsOut = Lengths;

	int *Reconstruct = (int *)malloc(sizeof(*Lengths)*ArraySize);
	for(int i = 0; i < LeftLength*RightLength; ++i)
	{
		Reconstruct[i] = Direction_Unknown;
	}
	*ReconstructOut = Reconstruct;

	int LCSLength = INT_MIN;
	for(int i = 1; i < LeftLength; i++)
	{
		for(int j = 1; j < RightLength; j++)
		{
			if(Left[i] == Right[j])
			{
				int Length = GetArray(Lengths, i - 1, j - 1, Stride) + 1;
				SetArray(Length, Lengths, i, j, Stride);
				SetArray(Direction_Diagonal, Reconstruct, i, j, Stride);
			}
#if 1
			else if(GetArray(Lengths, i - 1, j, Stride) >= GetArray(Lengths, i, j - 1, Stride))
			{
				int Length = GetArray(Lengths, i - 1, j, Stride);
				SetArray(Length, Lengths, i, j, Stride);
				SetArray(Direction_Left, Reconstruct, i, j, Stride);
			}
			else
			{
				int Length = GetArray(Lengths, i, j - 1, Stride);
				SetArray(Length, Lengths, i, j, Stride);
				SetArray(Direction_Up, Reconstruct, i, j, Stride);
			}
#else
			else
			{
				int TryLeft = GetArray(Lengths, i, j - 1, Stride);
				int TryUp = GetArray(Lengths, i - 1, j, Stride);
				int Length = MAX(TryLeft, TryUp);
				SetArray(Length, Lengths, i, j, Stride);
				SetArray((Length == TryLeft ? Direction_Left : Direction_Up), Reconstruct, i, j, Stride);
			}
#endif

			LCSLength = MAX(LCSLength, GetArray(Lengths, i, j, Stride));
		}
	}

	return(LCSLength);
}

inline void
PrintLCS(int *ReconArray, char *String, int LeftLength, int RightLength, int Stride)
{
	if((LeftLength == 0) ||
	   (RightLength == 0))
	{
		return;
	}
#if 1
	int Direction = GetArray(ReconArray, RightLength, LeftLength, Stride);
	if(Direction == Direction_Diagonal)
	{
		PrintLCS(ReconArray, String, LeftLength - 1, RightLength - 1, Stride);
		printf("%c ", String[LeftLength]);
	}
	else if(Direction == Direction_Up)
	{
		PrintLCS(ReconArray, String, LeftLength, RightLength - 1, Stride);
	}
	else  // if(Direction == Direction_Left)
	{
		PrintLCS(ReconArray, String, LeftLength - 1, RightLength, Stride);
	}
#else
	PrintLCS(ReconArray, String, LeftLength - 1, RightLength - 1, Stride);
	int Direction = GetArray(ReconArray, RightLength, LeftLength, Stride);
	if(Direction == Direction_Diagonal)
	{
		printf("%c ", String[LeftLength]);
	}
#endif
}

inline void
PrintArray(int *Array, int Length, int Stride)
{
	for(int i = 0; i < Length; ++i)
	{
		if(i && (i % Stride == 0))
		{
			printf("\n");
		}
		printf("%2d ", Array[i]);
	}
}

int
main(void)
{
	printf("Hello world\n");

	char *Left = " abcbdab";
	char *Right = " bdcaba";

	int *Lengths = 0;
	int *Reconstruct = 0;
	int Stride = 0;
	int Length = LCS_BottomUpDP(Left, Right, &Lengths, &Reconstruct, &Stride);

	assert(Lengths != 0);
	assert(Reconstruct != 0);
	assert(Stride != 0);

	PrintArray(Lengths, strlen(Left)*strlen(Right), Stride);
	printf("\n\n");
	PrintArray(Reconstruct, strlen(Left)*strlen(Right), Stride);
	printf("\n\n");
	printf("LCS_BottomUpDP = %d\n", Length);
	PrintLCS(Reconstruct, Left, strlen(Left), strlen(Right), Stride);
	//PrintLCS(Reconstruct, Left, 6, 6, Stride);

	return 0;
}
