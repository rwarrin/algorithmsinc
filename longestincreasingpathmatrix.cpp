/**
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

#define MAX(A, B) (A > B ? A : B)

inline int **
Make2DArray(int Rows, int Cols, int Init = 0)
{
	int **Result = (int **)malloc(sizeof(*Result)*Rows);
	for(int i = 0; i < Rows; i++)
	{
		Result[i] = (int *)malloc(sizeof(Result[i])*Cols);
	}

	for(int Y = 0; Y < Rows; ++Y)
	{
		for(int X = 0; X < Cols; ++X)
		{
			Result[Y][X] = Init;
		}
	}

	return(Result);
}

static inline void
PrintArray(int **Array, int Rows, int Cols)
{
	for(int Row = 0; Row < Rows; ++Row)
	{
		for(int Col = 0; Col < Cols; ++Col)
		{
			printf("%2d ", Array[Row][Col]);
		}
		printf("\n");
	}
	printf("\n");
}

static inline int
LongestIncreasingPath_TopDownDP(int **Map, int Rows, int Cols,
								int X, int Y, int **MemoizedMap)
{
	if((X < 0) || (X >= Cols) ||
	   (Y < 0) || (Y >= Rows))
	{
		return 0;
	}

	if(MemoizedMap[Y][X] != -1)
	{
		return(MemoizedMap[Y][X]);
	}
	
	int Longest = 1;
	if((X - 1 >= 0) && (Map[Y][X - 1] + 1 == Map[Y][X]))
	{
		Longest = MAX(Longest, 1 + LongestIncreasingPath_TopDownDP(Map, Rows, Cols, X - 1, Y, MemoizedMap));
	}
	if((X + 1 < Cols) && (Map[Y][X + 1] + 1 == Map[Y][X]))
	{
		Longest = MAX(Longest, 1 + LongestIncreasingPath_TopDownDP(Map, Rows, Cols, X + 1, Y, MemoizedMap));
	}
	if((Y - 1 >= 0) && (Map[Y - 1][X] + 1 == Map[Y][X]))
	{
		Longest = MAX(Longest, 1 + LongestIncreasingPath_TopDownDP(Map, Rows, Cols, X, Y - 1, MemoizedMap));
	}
	if((Y + 1 < Rows) && (Map[Y + 1][X] + 1 == Map[Y][X]))
	{
		Longest = MAX(Longest, 1 + LongestIncreasingPath_TopDownDP(Map, Rows, Cols, X, Y + 1, MemoizedMap));
	}

	MemoizedMap[Y][X] = Longest;
	return(Longest);
}

static inline int
LIPTD_Helper(int **Map, int Rows, int Cols, int **MemoizedMap)
{
	int Longest = INT_MIN;
	for(int Row = 0; Row < Rows; ++Row)
	{
		for(int Col = 0; Col < Cols; ++Col)
		{
			Longest = MAX(Longest, LongestIncreasingPath_TopDownDP(Map, Rows, Cols, Col, Row, MemoizedMap));
		}
	}

	return(Longest);
}

int
main(void)
{
	int Rows = 3;
	int Cols = 3;
	int **Map = Make2DArray(Rows, Cols, 0);
	/**
	 * 1 2 9
	 * 5 3 8
	 * 4 6 7
	 **/
	Map[0][0] = 1;
	Map[0][1] = 2;
	Map[0][2] = 9;
	Map[1][0] = 5;
	Map[1][1] = 3;
	Map[1][2] = 8;
	Map[2][0] = 4;
	Map[2][1] = 6;
	Map[2][2] = 7;

	int **MemoizedMap = Make2DArray(Rows, Cols, -1);

	int LongestPathTD = LIPTD_Helper(Map, Rows, Cols, MemoizedMap);
	printf("LongestIncreasingPath_TopDownDP() = %d\n", LongestPathTD);

	return 0;
}
