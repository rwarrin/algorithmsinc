/**
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))
#define MAX(A, B) (A >= B ? A : B)

int
LIS_TopDownDP(int *Array, int Length, int *MemoizedArray)
{
	if(Length == 0)
	{
		return 1;
	}

	if(MemoizedArray[Length - 1] != INT_MIN)
	{
		return MemoizedArray[Length];
	}

	int Result = INT_MIN;
	for(int i = 0; i < Length; ++i)
	{
		int Addend = (Array[Length] > Array[i] ? 1 : 0);
		Result = MAX(Result, LIS_TopDownDP(Array, i, MemoizedArray) + Addend);
	}

	MemoizedArray[Length] = Result;
	return(Result);
}

int
LIS_BottomUpDP(int *Array, int Length)
{
	int *CountArray = (int *)malloc(sizeof(*CountArray) * Length);
	int Result = INT_MIN;

	CountArray[0] = 1;
	for(int i = 1; i < Length; ++i)
	{
		CountArray[i] = 1;
		for(int j = 0; j < i; ++j)
		{
			if(Array[i] > Array[j])
			{
				if(CountArray[j] + 1 > CountArray[i])
				{
					CountArray[i] = CountArray[j] + 1;
				}
			}

			Result = MAX(Result, CountArray[i]);
		}
	}

	return(Result);
}

int
main(void)
{
	int Array[] = {10, 22, 9, 33, 21, 50, 41, 60, 80};

	int MemoizedArray[ArrayCount(Array)];
	for(int i = 0; i < ArrayCount(MemoizedArray); ++i)
	{
		MemoizedArray[i] = INT_MIN;
	}
	int TopDownLength = LIS_TopDownDP(Array, ArrayCount(Array), MemoizedArray);
	printf("LIS_TopDownDP = %d\n", TopDownLength);

	int BottomUpLength = LIS_BottomUpDP(Array, ArrayCount(Array));
	printf("LIS_BottomUpDP = %d\n", BottomUpLength);

	return 0;
}
