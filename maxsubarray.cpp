#include <stdio.h>
#include <limits.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

#define MAX(A, B) (A > B ? A : B)

int _MaxCrossingSum(int *Array, int Left, int Middle, int Right)
{
	int LeftSum = Array[Middle];
	int Sum = 0;
	for(int i = Middle; i >= Left; --i)
	{
		Sum = Sum += Array[i];
		if(Sum > LeftSum)
		{
			LeftSum = Sum;
		}
	}

	int RightSum = Array[Middle + 1];
	Sum = 0;
	for(int i = Middle + 1; i <= Right; ++i)
	{
		Sum = Sum += Array[i];
		if(Sum > RightSum)
		{
			RightSum = Sum;
		}
	}

	return(LeftSum + RightSum);
}

int FindMaxSubArray(int *Array, int Left, int Right)
{
	if(Left >= Right)
	{
		return Array[Left];
	}

	int Mid = (Left + Right) / 2;
	return MAX(MAX(FindMaxSubArray(Array, Left, Mid),
				   FindMaxSubArray(Array, Mid + 1, Right)),
			   _MaxCrossingSum(Array, Left, Mid, Right));
}

int main(void)
{
	int Numbers[] = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
	int Max = FindMaxSubArray(Numbers, 0, ArrayCount(Numbers) - 1);
	printf("Max sub array sum = %d\n", Max);

	return 0;
}
