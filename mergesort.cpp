/**
 * Mergesort
 *
 * Divide and conquer
 * - Base case when Left and Right have met or swapped
 * - Calculate Middle = (Left + Right) / 2
 * - Recursively call Mergesort on sub-arrays Array[Left...Middle] and
 *   Array[Middle + 1...Right]
 * - Combine Left and Right sorted sub-arrays into one sorted Array[Left..Right]
 *   - Set LeftIndex to Left
 *   - Set RightIndex to Middle + 1
 *   - Create temporary array of length ((Right - Left) + 1)
 *   - While there are values to copy into the sorted array...
 *     - If LeftIndex > Middle insert the remaining Right sub-array values
 *     - If RightIndex > Right insert the remaining Left sub-array values
 *     - If LeftIndex <= RightIndex insert Array[LeftIndex] into sorted array
 *       and increment LeftIndex
 *     - If RightIndex < LeftIndex insert Array[RightIndex] into sorted array
 *       and increment RightIndex
 *   - Copy sorted array back into input array
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

static inline void
MergeSort(int *Array, int Left, int Right)
{
	if(Left >= Right)
	{
		return;
	}

	int Middle = (Left + Right) / 2;
	MergeSort(Array, Left, Middle);
	MergeSort(Array, Middle + 1, Right);

	int Length = (Right - Left) + 1;
	int *Temp = (int *)malloc(sizeof(int)*Length);
	int LeftPtr = Left;
	int RightPtr = Middle + 1;
	for(int Index = 0; Index < Length; ++Index)
	{
		if(LeftPtr > Middle)
		{
			Temp[Index] = Array[RightPtr++];
			continue;
		}

		if(RightPtr > Right)
		{
			Temp[Index] = Array[LeftPtr++];
			continue;
		}

		if(Array[LeftPtr] <= Array[RightPtr])
		{
			Temp[Index] = Array[LeftPtr++];
		}
		else
		{
			Temp[Index] = Array[RightPtr++];
		}
	}

	for(int Index = 0; Index < Length; ++Index)
	{
		Array[Left + Index] = Temp[Index];
	}

	free(Temp);
}

int main(void)
{
	int Array[] = {9, 3, 4, 7, 1, 8, 5, 6, 2, 0, 10, 13, 2};
	MergeSort(Array, 0, ArrayCount(Array) - 1);
	for(int i = 0; i < ArrayCount(Array) - 1; ++i)
	{
		printf("%d, ", Array[i]);
		assert(Array[i] <= Array[i + 1]);
	}
	printf("%d\n", Array[ArrayCount(Array) - 1]);

	return 0;
}
