/**
 * Write a program that takes a string as argument, adn that prints out a table
 * giving, for each character that occurs in the string, the character and its
 * frequency of occurrence.
 **/

#include <stdio.h>

int
main(int ArgCount, char **Args)
{
	if(ArgCount != 2)
	{
		printf("Usage: %s [string]\n", Args[0]);
		return 1;
	}

	char *String = Args[1];
	int Table[26] = {0};
	int Length = 0;

	for(; *String != 0; ++String, ++Length)
	{
		*String |= 0x20;
		int Index = *String - 'a';
		printf("Index = %d\n", Index);
		if((Index >= 0) && (Index <= 25))
		{
			++Table[Index];
		}
	}

	for(int i = 0; i < 26; ++i)
	{
		if(Table[i] != 0)
		{
			printf("%c: %02.2f%%\n", ('A' + i), ((float)Table[i] / (float)Length));
		}
	}

	return 0;
}


