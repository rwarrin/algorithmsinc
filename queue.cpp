/**
 *
 **/

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct queue* queue_t;
struct queue
{
	int32_t Size;
	int32_t Head;
	int32_t Tail;
	int32_t *Array;
};

static queue_t
QueueNew(int32_t Size)
{
	queue_t Result = (queue_t)malloc(sizeof(*Result));
	Result->Size = Size;
	Result->Array = (int32_t *)malloc(sizeof(*Result->Array)*Size);
	Result->Head = 0;
	Result->Tail = 0;

	return(Result);
}

static int32_t
Enqueue(queue_t Queue, int32_t Value)
{
	Queue->Array[Queue->Tail] = Value;
	Queue->Tail = (Queue->Tail + 1) % Queue->Size;
	return(Value);
}

static int32_t
Dequeue(queue_t Queue)
{
	int32_t Result = Queue->Array[Queue->Head];
	Queue->Head = (Queue->Head + 1) % Queue->Size;
	return(Result);
}

static void
QueueDelete(queue_t *Queue)
{
	if((Queue != 0) && (*Queue != 0))
	{
		free((*Queue)->Array);
		free(*Queue);
	}

	*Queue = 0;
}

static int32_t
QueueEmpty(queue_t Queue)
{
	int32_t Result = (Queue->Head == Queue->Tail);
	return(Result);
}

int main(void)
{
	queue_t Queue = QueueNew(15);
	for(int i = 0; i < 14; ++i)
	{
		Enqueue(Queue, i);
	}

	while(!QueueEmpty(Queue))
	{
		printf("%d, ", Dequeue(Queue));
	}

	return 0;
}
