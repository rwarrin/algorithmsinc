/**
 * Quicksort algorithm from Introduction to Algorithms CLRS
 *
 * Uses randomized partitioning.
 *
 * Divide and Conquer
 * - Randomly choose an array index and swap it with the Rightmost array index
 * - Copy Array[Right] into a variable for comparisons
 * - Start the PartitionIndex at (Left - 1) (since swaps pre-increment)
 * - Loop from Left to Right - 1
 *   - If Array[Index] <= PartitionValue increment PartitionIndex and swap with
 *   Index
 * - Increment PartitionIndex by 1 and swap with Array[Left] to play the
 *   partition value into its final sorted position.
 * - Quicksort the left sub-array [Left...ParitionIndex - 1]
 * - Quicksort the right sub-array [ParitionIndex + 1...Right]
 **/

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>
#include <time.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

static inline void
Swap(int32_t *A, int32_t *B)
{
	int32_t Temp = *A;
	*A = *B;
	*B = Temp;
}

static inline void
QuickSort(int32_t *Array, int32_t Left, int32_t Right)
{
	if(Left >= Right)
	{
		return;
	}

	int32_t RandomIndex = Left + (rand() % (Right - Left));
	Swap(&Array[RandomIndex], &Array[Right]);

	int32_t PartitionValue = Array[Right];
	int32_t PartitionIndex = Left - 1;
	for(int Index = Left; Index < Right; ++Index)
	{
		if(Array[Index] <= PartitionValue)
		{
			Swap(&Array[++PartitionIndex], &Array[Index]);
		}
	}
	Swap(&Array[++PartitionIndex], &Array[Right]);

	QuickSort(Array, Left, PartitionIndex - 1);
	QuickSort(Array, PartitionIndex + 1, Right);
}

int main(void)
{
	srand(time(0));

	int32_t Array[] = {1, 3, 7, 5, 2, 9, 4, 8, 6, 0};
	QuickSort(Array, 0, ArrayCount(Array) - 1);

	for(int32_t Index = 0; Index < ArrayCount(Array) - 1; ++Index)
	{
		assert(Array[Index] <= Array[Index + 1]);
		printf("%d, ", Array[Index]);
	}
	printf("%d\n", Array[ArrayCount(Array) - 1]);

	return 0;
}
