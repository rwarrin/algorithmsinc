/**
 * Randomized-Select K'th Index
 *
 * Select the k-th item from a list using randomized partitioning.
 *
 * - Base case, if Left and Right are equal return the value at Left because
 *   this is the target index.
 * - Use randomized partitioning algorithm that quicksort uses to place the
 *   partition value at the correct sorted index in the array.
 * - Calculate the size of the left sub-array including the PartitionIndex.
 * - If the TargetIndex is equal to the length of the left sub-array then this
 *   is the index to find, so return the value at the PartitionIndex.
 * - Else if the TargetIndex is less than the length of the left sub-array then
 *   the recursively call SelectKthIndex on the left sub-array, excluding the
 *   placed partition.
 * - Else the TargetIndex is in the right sub-array so recursively all
 *   SelectKthIndex on the right sub-array excluding the placed partition and
 *   adjusting the TargetIndex to be mapped to the new range of A[Left...Right].
 **/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

inline void
Swap(int32_t *A, int32_t *B)
{
	int32_t Temp = *A;
	*A = *B;
	*B = Temp;
}

static int32_t
SelectKthIndex(int32_t *Array, int32_t Left, int32_t Right, int32_t TargetIndex)
{
	if(Left == Right)
	{
		return Array[Left];
	}

	int32_t RandomPartitionIndex = Left + (rand() % (Right - Left));
	Swap(&Array[RandomPartitionIndex], &Array[Right]);

	int32_t PartitionValue = Array[Right];
	int32_t PartitionIndex = Left - 1;
	for(int32_t Index = Left; Index < Right; ++Index)
	{
		if(Array[Index] <= PartitionValue)
		{
			Swap(&Array[++PartitionIndex], &Array[Index]);
		}
	}
	Swap(&Array[++PartitionIndex], &Array[Right]);

	int32_t LeftSubArraySize = PartitionIndex - Left + 1;
	if(LeftSubArraySize == TargetIndex)
	{
		return Array[PartitionIndex];
	}
	else if(TargetIndex < LeftSubArraySize)
	{
		return SelectKthIndex(Array, Left, PartitionIndex - 1, TargetIndex);
	}
	else
	{
		return SelectKthIndex(Array, PartitionIndex + 1, Right, TargetIndex - LeftSubArraySize);
	}
}

int main(void)
{
	int32_t Array[] = {1, 2, 9, 3, 7, 4, 5, 8, 6, 0};
	for(int Index = 0; Index < ArrayCount(Array); ++Index)
	{
		int32_t Selected = SelectKthIndex(Array, 0, ArrayCount(Array) - 1, Index + 1);
		printf("Array[%d] = %d\n", Index, Selected);
		assert(Selected == Index);
	}
	return 0;
}
