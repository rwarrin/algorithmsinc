#include <stdio.h>

#define ArrayCount(Array) (sizeof((Array)) / sizeof((Array)[0]))

int RecursiveArraySum(int *Array, int Left, int Right)
{
	if(Left >= Right)
	{
		return Array[Left];
	}

	int Mid = (Left + Right) / 2;
	int LeftSum = RecursiveArraySum(Array, Left, Mid);
	int RightSum = RecursiveArraySum(Array, Mid + 1, Right);
	int Result = LeftSum + RightSum;

	return(Result);
}

int main(void)
{
	int Numbers[] = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
	int Sum = RecursiveArraySum(Numbers, 0, ArrayCount(Numbers) - 1);
	printf("Sum = %d\n", Sum);
}
