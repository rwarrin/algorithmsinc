/**
 *
 **/

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum
{
	DEFAULT_STACK_SIZE = 32
};

typedef struct stack* stack_t;
struct stack
{
	int32_t Size;
	int32_t Top;
	int32_t *Array;
};

static stack_t
StackNew(int32_t SizeHint)
{
	int32_t StackSize = DEFAULT_STACK_SIZE;
	if(SizeHint)
	{
		StackSize = SizeHint;
	}

	stack_t Result = (stack_t)malloc(sizeof(*Result));
	Result->Size = SizeHint+1;
	Result->Top = 0;
	Result->Array = (int32_t *)malloc(sizeof(*Result->Array)*Result->Size);

	return(Result);
}

static void
StackDelete(stack_t *Stack)
{
	if((Stack != 0) && (*Stack != 0))
	{
		free((*Stack)->Array);
		free(*Stack);
		*Stack = 0;
	}
}

static int32_t
StackEmpty(stack_t Stack)
{
	int32_t Result = (Stack->Top <= 0);
	return(Result);
}

static int32_t
StackPush(stack_t Stack, int32_t Value)
{
	assert(Stack != 0);
	if(Stack != 0)
	{
		while(Stack->Top + 1 >= Stack->Size)
		{
			int32_t NewSize = Stack->Size*2;
			int32_t *NewArray = (int32_t *)realloc(Stack->Array, sizeof(*NewArray)*NewSize);

			Stack->Size = NewSize;
			Stack->Array = NewArray;
		}

		Stack->Array[++Stack->Top] = Value;
	}

	return(Value);
}

static int32_t
StackPop(stack_t Stack)
{
	assert(Stack != 0);
	assert(Stack->Top >= 0);

	int32_t Result = Stack->Array[Stack->Top--];
	return(Result);
}

int main(void)
{
	stack_t Stack = StackNew(0);
	for(int i = 0; i < 60; i++)
	{
		StackPush(Stack, i);
	}

	while(!StackEmpty(Stack))
	{
		printf("%d, ", StackPop(Stack));
	}

	StackDelete(&Stack);
	return 0;
}
