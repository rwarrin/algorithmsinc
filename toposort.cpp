/**
 *
 * Graph is
 *  1
 *  /\
 * 2  3
 *  \/
 *  4-6-7-8
 *  | _/
 *  5/
 **/

#include <assert.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct vertex* vertex_t;
struct vertex
{
	int32_t Id;
	struct vertex *Next;
};

typedef struct graph* graph_t;
struct graph
{
	int32_t VertexCount;
	struct vertex **VertexList;
};

static inline graph_t
NewGraph(uint32_t VertexCount)
{
	assert(VertexCount > 0);

	graph_t Result = (graph_t)malloc(sizeof(*Result));
	assert(Result != 0);

	Result->VertexCount = VertexCount;
	Result->VertexList = (struct vertex **)malloc(sizeof(struct vertex *) * VertexCount);
	assert(Result->VertexList != 0);
	for(int32_t Index = 0; Index < Result->VertexCount; ++Index)
	{
		Result->VertexList[Index] = 0;
	}

	return(Result);
}

static inline void
GraphAddEdge(graph_t Graph, int32_t Source, int32_t Dest)
{
	assert((Source >= 0) && (Source < Graph->VertexCount));
	assert((Dest >= 0) && (Dest < Graph->VertexCount));

	vertex_t NewLink = (vertex_t)malloc(sizeof(*NewLink));
	NewLink->Id = Dest;
	NewLink->Next = Graph->VertexList[Source];
	Graph->VertexList[Source] = NewLink;
}

static inline graph_t
GraphReverse(graph_t Graph)
{
	graph_t Result = NewGraph(Graph->VertexCount);

	for(int32_t VertexIndex = 0; VertexIndex < Graph->VertexCount; ++VertexIndex)
	{
		for(vertex_t Vertex = Graph->VertexList[VertexIndex];
			Vertex != 0;
			Vertex = Vertex->Next)
		{
			GraphAddEdge(Result, Vertex->Id, VertexIndex);
		}
	}

	return(Result);
}

struct queue_node
{
	int32_t Value;
	queue_node *Next;
};

typedef struct queue* queue_t;
struct queue
{
	int32_t Count;
	queue_node *Head;
	queue_node *Tail;
};

static inline queue_t
NewQueue()
{
	queue_t Result = (queue_t)malloc(sizeof(*Result));
	assert(Result != 0);

	Result->Count = 0;
	Result->Head = 0;
	Result->Tail = 0;

	return(Result);
}

static inline int32_t
QueueEmpty(queue_t Queue)
{
	int32_t Result = (Queue->Count == 0);
	return(Result);
}

static inline void
QueueAdd(queue_t Queue, int32_t Value)
{
	queue_node *NewNode = (queue_node *)malloc(sizeof(*NewNode));
	NewNode->Next = 0;
	NewNode->Value = Value;

	if(Queue->Head == 0)
	{
		Queue->Head = NewNode;
		Queue->Tail = NewNode;
	}
	else
	{
		Queue->Tail->Next = NewNode;
		Queue->Tail = NewNode;
	}

	++Queue->Count;
}

static inline int32_t
QueueRemove(queue_t Queue)
{
	assert(Queue->Count > 0);

	int32_t Result = INT_MIN;

	queue_node *Temp = Queue->Head;
	Queue->Head = Queue->Head->Next;

	Result = Temp->Value;
	free(Temp);

	--Queue->Count;
	return(Result);
}

static inline void
PrintGraph(graph_t Graph)
{
	for(int32_t VertexIndex = 0; VertexIndex < Graph->VertexCount; ++VertexIndex)
	{
		vertex_t Node = Graph->VertexList[VertexIndex];
		printf("%3d ", VertexIndex);
		for(; Node != NULL; Node = Node->Next)
		{
			printf("->%-3d", Node->Id);
		}
		printf("\n");
	}
}

static inline void
TopoSort(graph_t Graph, int32_t *VertexIndexedArray)
{
	assert(Graph != 0);
	assert(VertexIndexedArray != 0);

	int32_t *InDegree = (int32_t *)malloc(sizeof(*InDegree) * Graph->VertexCount);
	for(int32_t VertexIndex = 0; VertexIndex < Graph->VertexCount; ++VertexIndex)
	{
		InDegree[VertexIndex] = 0;
	}

	for(int32_t VertexIndex = 0; VertexIndex < Graph->VertexCount; ++VertexIndex)
	{
		for(vertex_t Vertex = Graph->VertexList[VertexIndex];
			Vertex != 0;
			Vertex = Vertex->Next)
		{
			++InDegree[Vertex->Id];
		}
	}

	queue_t Queue = NewQueue();
	for(int32_t VertexIndex = 0; VertexIndex < Graph->VertexCount; ++VertexIndex)
	{
		if((InDegree[VertexIndex] == 0) &&
		   ((Graph->VertexList[VertexIndex] != 0) &&
			(Graph->VertexList[VertexIndex]->Next != 0)))
		{
			QueueAdd(Queue, VertexIndex);
		}
	}

	for(int32_t SortIndex = 0;
		!QueueEmpty(Queue);
		++SortIndex)
	{
		int32_t VertexIndex = QueueRemove(Queue);
		VertexIndexedArray[SortIndex] = VertexIndex;

		vertex_t Vertex = Graph->VertexList[VertexIndex];
		for(; Vertex != 0; Vertex = Vertex->Next)
		{
			if((--InDegree[Vertex->Id]) == 0)
			{
				QueueAdd(Queue, Vertex->Id);
			}
		}
	}
}

int main(void)
{
	graph_t Graph = NewGraph(10);
	GraphAddEdge(Graph, 1, 2);
	GraphAddEdge(Graph, 1, 3);
	GraphAddEdge(Graph, 2, 4);
	GraphAddEdge(Graph, 3, 4);
	GraphAddEdge(Graph, 4, 5);
	GraphAddEdge(Graph, 4, 6);
	GraphAddEdge(Graph, 5, 7);
	GraphAddEdge(Graph, 6, 7);
	GraphAddEdge(Graph, 7, 8);
	printf("--- Graph ---\n");
	PrintGraph(Graph);

	int32_t *SortedArray = (int32_t *)malloc(sizeof(*SortedArray) * Graph->VertexCount);
	for(int32_t VertexIndex = 0; VertexIndex < Graph->VertexCount; ++VertexIndex)
	{
		*(SortedArray + VertexIndex) = -1;
	}

	TopoSort(Graph, SortedArray);
	printf("\n--- Sorted ---\n");
	for(int32_t VertexIndex = 0; VertexIndex < Graph->VertexCount; ++VertexIndex)
	{
		printf("%3d ", VertexIndex);
	}
	printf("\n");
	for(int32_t VertexIndex = 0; VertexIndex < Graph->VertexCount; ++VertexIndex)
	{
		if(SortedArray[VertexIndex] == -1)
		{
			printf("%3c ", '*');
		}
		else
		{
			printf("%3d ", SortedArray[VertexIndex]);
		}
	}

	return 0;
}
